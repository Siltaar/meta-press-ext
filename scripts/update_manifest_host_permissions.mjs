// #!/usr/bin/env deno
// # vim: ft=javascript
// SPDX-Filename: scripts/update_manifest_host_permissions.mjs
// SPDX-FileCopyrightText: 2024 Simon Descarpentres <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
/* globals console Deno */

import { build_src } from '../js/core/source_utils.js'
import { get_current_search_hosts, get_current_news_hosts } from '../js/mp_utils.js'

console.info	= (...txt) => console.log('[I]', ...txt)
console.warn	= (...txt) => console.log('[W]', ...txt)
console.error = (...txt) => console.log('[E]', ...txt)
console.debug = (...txt) => console.log('[D]', ...txt)

async function init() {
	const start_time = new Date()
	console.log(start_time, 'update_manifest_host_permissions starts')
	const built_src = build_src(
		JSON.parse(
			await Deno.readTextFile('json/sources.json')
		)
	)
	console.log('json/sources.json loading duration :', new Date() - start_time, 'ms')
	const src_keys = Object.keys(built_src)
	const hosts = get_current_search_hosts(src_keys, built_src)
	const bloated_hosts = hosts.concat(get_current_news_hosts(src_keys, built_src))
	const host_set = new Set(bloated_hosts)
	const manifest_json = JSON.parse(
		await Deno.readTextFile('manifest.json')
	)
	const undup_hosts = Array.from(host_set)
	manifest_json.host_permissions = undup_hosts
	Deno.writeTextFile('manifest.json', JSON.stringify(manifest_json, null, '\t'))
	const end_time = new Date()
	console.log(end_time, 'found', undup_hosts.length, 'hosts took', end_time - start_time, 'ms')
}
await init()
