// #!/usr/bin/env deno
// # vim: ft=javascript
// SPDX-Filename: scripts/mpress.mjs
// SPDX-FileCopyrightText: 2024 Simon Descarpentres <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
/* globals console Deno */

// import { readFile } from 'node:fs/promises'
import * as Colors from 'https://deno.land/std@0.224.0/fmt/colors.ts'
// import { DOMParser } from "https://esm.sh/linkedom"
// import { DOMParser } from 'https://deno.land/x/deno_dom/deno-dom-wasm.ts'
// import { XPathEvaluator } from "https://deno.land/x/lib/types/lib.dom.ts";
// try import * as xpath from 'node:https://www.npmjs.com/package/xpath'

import { build_src } from '../js/core/source_utils.js'
import { tag_sel_src_keys, QS_to_JSON } from '../js/core/tag_utils.js'
import { export_filename } from '../js/mp_snippets.js'
import { res_in_JSON,res_in_RSS,res_in_CSV,res_in_ATOM } from '../js/core/result_formats.js'
import { eff_worker_pool_size, split_jobs } from '../js/core/worker_utils.js'

// https://stackoverflow.com/questions/11398419/trying-to-use-the-domparser-with-node-js
// const jsdom = require("jsdom")
// const { JSDOM } = jsdom
// import { JSDOM } from 'https://esm.sh/jsdom@24.1.0' -> https://github.com/jsdom/jsdom/issues/3279
// import { JSDOM } from 'https://esm.sh/jsdom-deno@19.0.2'
import * as jsdom from 'https://esm.sh/jsdom-deno@19.0.2'


const output_formats = ['JSON', 'RSS', 'ATOM', 'CSV']
const force_log = console.log
const force_trace = console.trace

function print_usage () {	// usage represents the help guide
	const usageText = `Usage: mpress '<mpress-URL / querystring / search parameters>'
  <mpress-URL> is a Meta-Press.es addon-generated URL (or just its query-string part)
    It can contain :
     - q='search terms': the terms to search for
     - tech='many words': any tag related argument from Meta-Press.es to select sources
     - format='json': parameter to select the output format among : ${output_formats}
     - filename='custom.txt': parameter to customise the output filename
     - user_lang='en': parameter to customise the localized-date language
     - verbose=1: if set to 1, mpress will output some information
		   - if set to 2, warnings and debug messages are also output to the console
		   - if set to 3, all the errors are output to the console
Example: ./mpress '?q=search terms&lang=fr&tech=many words'`
	// console.log(usageText)
	force_log(usageText)
}

// moz-extension://66fc4e5b-dc68-4865-aa24-f2a58ae7cc00/html/index.html
// ?q=quadrature+du+net&src_type=Secondary+source&tech=many+words
// ?q=test&tech=cherry-pick_sources&add_src_sel=https%3A%2F%2Fscrutari.coredem.info%2F%23fr
// ?q=quadrature+du+net
async function init() {
	// Managing arguments
	const args = Deno.args
	// console.log('args', args)
	if (args.length < 1) {
		console.error(`Error: Only one argument is accepted, a Meta-Press.es URL or querystring`)
		print_usage()
		return
	}
	const joint_args = args.join(' ')
	// console.log('joint_args', joint_args)
	// const QS = new URL(args[args.length - 1], 'moz-extension://').searchParams
	const QS = new URL(joint_args, 'moz-extension://').searchParams
	if (QS.size === 0) {
		force_log(`Error: The argument is not a valid Meta-Press.es search URL or querystring`)
		print_usage()
		return
	}
	// Managing verbosity
	// https://stackoverflow.com/a/69958999 console-error-error-could-not-parse-css
	const virtualConsole = new jsdom.VirtualConsole()
	virtualConsole.on('error', () => { /* No-op to skip console errors. */})
	//
	const dbg = Number(QS.get('verbose'))
	console.log		= () => {}  // no op
	console.info	= () => {}
	console.warn	= () => {}
	console.error	= () => {}
	console.debug	= () => {}
	console.trace	= () => {}
	let JSDOM_args = {virtualConsole}
	const dt = () => new Date().toISOString()
	if (dbg) {
		console.info	= (...txt) => force_log(Colors.bold(Colors.blue('[I]')), dt(), ...txt)
		if (dbg > 1) {
			console.warn	= (...txt) => force_log(Colors.bold(Colors.yellow('[W]')), dt(), ...txt)
			console.error = (...txt) => force_log(Colors.bold(Colors.red('[E]')), dt(), ...txt)
			console.debug = (...txt) => force_log(Colors.bold(Colors.cyan('[D]')), dt(), ...txt)
		}
		if (dbg > 2) {
			console.log	= force_log
			console.trace = force_trace
			JSDOM_args = {}
		}
	}
	// Generating the DOM parser
	// Node DOMParser https://github.com/fb55/htmlparser2
	const JSDOMParser = {
		parseFromString: function (a, c_type) {
			// console.debug('parseFromString', c_type)
			JSDOM_args = Object.assign({}, JSDOM_args, {
				// url: "https://example.com/",
				// referrer: "https://example.org/",
				contentType: c_type,
				storageQuota: 100000000,
			})
			const DOM = new jsdom.JSDOM (a,	JSDOM_args)
			if (DOM && DOM.window && DOM.window.document)
				return DOM.window.document
			else {
				console.debug('Failed to parse', String(a.slice(0,60), c_type))
				return undefined
			}
		}
	}
	dbg && console.log('\n  mpress allows you to query Meta-Press.es sources from command line\n')
	const output_format = (QS.get('format') || 'JSON').toUpperCase()
	if (!output_formats.includes(output_format)) {
		force_log(`Error: the output format must be one of ${output_formats}`)
		print_usage()
		return
	}
	const search_terms = QS.get('q')
	if (!search_terms) {
		force_log(`Error: no search terms provided`)
		print_usage()
		return
	}
	const user_lang = QS.get('user_lang') || 'en'
	const start_load_src = performance.now()
	const built_src = build_src(
		JSON.parse(
			await Deno.readTextFile('json/sources.json')
		)
	)
	dbg && console.info(`sources.json loading time : ${performance.now() - start_load_src} ms`)
	const source_keys = Object.keys(built_src)
	const tags = QS_to_JSON(QS)
	// dbg && console.debug({tags})
	const src_sel_keys = tag_sel_src_keys(built_src, source_keys, tags)
	console.debug('src_sel_keys', src_sel_keys)
	if (src_sel_keys.length === 0) {
		force_log(`Error: no source corresponding to given tags`)
		print_usage()
		return
	}
	// const src_k = 'https://www.hautanjou.fr' // -> CSS
	// const src_k = 'https://www.grenzecho.net' // -> month_nb (Intl) XXX JSDOM no parse CSS
	// const src_k = 'https://www.faz.net/aktuell' // -> JSON + _tpl
	// const src_k = 'https://www.hs.fi'	// -> JSON
	// const src_k = 'https://northernstar.info'
	// const src_k = 'https://www.helsinkitimes.fi'  // -> XPath -> search for "trump" OK
	// const src_k = 'https://www.goodplanet.info'	// --> XML XPath for r_dt
	// console.log(built_src[src_k])
	let len_src_sel = src_sel_keys.length
	console.info(len_src_sel, 'sources will be queried')
	const ver = (JSON.parse(await Deno.readTextFile('manifest.json'))).version
	if (typeof (Worker) === 'undefined')
		force_log('Your browser doesn\'t support web workers.')
	const eff_worker_pool = eff_worker_pool_size(len_src_sel)
	let worker_pool_size = eff_worker_pool
	const worker_len_src_sel = []
	let findings = []
	let meta_findings = []
	const worker_srcs = split_jobs(src_sel_keys, eff_worker_pool)
	// console.debug('worker_srcs', worker_srcs)
	for (let wid = 0; wid < eff_worker_pool; wid++) {  // allow to search a single source
		const worker = new Worker(new URL('../js/page/search_worker.js', import.meta.url),
			{ type: 'module' })
		worker.postMessage([wid, search_terms, worker_srcs[wid], built_src, dbg, user_lang])
		worker_len_src_sel[wid] = worker_srcs[wid].length
		// console.info('worker_len_src_sel[wid]', worker_len_src_sel[wid], wid)
		// console.debug('starts worker', wid, 'with', worker_srcs[wid])
		worker.onmessage = async (e) => {
			const rz = e.data
			// force_log('rz = e.data', e.data)
			findings = findings.concat(rz.findings)
			meta_findings = meta_findings.concat(rz.meta_findings)
			worker_len_src_sel[rz.worker_id] -= 1
			if (worker_len_src_sel[rz.worker_id] == 0) {
				// close()
				worker_pool_size -= 1
				// console.info('worker', rz.worker_id, 'finished')
				// console.info('worker_pool_size', worker_pool_size)
				if (worker_pool_size == 0)
					write_results ()
			}
		}
	}
	async function write_results () {
		let from_sources = 0
		for (const mf_src of meta_findings) {
			if (mf_src.mf_res_nb > 0)
				from_sources += 1
		}
		console.info(findings.length, 'results found from', from_sources, 'sources in',
			((performance.now() - start_load_src) / 1000).toFixed(2), 's')
		const output_filename = QS.get('filename') || export_filename(ver, output_format)
		let out_str = ''
		switch (output_format) {
			case 'JSON': out_str = res_in_JSON(tags, findings, meta_findings, search_terms) ;break
			case 'CSV' : out_str = res_in_CSV(findings) ;break
			case 'RSS' : out_str = res_in_RSS(tags, findings, search_terms, user_lang, ver) ;break
			case 'ATOM': out_str = res_in_ATOM(tags, findings, search_terms, user_lang, ver) ;break
		}
		await Deno.writeTextFile(output_filename, out_str)
	}
}
await init()

// + tag selection (from querystring format)
// + file exports
// X headline fetching  -->  you better wget the RSS directly
// + parallelisation of the requests : Web Workers
// - [D] Why did we lost document ? When using JSdOM
