// SPDX-FileName: ./source_fetching.js
// SPDX-FileCopyrightText: 2017-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

import * as sµ from './source_utils.js'
import { regextract, scrap } from './web_scraping.js'
import { parse_dt_str } from './date_parsing.js'
import { delay } from '../lib/js/date.js'
import { drop_escaped_quotes, triw } from '../lib/js/text.js'
import { is, is_str } from '../lib/js/types.js'
import { deep_copy } from '../lib/js/object.js'
import { urlify } from '../lib/js/URL.js'
import { do_int } from '../lib/js/math.js'
import { shorten, bolden, drop_low_unprintable_utf8 } from '../lib/js/text.js'
import { XML_encode_UTF8 } from '../lib/js/XML.js'
import { parse_json } from '../lib/js/JSON.js'

import { HTML_decode_entities } from '../lib/browser/BOM_utils.js'
import { strip_HTML_tags, is_DOM_parsererror } from '../lib/DOM_parser.js'
import { evaluateXPath } from '../lib/XPath_evaluator.js'
import { DNS_resolve } from '../lib/DNS_resolver.js'

import * as arr   from '../lib/js/array.js'

const EXCERPT_SIZE = 350
const HEADLINE_TITLE_SIZE = 140
export const INTER_FETCH_DELAY = 251  // ms ; 2001 -> no improvment

// https://unicode.org/reports/tr18/#General_Category_Property
const word_sep_re = '[^\p{Letter}\p{Number}]'

export async function fetch_raw (url, method, headers, ctype, body,
	search_terms, MAX_RES_BY_SRC, token, abort_controller) {
	return await fetch(
		format_search_url(url, search_terms, MAX_RES_BY_SRC, null, token),
		{
			signal: abort_controller && abort_controller.signal || null,
			method: method || 'GET',
			headers: Object.assign(
				headers || {},
				{'Content-Type': ctype || 'application/x-www-form-urlencoded'},
			),
			body: body ? format_search_url(body, search_terms, MAX_RES_BY_SRC, ctype, token) : null
		}
	)
}
/**
 *
 */
export function format_search_url(search_url, terms, max_res_by_src, search_ctype, token='') {
	return search_url.replace(/{}/g, !search_ctype ? encodeURIComponent(terms) : terms)
		.replace(/{<(\d+?)}/g, (_, p1) => Math.min(max_res_by_src, Number(p1)-1))  // size={<51}
		.replace(/{#}/g, max_res_by_src)  // if the limit is unknown
		.replace(/{T}/g, token)  // used by AfricaIntelligence, or ScienceDirect.com
		.replace(/{D}/g, new Date().toISOString())  // used by Mobilizon
		.replace(/{,}/g, terms && terms.replace(/ /g, ',') || '')  // used by sciencepresse.qc.ca
}
/**
 *
 */
async function extract_rep (raw_rep, raw_rep_re, rep_charset, rep_type, DOM_parser,
	dbg=true) {
	let rep
	const rep_ctype = raw_rep.headers.get('content-type')
	if (rep_ctype.includes('application/json') || 'JSON' === rep_type) {
		if(raw_rep_re) {  // jsonp_to_json_re
			const raw_txt = drop_escaped_quotes(triw(await raw_rep.text()))
			dbg && console.log('regextract(raw_rep_re raw_txt', raw_rep_re[0], raw_txt, raw_rep_re[1])
			const extracted_txt = regextract(raw_rep_re[0], raw_txt, raw_rep_re[1])
			dbg && console.log({extracted_txt})
			rep = parse_json(extracted_txt)
			dbg && console.log('rep', rep)
		} else {
			rep = await raw_rep.json()
		}
	} else {  // non JSON sources
		let txt_rep, arr_rep
		if (rep_charset) {
			arr_rep = await raw_rep.arrayBuffer()
			const text_decoder = new TextDecoder(rep_charset, {fatal: true})
			txt_rep = text_decoder.decode(arr_rep)
		} else {
			txt_rep = await raw_rep.text()
			if (raw_rep_re)
				txt_rep = regextract(raw_rep_re[0], txt_rep, raw_rep_re[1])
		}
		rep = DOM_parse_text_rep(txt_rep, rep_ctype, DOM_parser)
		if (!rep) throw `dom parser error ${rep}`
	}
	return rep
}
/**
 *
 */
function get_results (src_def, rep, DOM_parser, dbg=false) {
	dbg && console.log('get_results (src_def, rep)', src_def, rep)
	let fdgs = []
	if ('JSON' === src_def.type && !is(src_def.json_to_html)) {
		fdgs = is(src_def.results) ? scrap('results', rep, src_def, DOM_parser, 0, [], false) : rep
	} else {
		if (is(src_def.json_to_html)) {
			const src_copy = deep_copy(src_def)
			src_copy.type = 'JSON'
			rep = scrap('json_to_html', rep, src_copy, DOM_parser, 0, [], false)
			rep = DOM_parse_text_rep(rep, 'text/html', DOM_parser)
		}
		if (is(src_def.results)) {
			fdgs = rep.querySelectorAll(src_def.results)
			dbg && console.log('rep.querySelectorAll(src_def.results)', rep, src_def.results)
		} else if (is(src_def.results_xpath))
			fdgs = evaluateXPath(rep, src_def.results_xpath)
		else {
			fdgs = rep.querySelectorAll('item') // we need fdgs to be able to auto-detect RSS variant
			if (!fdgs.length)
				fdgs = rep.querySelectorAll('entry')  // it's ATOM flavour fdgs
			if (!fdgs.length) {
				let err_msg = 'no source definition for "results"'
				if (src_def.type && src_def.type == 'XML')
					err_msg = 'No items, no entries, maybe no results'
				throw err_msg
			}
		}
	}
	dbg && console.log('get_results fdgs', fdgs)
	return fdgs
}
export async function search_source_selection(search_terms, src_keys, src_objs, search_cb) {
  let next_req_delay
  arr.shuffle_array(src_keys)
  let domain_ip = null
  let last_fetch_date = null
  let now_dt = null
	const SERIAL_QUERIES_DBG = false
  const last_seen_IPs = {}
	const resolved_search_hosts = {}
  let src_host = ''
  let first_ip
  // for (let src_key of current_source_selection.slice(0, 499)) {
  for (const src_key of src_keys) {
    SERIAL_QUERIES_DBG && console.debug('will', search_cb.name, src_key)
    // console.debug('will', search_cb.name, src_key)
    src_host = new URL(src_key).host
    domain_ip = resolved_search_hosts[src_host]
    if (!domain_ip) {
      const addrs = await DNS_resolve(src_host)
      if (addrs.length) {
        first_ip = addrs[0]
        resolved_search_hosts[src_host] = first_ip
        domain_ip = first_ip
      }
    }
    // console.log(src_key, domain_ip)
    if (domain_ip) {
      now_dt = new Date().getTime()
      last_fetch_date = last_seen_IPs[domain_ip]
      const should_wait = last_fetch_date && last_fetch_date > (now_dt - INTER_FETCH_DELAY)
      const next_fetch_date = last_fetch_date + INTER_FETCH_DELAY
      next_req_delay = should_wait ? next_fetch_date - now_dt : 0
      should_wait && console.debug(src_key, 'will wait', next_req_delay, 'ms')
      last_seen_IPs[domain_ip] = next_fetch_date || now_dt
    } else {
      next_req_delay = INTER_FETCH_DELAY / 100
    }
    const src_def = src_objs[src_key]
    if (SERIAL_QUERIES_DBG) {
      await search_cb(src_key, src_def, search_terms)
    } else {
			setTimeout(() => search_cb(src_key, src_def, search_terms), next_req_delay)
		}
  }
	console.debug('finished launching search_cbs')
}
/**
 * Request the source and scrap the results
 * @param {string} src_key The source key
 * @param {Object} src_def The source definition
 * @returns {Object} Search JSON-export for this source
 */
export async function search_source (worker_id, src_key, src_def, search_terms, dbg, userLang,
	DOM_parser, fetch_abort_controller, MAX_RES_BY_SRC=10, TEST_SRC=false) {
	const src_query_start_date = new Date()
	let token = ''
	const rz = {'errors': [], 'findings': [], 'meta_findings': [], 'search_terms': search_terms}
	if (src_def.token) {
		let raw_pre
		try {
			raw_pre = await fetch_raw (  // token for {T} in format_search_URL
				src_def.token.token_url,
				src_def.token.token_method,
				src_def.token.token_headers,
				src_def.token.token_ctype,
				src_def.token.token_body,
				search_terms,
				MAX_RES_BY_SRC,
				'',
				fetch_abort_controller
			)
		} catch (exc) {
			rz.errors.push(sµ.create_error(
				src_def, sµ.error_code.NETWORK_ERROR, exc, sµ.error_status.ERROR, 'test_src', {}, 1))
			return rz
		}
		if (!raw_pre.ok) {
			rz.errors.push(sµ.create_error(src_def, sµ.error_code.ERROR,
				`fetch not ok : ${raw_pre.status}`, sµ.error_status.ERROR, 'test_src', {}, 1))
			return rz
		}
		let rep_pre
		try {
			rep_pre = await extract_rep(raw_pre,
				src_def.token.raw_pre_re,
				src_def.token.charset,
				src_def.token.token_type,
				DOM_parser
			)
		} catch (exc) {
			rz.errors.push(sµ.create_error(src_def, sµ.error_code.ERROR,
				exc, sµ.error_status.ERROR, 'test_src', {}, dbg))
			return rz
		}
		// console.debug({rep_pre})
		token = scrap('token_sel', rep_pre, src_def.token, DOM_parser, 0, rz.errors)
	}
	// end of token fetching
	let raw_rep
	try {
		raw_rep = await fetch_raw (
			src_def.search_url,
			src_def.method,
			src_def.search_headers,
			src_def.search_ctype,
				src_def.body,
			search_terms,
			MAX_RES_BY_SRC,
			token,
			fetch_abort_controller
		)
	} catch (exc) {
		rz.errors.push(sµ.create_error(src_def, sµ.error_code.NETWORK_ERROR,
			exc, sµ.error_status.ERROR, 'test_src', {}, 1))
		return rz
	}
	try {
		if (!raw_rep.ok) {
			rz.errors.push(sµ.create_error(src_def, sµ.error_code.ERROR,
				`fetch not ok : ${raw_rep.status}`, sµ.error_status.ERROR, 'test_src', {}, 1))
			return rz
		}
		let rep
		try {
			rep = await extract_rep(raw_rep,
				src_def.raw_rep_re,
				src_def.tags.charset,
				src_def.type,
				DOM_parser)
		} catch (exc) {
			rz.errors.push(sµ.create_error(src_def, sµ.error_code.ERROR,
				exc, sµ.error_status.ERROR, 'test_src', {}, dbg))
			return rz
		}
		let fdgs = []
		try {
			fdgs = get_results(src_def, rep, DOM_parser)
		} catch (exc) {
			rz.errors.push(sµ.create_error(src_def, sµ.error_code.ERROR,
				exc, sµ.error_status.ERROR, 'test_src', {'article': src_key}, dbg))
		}
		let fdgs_nb = fdgs ? fdgs.length : 0
		// dbg && console.debug('fdgs', fdgs_nb, fdgs)
		let max_fdgs = MAX_RES_BY_SRC  //!\ fail things when undef
		// dbg && console.debug('MAX_REs_BY_SRC', MAX_RES_BY_SRC)
		fdgs_nb = Math.min(fdgs_nb, max_fdgs)
		if (0 === fdgs_nb || isNaN(fdgs_nb)) {
			rz.errors.push(sµ.create_error(src_def, sµ.error_code.NO_RESULT,
				'No results', sµ.error_status.WARNING, 'test_src', {}, dbg))
			if (TEST_SRC)  // keep empty sources in the metaFindings to allow reports
				return rz
		}
		let res_nb_str = ''
		let res_nb = NaN
		if (src_def.type !== 'XML') {
			if (src_def.res_nb)
				try {
					res_nb_str = scrap('res_nb', rep, src_def, DOM_parser, 0, rz.errors)
					// console.info('res_nb_str', res_nb_str)
					res_nb = do_int(res_nb_str)
					if (isNaN(res_nb) || 0 === res_nb)
						rz.errors.push(sµ.create_error(src_def, sµ.error_code.ERROR,
							`res_nb do_int("${res_nb_str}") failed`,
							sµ.error_status.WARNING, 'test_src', {}, dbg)
						)
				} catch (exc) {
					rz.errors.push(sµ.create_error(src_def, sµ.error_code.ERROR,
						exc, sµ.error_status.ERROR, 'test_src', {}, dbg))
				}
		} else {
			src_def = set_RSS_variant(fdgs, src_def, DOM_parser)
		}
		if (isNaN(res_nb) || 0 === res_nb) {  // some sources don't have res_nb at all
			res_nb = fdgs_nb
		}
		let f_h1='', f_url='', f_by='', f_dt=new Date(0), f_nb=0, f_img=''
		for (const fdg of fdgs) {
			if (max_fdgs-- === 0) break
			f_nb = MAX_RES_BY_SRC - max_fdgs
			f_h1 = ''; f_url=''; f_by=''; f_img=''; f_dt=new Date(0)
			let l_fmp ={'f_img_src':'','f_img_alt':'','f_img_title':'','mp_data_img':'',
				'mp_data_alt':'','mp_data_title':'','f_txt':''}
			try {
				f_h1 = scrap('r_h1', fdg, src_def, DOM_parser, f_nb, [])
				if (!f_h1) throw 'missing this f_h1'
				f_dt = scrap('r_dt', fdg, src_def, DOM_parser, f_nb, [], false)
				if (!f_dt) throw `No f_dt after lookup (mp.scrap) f_h1 ${f_h1} %%%`
				f_dt = parse_dt_str(f_dt, src_def.tags.tz, f_nb, src_def)
				if(isNaN(f_dt)) throw 'No f_dt after parse_dt_str'
				f_url = scrap('r_url', fdg, src_def, DOM_parser, f_nb, [])
				if (!f_url) throw 'no f_url after lookup'
				f_url = urlify(f_url, src_def.domain_part)
				if (!f_url) throw 'missing this f_url'
			} catch (exc) {
				// console.debug('f_h1', f_h1, 'f_dt', f_dt, 'f_url', f_url)
				rz.errors.push(sµ.create_error(src_def, sµ.error_code.ERROR,
					f_nb + ' ' + exc, sµ.error_status.ERROR, 'test_src', {'article': src_key}, dbg))
				res_nb--
				fdgs_nb--
				continue
			}
			try {
				l_fmp.f_txt = scrap('r_txt', fdg, src_def, DOM_parser, f_nb, rz.errors)
				l_fmp = img_lookup(l_fmp, f_img, fdg, src_def, DOM_parser, f_nb)
				f_by = scrap('r_by', fdg, src_def, DOM_parser, f_nb, []) || src_def.tags.name
				if (src_def.filter_results) {
					// let f_full_txt = _.str_uniq_line(`${l_fmp.f_txt} ${f_h1}`.toLowerCase())
					const f_full_txt = `${f_h1} ${l_fmp.f_txt}`.toLowerCase()
					// console.log('filter_results on `${f_txt} ${f_h1}`.toLowerCase()', {f_full_txt})
					const terms = search_terms.toLowerCase().split(' ')
					// console.log({terms})
					const terms_re = terms.map((a) => new RegExp(`${word_sep_re}${a}${word_sep_re}`, 'u'))
					// console.log({terms_re})
					const keep_result = terms_re.every(a => a.test(f_full_txt))
					if (!keep_result) {
						console.log(`[${worker_id}] Discarded result`, f_nb, 'from', src_def.tags.name,
							'because missing search terms', {f_full_txt})
						res_nb--
						fdgs_nb--
						continue
					}
				}
				const f_txt = TEST_SRC ? shorten(l_fmp.f_txt, EXCERPT_SIZE) :
					bolden(l_fmp.f_txt, search_terms)
				rz.findings.push({f_h1:f_h1, f_url:f_url, f_by:f_by, f_txt: f_txt,
					f_h1_title:	f_h1,
					f_by_title:	f_by,
					f_dt: f_dt.toISOString().slice(0, -14),
					f_epoc:	f_dt.valueOf(),
					f_ISO_dt: f_dt.toISOString(),
					f_dt_title: f_dt.toLocaleString(userLang),
					f_icon:	src_def.favicon_url,
					f_img_src: l_fmp.f_img_src,
					f_img_alt: l_fmp.f_img_alt,
					f_img_title: l_fmp.f_img_title,
					mp_data_img: l_fmp.mp_data_img,
					mp_data_alt: l_fmp.mp_data_alt,
					mp_data_title: l_fmp.mp_data_title,
					f_source: src_def.tags.name,
					f_source_title: src_key,
					mp_data_key: src_key
				})
			} catch (exc) {
				rz.errors.push(sµ.create_error(src_def, sµ.error_code.ERROR,
					f_nb + ' ' + exc, sµ.error_status.ERROR, 'test_src', {'article': src_key}, dbg))
			}
		}
		const local_res_nb = fdgs_nb === 0 ? fdgs_nb : res_nb
		rz.meta_findings.push({
			mf_icon: src_def.favicon_url,
			mf_name: src_def.tags.name,
			mf_ext_link: format_search_url(src_def.search_url_web || src_def.search_url,
				search_terms, MAX_RES_BY_SRC),
			mf_res_nb: local_res_nb,
			mf_locale_res_nb: local_res_nb.toLocaleString(userLang),
		})
		console.log(`[${worker_id}]`, src_key, 'duration', new Date() - src_query_start_date, 'ms',
			{res_nb})
	} catch (exc) {
		const err = exc.name == 'AbortError' ? sµ.error_code.NETWORK_ERROR : sµ.error_code.ERROR
		rz.errors.push(sµ.create_error(src_def, err, exc,
			sµ.error_status.ERROR, 'test_src', {'article': src_key}, dbg))
	}
	return rz
}
function img_lookup(l_fmp, f_img, fdg, src_def, DOM_parser, f_nb=0) {
	const dbg = false
	dbg && console.debug('img_lookup(l_fmp, f_img, f, src_def, DOM_parser, f_nb=0)',
		l_fmp, f_img, 'f', 'n', 'DOM_parser', f_nb)
	if (! src_def.type) {
		const is_r_img = is_str(src_def.r_img)
		if (is_r_img || is_str(src_def.r_img_xpath))
			if (is_r_img)
				f_img = fdg.querySelector(src_def.r_img)
				/* try { f_img = f.querySelector(src_def.r_img) }
				catch (exc) { error.push(sµ.create_error(src_def, sµ.error_code.ERROR,
					exc, sµ.error_status.ERROR, 'test_src', {'article': i})) }*/
			else // (is_str(src_def.r_img_xpath))
				f_img = evaluateXPath(fdg, src_def.r_img_xpath)
				/*try { f_img = evaluateXPath(f, src_def.r_img_xpath) }
				catch (exc) { error.push(sµ.create_error(src_def, sµ.error_code.ERROR,
					exc, sµ.error_status.ERROR, 'test_src', {'article': i})) }*/
	}
	if (is(f_img)) {
		if (f_img === '' && src_def.type === 'XML') { // extract image from f_txt
			const f_txt = l_fmp.f_txt
			const f_txt_dom = DOM_parse_text_rep(
				HTML_decode_entities(f_txt),
				'text/html', DOM_parser
			)
			// f_img = f_txt_dom.querySelector('img')
			f_img = f_txt_dom.getElementsByTagName('img')[0] // it's not faster with Firefox 90.a1
			// it's slower in Chromium Version 90.0.4430.85 (Build officiel) Arch Linux (64 bits)
			// a benchmark shows it 2x faster in Firefox 96
			l_fmp.f_txt = f_txt_dom.body.textContent // removes image from f_txt
		}
		if (f_img) {
			// l_fmp.f_img_src = urlify(f_img.attributes.src.value, src_def.domain_part)
			// -> Deno can't .attri
			l_fmp.f_img_src = urlify(f_img.getAttribute('src'), src_def.domain_part)
			l_fmp.f_img_alt = strip_HTML_tags(f_img.getAttribute('alt'), DOM_parser) || ''
			l_fmp.f_img_title = strip_HTML_tags(
				f_img.getAttribute('title'), DOM_parser) || l_fmp.f_img_alt || ''
		}
	}
	if (is(src_def.r_img_src) || is(src_def.r_img_src_xpath)) {
		// we loose errors of this lookup
		l_fmp.f_img_src = scrap('r_img_src', fdg, src_def, DOM_parser, f_nb, [])
		if (is(l_fmp.f_img_src))
			l_fmp.f_img_src = urlify(l_fmp.f_img_src, src_def.domain_part)
	}
	if (is(src_def.r_img_alt))
		l_fmp.f_img_alt = scrap('r_img_alt', fdg, src_def, DOM_parser, f_nb, [])
	if (is(src_def.r_img_title))
		l_fmp.f_img_title = scrap('r_img_title', fdg, src_def, DOM_parser, f_nb, [])
	if (!l_fmp.f_img_title)
		l_fmp.f_img_title = l_fmp.f_img_alt
	l_fmp.mp_data_img = l_fmp.f_img_src
	l_fmp.mp_data_alt = l_fmp.f_img_alt
	l_fmp.mp_data_title = l_fmp.f_img_title
	// if (!IS_LOAD_IMG && l_fmp.f_img_src)  // garbadge from the past
	// 	img_not_load(l_fmp)
	dbg && console.debug('img_lookup end (l_fmp, f_img, fdg, src_def, DOM_parser, f_nb=0)',
		l_fmp, f_img, 'fdg', 'n', 'DOM_parser', f_nb)
	return l_fmp
}
/*
 *
 */
export async function try_fetch (url, method='GET', body=null) {
	/* if (!await request_one_host_perm(url)) {  // async call cancels user-originated actions
		console.warning('Refused permission')
		return {ok:false}
	}*/
	try {
		return await fetch(url, {
			method: method,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded'},
			body: body
		})
	} catch (exc) {
		console.warn(exc)
		return {ok:false}
	}
}
/**
 *
 */
export async function fetch_headlines (
	source_objs,
	current_source_selection,
	max_news_total,
	max_news_by_src,
	user_lang,
	DOM_parser
) {
	let idx = 0
	const set_hdl = new Set()
	const headlines = []
	for (const src_key of current_source_selection.slice(0, max_news_total)) {
		// if(await mµ.check_host_perm (current_source_selection, source_objs)) {
		const src_def = source_objs[src_key]
		if (!src_def) {
			console.warn('Why is this source missing ?', src_key, src_def)
			continue
		}
		if (!src_def.news_rss_url) {
			console.warn(src_key, 'missing news_rss_url', src_def)
			continue
		}
		await delay(INTER_FETCH_DELAY / 10 * (idx++) / 2)
		const raw_rep = await try_fetch(src_def.news_rss_url)
		if (!raw_rep.ok) {
			console.warn(`${src_def.news_rss_url} status ${raw_rep.status}`)
			continue
		}
		let text_rep
		if (src_def.tags.charset) {
			text_rep = await raw_rep.arrayBuffer()
			const text_decoder = new TextDecoder(src_def.tags.charset, {fatal: true})
			text_rep = text_decoder.decode(text_rep)
		} else {
			text_rep = await raw_rep.text()
		}
		const rep = DOM_parse_text_rep(text_rep, raw_rep.headers.get('content-type'), DOM_parser)
		try {
			let items = rep.querySelectorAll('item')
			if (!items.length)
				items = rep.querySelectorAll('entry')
			const RSS = sµ.XML_SRC[find_RSS_variant(items, src_def, DOM_parser)]
			// console.log('RSS variant', find_RSS_variant(items, src_def., DOM_parser))
			const n_RSS = sµ.extend_source(RSS, {tags: src_def.tags,
				domain_part: src_def.domain_part,
				type: 'XML'
			})
			for (const hdl of Array.from(items).slice(0, max_news_by_src)) {
				try {
					const h_title = scrap('r_h1', hdl, n_RSS, DOM_parser, idx)
					const h_body = scrap('r_txt', hdl, n_RSS, DOM_parser, idx)
					const h_title_body = `${h_title} ${h_body}`
					// console.debug("scrap('r_dt', hdl, n_RSS", 'h_dt', hdl, n_RSS, i)
					const h_dt = new Date(scrap('r_dt', hdl, n_RSS, DOM_parser, idx, [], false))
					// console.debug('h_dt', h_dt)
					let h_img = img_lookup({}, '', hdl, n_RSS, DOM_parser)
					if (!h_img.f_img_src) { // for The Conversation
						h_img = img_lookup({f_txt: scrap('r_txt', hdl, n_RSS, DOM_parser, idx)},
							'', hdl, n_RSS, DOM_parser)
					}
					if(!set_hdl.has(h_title)) {
						set_hdl.add(h_title)
						headlines.push({
							h_title: shorten(h_title_body, HEADLINE_TITLE_SIZE),
							h_body: h_body,
							h_url:  urlify(scrap('r_url', hdl, n_RSS, DOM_parser, idx), src_def.domain_part),
							h_icon: src_def.favicon_url,
							h_tip: `${src_def.tags.name} (${h_dt.toLocaleString(user_lang)}) ${h_title}`,
							h_dt: h_dt.toISOString(),
							h_img: h_img.f_img_src || '',
							h_img_alt: h_img.f_img_alt || h_title,
							h_img_title: h_img.f_img_title || h_title,
							mp_data_img: h_img.f_img_src || '',
							mp_data_alt: h_img.f_img_alt || h_title,
							mp_data_title: h_img.f_img_title || h_title,
							mp_data_key: src_def.domain_part,
						})
					}
				} catch (exc) {
					console.error('header entry for', src_key, exc, hdl)
				}
			}
		} catch (exc) {
			console.error('header for', src_key, exc)
		}
		// }
	}
	return headlines
}
/**
 *
 */
export function DOM_parse_text_rep (txt_rep, c_type, DOM_parser) {
	c_type = c_type.split(';')[0].replace(/(rss|atom)\+/, '')
	c_type = c_type.replace(/vnd.turbo-stream./, '')  // for slate.fr
	// console.log('txt_rep', txt_rep)
	// LMD.no XML Parsing Error: undefined entity
	// Revue Ballast
	// Tel Quel (ma) XML Parsing Error: undefined entity ; InternalError: too much recursion
	txt_rep = txt_rep.trim()
	txt_rep = drop_low_unprintable_utf8(txt_rep)
	let rep
	try {
		rep = DOM_parser.parseFromString(txt_rep, c_type)
	} catch (exc) {
		if (! /"application\/xml" unimplemented/.test(String(exc)))
			throw exc
		c_type = 'text/html'  // other c_types are unimplemented in deno_dom
		try {
			rep = DOM_parser.parseFromString(txt_rep, c_type)
		} catch (inner_exc) {
			console.debug('DOM_parse_text_rep inner_exc :', inner_exc)
		}
	}
	if (is_DOM_parsererror(rep, false)) {  // true would console.error the error
		if (c_type.includes('xml')) {  // what was the source needing this ?
			// Sources needing decode / encode : LDM.no, Revue Ballast, Tel Quel (ma)
			txt_rep = HTML_decode_entities(txt_rep)
			// here '&' aren't encoded in URL anymore
			txt_rep = XML_encode_UTF8(txt_rep)
			txt_rep = drop_itunes_namespace(txt_rep)
			rep = DOM_parser.parseFromString(txt_rep, c_type)
		}
		if (is_DOM_parsererror(rep, true))
			// return null
			throw 'DOM parser error'
	}
	// console.log(rep)
	return rep
}
/**
 *
 */
function drop_itunes_namespace(txt) { // 2023: Science.org RSS miss this namespace declaration
	return txt.replace(/<itunes:.*?(\/>|<\/itunes:.{2,9}?>)/ig, '')
}
export function find_RSS_variant(items, src_def, DOM_parser) { /*eslint-disable dot-notation*/
	let nb = 0
	const src_lite = {tags: src_def.tags}
	for (const itm of items) {
		src_def = sµ.extend_source(sµ.XML_SRC['RSS_image:image'], src_lite)
		// if (scrap('r_img_title',itm, src_def, DOM_parser, nb, errors, to_txt, silent))
		if (scrap('r_img_title',itm, src_def, DOM_parser, nb, [], 1, 1)) return 'RSS_image:image'
		src_def = sµ.extend_source(sµ.XML_SRC['RSS_enclosure'], src_lite)
		if (scrap('r_img_src',  itm, src_def, DOM_parser, nb, [], 1, 1)) return 'RSS_enclosure'
		src_def = sµ.extend_source(sµ.XML_SRC['RSS_media:thumbnail'], src_lite)
		if (scrap('r_img_src',  itm, src_def, DOM_parser, nb, [], 1, 1)) return 'RSS_media:thumbnail'
		src_def = sµ.extend_source(sµ.XML_SRC['RSS_media:content'], src_lite)
		if (scrap('r_img_src',  itm, src_def, DOM_parser, nb, [], 1, 1)) return 'RSS_media:content'
		console.log('r_img_src', scrap('r_img_src',  itm, src_def, DOM_parser, nb, [], 1, 1))
		// 2023-07-13 : added r_img_src_re in image-less XML source definitions
		//if (i.innerHTML.includes('img') && i.innerHTML.includes('src'))
		//  console.info('XML from', src_def.tags.name, 'might contain undetected img', i)
		src_def = sµ.extend_source(sµ.XML_SRC['RSS_dc:date'], src_lite)
		if (scrap('r_dt',       itm, src_def, DOM_parser, nb, [], 1, 1)) return 'RSS_dc:date'
		src_def = sµ.extend_source(sµ.XML_SRC['RSS_content:encoded'], src_lite)
		if (scrap('r_txt',      itm, src_def, DOM_parser, nb, [], 1, 1)) return 'RSS_content:encoded'
		src_def = sµ.extend_source(sµ.XML_SRC['ATOM_content'], src_lite)
		if (scrap('r_txt',      itm, src_def, DOM_parser, nb, [], 1, 1)) return 'ATOM_content'
		src_def = sµ.extend_source(sµ.XML_SRC['ATOM'], src_lite)
		if (scrap('r_txt',      itm, src_def, DOM_parser, nb, [], 1, 1)) return 'ATOM'
		nb += 1
	} /* eslint-enable dot-notation */
	return 'RSS'
}
function set_RSS_variant(items, src_def, DOM_parser) {
	src_def.xml_type = find_RSS_variant(items, src_def, DOM_parser)
	// console.debug('RSS_variant', src_def.xml_type)
	const built_rss_src = sµ.extend_source(src_def, sµ.XML_SRC[src_def.xml_type])
	// console.log({built_rss_src})
	return built_rss_src
}
