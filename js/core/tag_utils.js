// SPDX-FileName: ./tag_utils.js
// SPDX-FileCopyrightText: 2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

import * as arr from '../../js/lib/js/array.js'
import { is } from '../../js/lib/js/types.js'

/*
 *
 */
export const tag_categ = [
	'tech',
	'lang',
	'country',
	'themes',
	'src_type',
	'res_type'
]
/*
 *
 */
export const tags = Object.fromEntries(tag_categ.map(elt => [elt, '']))
/**
 * Create an object with the default search tags.
 * @returns {Object} the tags
 */
export function get_default_tags(user_lang) {
	return {
		'tech': ['fast', 'one word'],
		'lang': [user_lang.slice(0, 2).toLowerCase()],
		'country': [],
		'themes': [],
		'src_type': ['Press'],
		'res_type': [],
		'del_src_sel': [],
		'add_src_sel': []
	}
}
/**
 * Reset tags, remove all the search settings.
 */
function get_empty_json_tags() {
	const json_obj = {}
	for (const tag of tag_categ)
		json_obj[tag] = []
	json_obj.del_src_sel = []
	json_obj.add_src_sel = []
	return json_obj
}
/*
 *
 */
export function QS_to_JSON(QUERYSTRING) {  // a generic version is a bad idea security-wise
	// inputs must be sanitized
	const json_obj = get_empty_json_tags()
	for (const [key, value] of QUERYSTRING) {
		if (is(json_obj[key]))
			json_obj[key].push(value)
	}
	return json_obj
}
/*
 *
 */
export function tag_sel_src_keys (source_objs, source_keys, tags) {
	// console.log('tag_sel_src_keys', {tags}, {source_objs}, {source_keys})
	const new_src_sel = {}
	for (const tag_name of tag_categ) {
		new_src_sel[tag_name] = {}
		if (!is(tags[tag_name])) continue
		for (const selected_tag of tags[tag_name]) {
			if (tag_name !== 'tech') { // inner accumulation
				for (const src_key of source_keys)
					if (Array.isArray(source_objs[src_key].tags[tag_name])) {
						if (source_objs[src_key].tags[tag_name].includes(selected_tag))
							new_src_sel[tag_name][src_key] = ''
					} else {
						if (source_objs[src_key].tags[tag_name] === selected_tag)
							new_src_sel[tag_name][src_key] = ''
					}
			} else { // inner intersection
				const cur_tag_keys = []
				for (const src_key of source_keys)
					if (source_objs[src_key].tags[tag_name].includes(selected_tag))
						cur_tag_keys.push(src_key)
				if (Object.keys(new_src_sel.tech).length === 0) {
					for (const cur_tag_key of cur_tag_keys)
						new_src_sel.tech[cur_tag_key] = ''
					continue
				} else {
					const prec_tag_keys = Object.keys(new_src_sel[tag_name])
					new_src_sel[tag_name] = {}
					for (const src_key of prec_tag_keys.filter(e => cur_tag_keys.includes(e)))
						new_src_sel[tag_name][src_key] = ''
				}
			}
			if (tags[tag_name].length > 0 &&
				Object.keys(new_src_sel[tag_name]).length === 0 &&
				tags.add_src_sel.length === 0
			) {
				return []
			}
		}
	}
	let new_src_sel_keys = []
	if (!tags.tech || !tags.tech.includes('cherry-pick_sources')) {
		new_src_sel_keys = source_keys
		let tag_sel_keys = []
		for (const tag_name of tag_categ) { // get the smallest ensemble
			if (new_src_sel_keys.length === 0) break
			tag_sel_keys = Object.keys(new_src_sel[tag_name])
			if (tag_sel_keys.length > 0)
				new_src_sel_keys = new_src_sel_keys.filter(v => tag_sel_keys.includes(v))
		}
	}
	if (new_src_sel_keys.length > tags.del_src_sel.length || tags.add_src_sel.length > 0) {
		for (const elt of tags.del_src_sel) arr.array_remove_once(new_src_sel_keys, elt)
		for (const elt of tags.add_src_sel) arr.array_add_once(new_src_sel_keys, elt)
	} else {
		new_src_sel_keys = []
	}
	return new_src_sel_keys
}
