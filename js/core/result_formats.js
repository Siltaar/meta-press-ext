// SPDX-FileName: ./js/core/result_formats.js
// SPDX-FileCopyrightText: 2024 Simon Descarpentres <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

import { img_tag } from '../mp_snippets.js'

import { tag_categ } from '../core/tag_utils.js'

import * as XML from '../lib/js/XML.js'
import { ndt }  from '../lib/js/date.js'
import { is_str } from '../lib/js/types.js'

import * as csv from '../deps/csv.min.js'

export function res_in_JSON(tags, findings, meta_findings, search_terms) {
	return `{
  "filters": ${JSON.stringify(tags)},
  "findings": ${JSON.stringify(findings)},
  "meta_findings": ${JSON.stringify(meta_findings)},
  "search_terms": ${JSON.stringify(search_terms)}
}`
}

function title_line(v) {
	if (!is_str(v.f_h1)) return v.f_source
	const src_prefix = `[${v.f_source}]`
	return v.f_h1.indexOf(src_prefix) !== -1 ? `${src_prefix} ${v.f_h1}` : `${v.f_h1}`
}
/*
 *
 */
function tags_to_XML(tags, XML_type) {
	let xml_tags = '', tag_val
	for (const tag_name of tag_categ) {
		tag_val = tags[tag_name]
		switch(typeof(tag_val)) {
			case 'string': xml_tags += XML_tag_in_cat(XML_type, tag_name, tag_val) ;break
			case 'object': for (const val of tag_val)
					xml_tags += XML_tag_in_cat(XML_type, tag_name, val) ;break
		}
	}
	for (const val of tags.del_src_sel) xml_tags += XML_tag_in_cat(XML_type, 'del_src_sel', val)
	for (const val of tags.add_src_sel) xml_tags += XML_tag_in_cat(XML_type, 'add_src_sel', val)
	return xml_tags
}

function XML_tag_in_cat(XML_type, tag_name, tag_value) {
	tag_name = XML.encode_XML(tag_name)
	tag_value = XML.encode_XML(tag_value)
	if ('ATOM' === XML_type) return `<category term='${tag_value}' label='mp__${tag_name}' />\n`
	return `<category>mp__${tag_name}__${tag_value}</category>\n`
}

export function res_in_RSS (tags, findings, search_terms, user_lang, version) {
	let flux_items = []
	for (const val of findings) {
		flux_items += `<item>
				<title>${XML.encode_XML(title_line(val))}</title>
				<description>${XML.encode_XML(val.f_txt)}</description>
				<media:thumbnail xmlns:media="http://search.yahoo.com/mrss/"
					url="${XML.encode_XML(val.mp_data_img)}"/>
				<link>${XML.encode_XML(val.f_url)}</link>
				<pubDate>${new Date(val.f_ISO_dt).toUTCString()}</pubDate>
				<dc:creator>${XML.encode_XML(val.f_by)}</dc:creator>
				<guid>${XML.encode_XML(val.f_url)}</guid>
				<dc:identifier>${val.mp_data_key}</dc:identifier>
			</item>`
	}
	return `<?xml version='1.0' encoding='UTF-8'?>
		<rss version='2.0' xmlns:dc='http://purl.org/dc/elements/1.1/'>
			<channel>
				<title>Meta-Press.es : ${search_terms}</title>
				<description>Selection RSS 2.0</description>
				<link>https://www.meta-press.es</link>
				<lastBuildDate>${ndt().toUTCString()}</lastBuildDate>
				<language>${user_lang}</language>
				<generator>Meta-Press.es (v${version})</generator>
				<docs>http://www.rssboard.org/rss-specification</docs>
				${tags_to_XML(tags, 'RSS')}
				${flux_items}
			</channel>
		</rss>`
}
/*
 * https://validator.w3.org/feed/#validate_by_input
 * https://tools.ietf.org/html/rfc4287
 */
export function res_in_ATOM (tags, findings, search_terms, version) {
	let flux_items = '', v = {}, img=''  // no clean way to embed media in results
	for (let val of findings) {
		if (val.f_img_src || val.mp_data_img)
			img = img_tag(val.mp_data_img, val.mp_data_alt, val.mp_data_title)
		else
			img = ''
		flux_items += `<entry>
				<title>${XML.encode_XML(title_line(val))}</title>
				<summary type='html'>${XML.encode_XML(img)}${XML.encode_XML(val.f_txt)}</summary>
				<published>${val.f_ISO_dt}</published>
				<updated>${val.f_ISO_dt}</updated>
				<link href='${XML.encode_XML(val.f_url)}'/>
				<author><name>${XML.encode_XML(val.f_by)}</name></author>
				<contributor>
					<name>${XML.encode_XML(val.f_source_title)}</name>
					<uri>${XML.encode_XML(val.mp_data_key)}</uri>
				</contributor>
				<id>${XML.encode_XML(val.f_url)}</id>
			</entry>`
	}
	return `<?xml version='1.0' encoding='UTF-8'?>
		<feed xmlns='http://www.w3.org/2005/Atom'>
			<title>Meta-Press.es : ${search_terms}</title>
			<subtitle>Decentralize press search engine</subtitle>
			<link href='https://www.meta-press.es'/>
			<generator uri='https://www.meta-press.es'>Meta-Press.es (v${version})</generator>
			<updated>${ndt().toISOString()}</updated>
			<author><name>Meta-Press.es</name></author>
			<id>https://www.meta-press.es/</id>
			${tags_to_XML(tags, 'ATOM')}
			${flux_items}
		</feed>`
}

export function res_in_CSV (findings) {
	const arr = [['ISO_date', 'Source', 'Source_key', 'Favicon', 'URL', 'title', 'txt',
		'by', 'img_src', 'img_alt', 'img_title']]
	for (const val of findings) {
		let img = ''
		let alt = ''
		let title = ''
		if (val.mp_data_img) {
			img = val.mp_data_img || ''
			alt = val.mp_data_alt || ''
			title = val.mp_data_title || ''
		}
		arr.push([val.f_ISO_dt, val.f_source, val.mp_data_key, val.f_icon, val.f_url, val.f_h1,
			val.f_txt, val.f_by, img, alt, title])
	}
	return  csv.stringify(arr)
}
