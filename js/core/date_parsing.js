// SPDX-FileName: date_parsing.js
// SPDX-FileCopyrightText: 2017-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

import { regextract } from './web_scraping.js'
import { get_browser_locale, lang_month_nb } from '../lib/browser/BOM_utils.js'
import { timezoned_date } from '../lib/js/date.js'
import { is_str } from '../lib/js/types.js'

/*
 *
 */
function get_src_dt_locale(src_def) {
	const date_locale = src_def.tags.date_locale
	if (date_locale) {
		if (date_locale === 'browser')
			return get_browser_locale()
		return date_locale
	}
	return `${src_def.tags.lang}-${src_def.tags.country}`
}

/**
 * Parses a date string from a website in a Date object with the given regex and format.
 * The format is in general `years-months-days hours:minutes`.
 * If months is not a number (but a word) use {} to translate this month name in its number:
 * $years-{$months}-$days $hours:minutes.
 * Another posible format is : "$number (sec|min|hour|today|day|yesterday|week|month|year)"
 * for the date like: 5 minutes ago.
 * @param {string} dt_str The date to parse
 * @param {string} tz The timezone of the date
 * @param {Array} f_nb The format "$4-{$3}-$2 $1"
 * @returns {Date}
 */
export function parse_dt_str(dt_str, tz, f_nb, src_def, dbg=false) {
	let dt = undefined
	let cnt = 1
	let cur_fmt = src_def[`r_dt_fmt_${cnt}`]
	let dt_repl = ''
	dbg && console.debug('dt before', {dt_str})
	if (cur_fmt) {
		do {
			if(typeof(dt_str) === 'number')
				dt_str = String(dt_str)
			dt_repl = regextract(cur_fmt[0], dt_str, cur_fmt[1])
			dbg && console.debug(`dt tried fmt_${cnt} [${cur_fmt}] got`, {dt_repl})
			if (dt_repl !== '')
				dt = sub_parse_dt_str(dt_repl, tz, f_nb, get_src_dt_locale(src_def), dbg)
			cnt += 1
		} while ((cur_fmt = src_def[`r_dt_fmt_${cnt}`]) && isNaN(dt))
	} else {
		dt = sub_parse_dt_str(dt_str, tz, f_nb, get_src_dt_locale(src_def), dbg)
	}
	if (isNaN(dt)) throw new Error(`'${dt_str}' (${tz}) is an invalid new Date()`)
	dbg && console.debug('dt after', dt_str, {dt})
	return dt
}
/**
 * With a formated date, tries to create a Date object.
 * @param {string} dt_str the date formated
 * @param {string} tz the timezone
 * @param {string} f_nb the format
 * @returns {Date} The foud date
 */
function sub_parse_dt_str(dt_str, tz, f_nb, lang, dbg=false) {
	let dt = NaN
	dbg && console.debug('sub_parse_dt_str(dt_str, tz, f_nb, lang)', dt_str, tz, f_nb, lang)
	if (is_str(dt_str))  // JSON timestamps are integers
		dt_str = dt_str.replace(/{(.+?)}/, (_, $1) => lang_month_nb(lang, $1))
	if (MM_DD_ONLY_RE.test(dt_str)) {
		const today = timezoned_date('', tz)
		const cur_month = today.getMonth()  // starts at 0 == january
		if (cur_month < Number(dt_str.replace(MM_DD_ONLY_RE.test, '$1')))
			dt_str = `${today.getFullYear()-1}${dt_str}`
		else
			dt_str = `${today.getFullYear()}${dt_str}`
	} else if (HH_MM_ONLY_RE.test(dt_str)) {
		const today = timezoned_date('', tz)
		dt_str = `${today.getFullYear()}-${today.getMonth()+1}-${today.getDate()} ${dt_str}`
	}
	dbg && console.info('sub_parse_dt_str dt_str', dt_str)
	dt = timezoned_date(dt_str, tz)
	dbg && console.info('dt = timezoned_date(dt_str, tz)', dt)
	if(isNaN(dt) && /\d{12,20}/i.test(dt_str)) {
		dbg && console.debug('found it\'s a timestamp')
		dt = new Date(Number(dt_str))
	}
	if(isNaN(dt) && ['sec','min','hour','today','day','yesterday','week','month','year'].some(
		elt => dt_str.includes(elt))
	) {
		dt = timezoned_date('', tz)
		const regex_nb = /[^\d]*(\d+)[^\d]*/
		if (/sec/i.test(dt_str)) {
			dt.setSeconds(dt.getMinutes() - Number(dt_str.replace(/sec?.*/, '')))
		} else if (/min/i.test(dt_str)) {
			dt.setMinutes(dt.getMinutes() - Number(dt_str.replace(/min?.*/, '')))
		} else if (/hour/i.test(dt_str)) {
			dt.setHours(dt.getHours() - Number(dt_str.replace(/hour.*/, '')))
		} else if (/yesterday/i.test(dt_str)) {
			dt.setDate(dt.getDate() - 1)
			dt.setMinutes(0)
			dt.setSeconds(0)
		} else if (/today/i.test(dt_str)) {
			dt.setMinutes(0)
			dt.setSeconds(0)
		} else if (/day/i.test(dt_str) ) {
			dt.setDate(dt.getDate() - Number(dt_str.replace(regex_nb, '$1')))
		} else if (/week/i.test(dt_str) ) {
			dt.setDate(dt.getDate() - (Number(dt_str.replace(regex_nb, '$1')) * 7))
		} else if (/month/i.test(dt_str) ) {
			dt.setMonth(dt.getMonth() - Number(dt_str.replace(regex_nb, '$1')))
		} else if (/year/i.test(dt_str) ) {
			dt.setFullYear(dt.getFullYear() - Number(dt_str.replace(regex_nb, '$1')))
		}
	}
	if (dt && !dt.getMinutes() && !dt.getSeconds()) {  // no time set ? put an ordered one
		if (HH_MM_RE.test(dt_str)) {
			const d_split = regextract(HH_MM_STR, dt_str).split(':')
			dt.setHours(Number(d_split[0]))
			dt.setMinutes(Number(d_split[1]))
		} else {
			dt.setHours(0)
			dt.setMinutes(f_nb % 60)
		}
	}
	return dt
}
/**
 * An arbitrary french internationalisation number format. What's important is to use the same
 * anywhere. Meta-Press.es has been created by a french guy.
 * @type {Intl.NumberFormat}
 */
/* const intlNum = Intl.NumberFormat('fr', {
		minimumIntegerDigits: 4,
		useGrouping: 0,
		signDisplay: 'always'})
		*/
// const intl_datetime_format = new Intl.DateTimeFormat('fr',
//  {timeZoneName: 'short', timeZone: tz})
/**
 * Extract an HTML tag attribute attr from a string str supposed to contain HTML.
 * @param {string} attr Attribute to extract
 * @param {string} str The string to search in
 * @returns {string} The value of the 1st attribute
 */
/*export function extract_val_attr (attr, str) {
		let str2 = regextract(` ${attr}=["'](.*?)["']`, str)
		return (str2 === str ? '' : str2)
	}*/
/**
 * The string to build a regular expression to extract hour:minutes from a date.
 * @type {string}
 */
export const HH_MM_STR = '(\\d\\d?:\\d\\d)'
/**
 * The regular expression allowing to extract hour:minutes from a date.
 * @type {RegExp}
 */
export const HH_MM_RE = new RegExp(`${HH_MM_STR}`, 'i')
/**
 * The regex to extract a string with just hour:minutes.
 * @type {RegExp}
 */
export const HH_MM_ONLY_RE = new RegExp(`^${HH_MM_STR}$`, 'i')
/**
 * The string to build a regular expression to extract -month-day in a date.
 * @type {string}
 */
export const MM_DD_ONLY_STR = '^-(\\d{1,2})-(\\d{1,2})'
/**
 * The regular expression to extract -month-day in a date.
 * @type {RegExp}
 */
export const MM_DD_ONLY_RE = new RegExp(MM_DD_ONLY_STR, 'i')

