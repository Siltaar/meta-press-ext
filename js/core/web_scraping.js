// SPDX-FileName: web_scraping.js
// SPDX-FileCopyrightText: 2017-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
/* globals console */

import * as sµ from './source_utils.js'
import { is, is_str, is_arr } from '../lib/js/types.js'
import { str_uniq_line, triw } from '../lib/js/text.js'

import { strip_HTML_tags } from '../lib/DOM_parser.js'
import { evaluateXPath } from '../lib/XPath_evaluator.js'

/**
 * Extract the given regex matching group(s) and return it (them)
 * @param {string} re The matching groups of the regular expression to apply, everything before
 * and after them will be matched outside replacement groups and discarded.
 * @param {string} str The string to search in
 * @param {string} repl The replacement tokens used to keep some matching groups (default : $1)
 * @param {string} p regular expression string that matches the smallest
 * ensemble of every possible characters, not creating a match group
 * @returns {string} the input string with tokens replaced
 */
export function regextract (re, str, repl='$1', pad='(?:.*?)') {
	str = str_uniq_line(str)
	const str_rep = str.replace(new RegExp(`^${pad}${re}${pad}$`), repl)
	return str_rep !== str ? str_rep : ''  // if the match/replace failed, return an empty string
}
/**
 * Format the string with tokens : $1 -> $i.
 * @example str_fmt('$1-ok', ["ko"]) return 'ko-ok'
 * @param {string} a The string to format
 * @param {Array} tokens The tokens to use
 * @returns {string} The formated string
 */
export function str_fmt (str, tokens) {
	// console.log('str_fmt', str, tokens)
	for (const [key, val] of Object.entries(tokens)) {
		str = str.replace(new RegExp(`\\$${Number(key)+1}`, 'g'), String(val || ''))
	}
	return str
}
/**
 * Get the value at the given path of a JSON JSON object
 * @param {string} JSON_path The JSON path like : sources.invidius.tags.name
 * @param {Object} JSON_obj The object to search in
 * @returns The found value or undefined
 */
export function deref_JSON_path(JSON_path, JSON_obj) {
	const dbg = true
	let val = JSON_obj
	if (JSON_path)
		for (const JSON_path_elt of JSON_path.split(' -> ')) {  // all char is valid JSON key char
			// dailytelegraph.com.au is using '.' in keys
			// others are using '/'
			// lenouvellist.ch : 'aio:urls'
			// if '->' is colliding with a valid key, we can try : ':{' (a JSON object aperture)
			try {
				val = val[JSON_path_elt]
			} catch (exc) {
				dbg && console.debug('deref_JSON_path failure', JSON_path, {JSON_obj}, exc)
				return ''
			}
		}
	return val
}
/**
 * Parsing lookup
 */
export function scrap(elt, dom, src, DOM_parser, f_nb=0, errors=[], do_txt=true, silent=false) {
	const local_dbg=false
	// if (elt == 'r_img_src') local_dbg=true
	local_dbg && console.debug('scrap(elt, dom, src, f_nb…)', elt, dom, src, f_nb)
	let val
	const src_elt = src[elt]
	if (src.type === 'JSON') {
		if (!is(src_elt)) return '' // we need undef src_elt to seek _xpath
		if (Array.isArray(src_elt)) {
			try {
				const val_lst = []
				for (const val_idx of src_elt)
					val_lst.push(strip_HTML_tags(deref_JSON_path(val_idx, dom) || '', DOM_parser) || '')
				val = str_fmt(src[`${elt}_tpl`], val_lst)
			} catch (exc) {
				!silent && errors.push(sµ.create_error(src, sµ.error_code.ERROR,
					`${f_nb}: ${elt} ${exc}`, sµ.error_status.WARNING, 'test_src', {}, 'log'))
				// add_err(src, 0, `${f_nb}: ${elt} ${exc}`, 'warn', 'test_src')
				// console.debug(src.tags.name, {src}, `${f_nb}: ${elt}`, exc, {exc})
			}
		} else {
			try {
				val = deref_JSON_path(src_elt, dom) || ''
				if (elt !== 'results' && Array.isArray(val)) {  // if wanted elt 'results' return them
					let val_acc = ''
					const val_attr = src[`${elt}_attr`]
					for (const val_elt of val) {
						if (is_str(val_attr)) {
							val_acc += `${val_elt[val_attr]}, `
						} else {
							val_acc += val_elt
						}
					}
					val = val_acc
				}
			} catch (exc) {
				!silent && errors.push(sµ.create_error(src, sµ.error_code.ERROR,
					`${f_nb}: ${elt} ${exc}`, sµ.error_status.WARNING, 'test_src', {}, 'log'))
			}
		}
	} else {
		if (is_str(src_elt)) {
			val = dom.querySelector(src_elt)
			local_dbg && console.debug('val querySelector', val, 'for src_elt', src_elt)
			// local_dbg && val && console.debug('val', Object.keys(val), 'for src_elt', src_elt)
			// if (val) for (const key of Object.keys(val)) console.info (val[key])
		} else if (Array.isArray(src_elt)) {
			try {
				return lookup_tpl(dom, src, elt, src_elt, DOM_parser, null, do_txt)
			} catch (exc) {
				!silent && errors.push(sµ.create_error(src, sµ.error_code.ERROR,
					`${f_nb}: ${elt} ${exc}`, sµ.error_status.WARNING, 'test_src', {}, 'log'))
			}
		} else {
			const elt_xpath = src[`${elt}_xpath`]
			if (is_str(elt_xpath)) {
				val = evaluateXPath(dom, elt_xpath)
			} else if (Array.isArray(elt_xpath)) {
				try {
					return lookup_tpl(dom, src, elt, elt_xpath, DOM_parser, 'xpath', do_txt)
				} catch (exc) {
					!silent && errors.push(sµ.create_error(src, sµ.error_code.ERROR,
						`${f_nb}: ${elt} ${exc}`, sµ.error_status.WARNING, 'test_src', {}, 'log'))
				}
			} else {
				return '' // `Missing ${elt} (or ${elt}_xpath) in ${src.tags.name}, or not string.`)
			}
		}
		const src_elt_attr = src[`${elt}_attr`]
		if (is_str(src_elt_attr)) {
			let val_attr
			try {
				// val_attr = val.attributes[src_elt_attr]
				// val = strip_HTML_tags(val_attr && val_attr.value || val[src_elt_attr] || '',
				//	DOM_parser)
				val_attr = val.getAttribute(src_elt_attr)
				// val = strip_HTML_tags(val_attr || val[src_elt_attr] || '', DOM_parser)
				val = strip_HTML_tags(val_attr || '', DOM_parser)
			} catch (exc) {
				val = ''
				!silent && errors.push(sµ.create_error(src, sµ.error_code.ERROR,
					`${f_nb}: ${elt} ${exc}`, sµ.error_status.WARNING, 'test_src', {}, 'log'))
			}
			// } else if (! Array.isArray(src_elt)) { // is it possible to be an array here ?
			// } else if (typeof(src_elt) !== 'object') { // this prevents XPath objects to be treated
		} else {
			try {
				val = triw(val.textContent)
			} catch (exc) {
				val = (elt === 'r_dt') ? '100000000000' : ''  // date : 1973-03-03 09:46:40 GMT+0000
				!silent && errors.push(sµ.create_error(src, sµ.error_code.ERROR,
					`${f_nb}: ${elt} ${exc}`, sµ.error_status.WARNING, 'test_src', {}, 'log'))
			}
		}
	}
	// local_dbg && console.debug(elt, 'value before _re', val)
	// if (elt == 'r_img_src') console.log('before', val)
	const elt_re = src[`${elt}_re`]  // it should be a list of 2 elements
	if (is_arr(elt_re)) {  // test via Array.isArray because typeof(null) === 'object'
		if (is_str(val)) {
			val = regextract(elt_re[0], val, elt_re[1])
		} else {  // let's hope it's a Number
			const val_type = typeof (val)
			val = String(val)
			val = regextract(elt_re[0], val, elt_re[1])
			if (val_type === 'number')
				val = Number(val)
		}
	}
	// local_dbg && console.debug(elt, 'value before strip_HTML_tags', val)
	if (val && do_txt)
		val = strip_HTML_tags(val, DOM_parser)
	local_dbg && console.debug(elt, 'returned val', val)
	return val
}
/**
 *
 */
function lookup_tpl (dom, src, elt, src_elt, DOM_parser, is_xpath=false, do_txt=true) {
	const val_lst = []
	const val_attr = src[`${elt}_attr`]
	let val_lst_len
	const local_dbg = false
	for (const val_idx of src_elt) {
		if (is_xpath) {
			val_lst.push(evaluateXPath(dom, val_idx) || '')
		} else {
			val_lst.push(dom.querySelector(val_idx) || '')
		}
		val_lst_len = val_lst.length - 1
		if (Array.isArray(val_attr) && val_attr[val_lst_len]) {
			// const tpl_elt_attrs = val_lst[z].attributes[val_attr[z]]  // Deno can't .attributes
			const tpl_elt_attrs = val_lst[val_lst_len].getAttribute(val_attr[val_lst_len])
			val_lst[val_lst_len] = strip_HTML_tags(tpl_elt_attrs || '', DOM_parser)
		} else {
			val_lst[val_lst_len] = triw(val_lst[val_lst_len].textContent)
		}
	}
	local_dbg && console.debug('val_lst', val_lst)
	let val = str_fmt(src[`${elt}_tpl`], val_lst)
	if (val && do_txt) val = strip_HTML_tags(val, DOM_parser)
	local_dbg && console.debug(`${elt}_tpl returned:`, val)
	return val
}
/**
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions
 * $& means the whole matched string
 */
export function escape_RegExp(string) { return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&') }
