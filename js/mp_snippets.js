// SPDX-FileName: ./mp_snippets.js
// SPDX-FileCopyrightText: 2014-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
/* globals document */

import { ndt_human_readable } from './lib/js/date.js'

export function export_filename(mp_version, file_type) {
	return `${ndt_human_readable()}_meta-press.es_v${mp_version}.${file_type}`
}
/**
 * Creates an img HTML tag in a string filled with the given args.
 * @param {string} src The image url
 * @param {string} alt The image alt
 * @param {string} title The image title
 * @returns {string} The img balise filled
 */
export function img_tag(src, alt, title) {
	return `<img src="${encodeURI(src)}" alt="${alt}" title="${title}"/>`
}

export async function fetch_manifest () {
	const raw = await fetch('manifest.json')
	return await raw.json()
}
