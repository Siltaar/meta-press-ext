// SPDX-FileName: ./background.js
// SPDX-FileCopyrightText: 2017-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

import { is } from '../lib/js/types.js'
import * as xµ from '../lib/webext/webext_utils.js'
import { build_src, get_raw_src } from '../core/source_utils.js'
import { init_table_sch_s, create_alarm } from '../lib/scheduled_searchs.js'

if (typeof browser == 'undefined') { // Chrome does not support the browser namespace yet.
	globalThis.browser = chrome        // 2024-10-09
}
browser.action.onClicked.addListener(() => browser.tabs.create({url:'/html/index.html'}))

/**
 * Executes in function call with a message.
 * @param {Object} data `function`'s name of the function and its `params` array of parameters
 * @param {Object} sender The information sender
 * @returns The result of the function or false
 */
/*
async function handle_function_calls(data, sender) {
	const dbg = false
	if(dbg) console.log(`request : ${data.function} ${data.params}, sender ${sender.tab.id}`)
	if (is_str(data.function) && window[data.function]) {
		let res = window[data.function].apply(null, data.params)
		if (is(res) && is(res.then))
			res = await res
		return res
	}
	return false
}
browser.runtime.onMessage.addListener(handle_function_calls)
*/

/**
 * Cache.add: Request URL moz-extension://[…]/json/sources.json must be either http: or https:
 */
/*
caches.open('background').then(function(cache_handler) {
	cache_handler.add('/json/sources.json')
})*/
/**
 *
 */
async function cache_built_src() {
	const start_dt = new Date()
	const custom_src = await xµ.get_stored('custom_src', {})
	xµ.try_store('built_src', build_src(await get_raw_src(), custom_src))
	// xµ.update_need_reload_src()
	xµ.try_store('built_src_dt', new Date())
	console.log(`background json/sources.json loading time : ${new Date() - start_dt} ms`)
}
/*
window.bg_get_built_src = cache_built_src

let dt_need_reload_src = new Date()
function is_need_reload_src(dt_src_got) {
	return dt_need_reload_src > dt_src_got
}
window.bg_is_need_reload_src = is_need_reload_src
function update_need_reload_src() {
	dt_need_reload_src = new Date()
}
window.bg_update_need_reload_src = update_need_reload_src
*/
/**
 * Opens the welcome.html for new user or for an update.
 * @param {string} stored_version The current version
 */
function announce_updates (stored_version) {
	const manifest_version = browser.runtime.getManifest().version
	if (!is(stored_version)) {
		browser.tabs.create({'url': '/html/welcome.html'})
	} else {
		const do_announce_updates = true
		if (do_announce_updates)
			if (stored_version !== manifest_version)
				browser.tabs.create({'url': 'https://www.meta-press.es/category/journal.html'})
	}
	xµ.try_store('version', manifest_version)
}
browser.runtime.onInstalled.addListener(async ({reason}) => {
	if (reason === 'install') {
		await announce_updates(await xµ.get_stored('version', undefined))
	}
	if (reason === 'update') {
		// await announce_updates(await xµ.get_stored('version', undefined))
	}
})
/*
 *
 */
async function init() {
	await cache_built_src()
	// resolve_search_hosts(Object.keys(built_srcs), built_srcs)
	const stored_sch_sea = init_table_sch_s(await xµ.get_storage())
	if(Array.isArray(stored_sch_sea))
		for (const sch_s of stored_sch_sea)
			create_alarm(sch_s)
	xµ.check_local_storage_capacity()
}
init()
