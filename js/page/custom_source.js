// SPDX-FileName: ./custom_source.js
// SPDX-FileCopyrightText: 2018-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
//
/* globals console alert window navigator URL document */
import * as mµ from '../mp_utils.js'
import * as BOMµ from '../lib/browser/BOM_utils.js'
import * as sµ from '../core/source_utils.js'
import * as gtt from '../gettext_html_auto.js/gettext_html_auto.js'

mµ.set_theme()
const dbg = false
/**
 * Load the custom sources in the HTML document.
 * @memberof load_sources
 * @param {Object} custom_src The stored customs sources
 */
function load_custom_src(custom_src){
	dbg && console.log('custom_src', custom_src)
	if (typeof(custom_src) === 'object' && Object.keys(custom_src).length) {
		// BOMµ.id('custom_sources').textContent = JSON.stringify(custom_src, null, '\t')
		BOMµ.id('custom_sources').textContent = sµ.src_to_json(custom_src)
	} else {
		BOMµ.id('custom_sources').textContent = BOMµ.id('default_custom_sources').textContent
	}
}
BOMµ.id('save_custom_sources').addEventListener('click', async (evt) => {
	evt.preventDefault()
	const custom_src_value = BOMµ.id('custom_sources').value
	dbg && console.log('custom_src_value', custom_src_value)
	let custom_obj
	try {
		custom_obj = JSON.parse(custom_src_value)
	} catch (exc) {
		alert(`Error parsing user custom source (${exc}).`)
		return
	}
	await mµ.to_storage('custom_src', custom_obj)
	mµ.update_need_reload_src()
	window.location.reload()
})
BOMµ.id('reset_custom_sources').addEventListener('click', async () => {
	await mµ.to_storage('custom_src', {})
	mµ.update_need_reload_src()
})
BOMµ.id('copy_reformated_raw_src').addEventListener('click', (evt) => {
	evt.preventDefault()
	const raw_src = BOMµ.id('provided_sources').value
	navigator.clipboard.writeText(raw_src)
})
/**
 * Management of locally user-defined custom sources
 * @namespace new_update_sources
 */
async function init () {
	const userLang = await mµ.get_wanted_locale()
	const QUERYSTRING = new URL(window.location).searchParams
	if (QUERYSTRING.get('xgettext'))
		await gtt.xgettext_html()
	/*const mp_i18n = */await gtt.gettext_html_auto(userLang)
	BOMµ.id('provided_sources').textContent = sµ.src_to_json(await sµ.get_raw_src())
	load_custom_src(await mµ.get_custom_src())
	document.body.classList.add('javascript_loaded')
}
init()
