// SPDX-FileName: ./index.js
// SPDX-FileCopyrightText: 2017-2024 Simon Descarpentres <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
import * as mµ 		from '../mp_utils.js'

mµ.set_theme() // exceptional priority because to shorten the background flash (000/FFF)

if (typeof(browser) !== 'undefined')
	not_just_frontend()  // exceptionnal priority also

import { export_filename } from '../mp_snippets.js'
import { res_in_JSON, res_in_RSS, res_in_CSV, res_in_ATOM } from '../core/result_formats.js'
import * as mp    from '../core/source_fetching.js'
import { scrap }  from '../core/web_scraping.js'
import * as sµ 		from '../core/source_utils.js'
import { tag_categ, tags, get_default_tags, tag_sel_src_keys,
	QS_to_JSON }    from '../core/tag_utils.js'

import { evaluateXPath } from '../lib/XPath_evaluator.js'
import { DNS_resolve } 	from '../lib/DNS_resolver.js'
import * as BOMµ	from '../lib/browser/BOM_utils.js'  // Browser Object Model
import * as DOMµ 	from '../lib/DOM_parser.js'
import * as st 		from '../lib/source_testing.js'
import * as ss 		from '../lib/scheduled_searchs.js'

import { uuidv4 } from '../lib/js/UUID.js'
import { is, is_str, is_obj } from '../lib/js/types.js'
import { triw }   from '../lib/js/text.js'
import { rnd, pick_between } from '../lib/js/math.js'
import { ndt, ndt_human_readable, delay } from '../lib/js/date.js'
import { deep_copy } from '../lib/js/object.js'
import * as µrl 	from '../lib/js/URL.js'
import * as arr   from '../lib/js/array.js'
import * as XML 	from '../lib/js/XML.js'

import * as gtt		from '../gettext_html_auto.js/gettext_html_auto.js'
import * as csv		from '../deps/csv.min.js'

function not_just_frontend() {
	mµ.get_child_mode().then((child_mode) => {
		if (!child_mode) {
			BOMµ.id('cog_settings').classList.toggle('display_none')
			BOMµ.$('.add_src').classList.toggle('display_none')
			BOMµ.id('mp_autosearch_permalink').classList.toggle('display_none')
			BOMµ.id('a_propos').classList.toggle('display_none')
			BOMµ.id('report_feedback').classList.toggle('display_none')
			BOMµ.id('support_project').classList.toggle('display_none')
			BOMµ.id('footer_version_line').classList.toggle('display_none')
			BOMµ.id('blog-title').classList.toggle('display_none')
		}
	})
	BOMµ.id('load_headlines').classList.toggle('display_none')
	BOMµ.id('perm_line').classList.toggle('display_none')
}
const META_FINDINGS_PAGE_SIZE = 15
const dynamic_css = BOMµ.createStyleSheet()
const CSS_CHECKBOX_RULE_INDEX = dynamic_css.insertRule('.f_selection {display:none}', 0)
const QUERYSTRING = new URL(window.location).searchParams
const TEST_SRC = QUERYSTRING.get('test_sources')
const IMG_PLACEHOLDER = '/img/photo_loading_placeholder.png'
const SERIAL_QUERIES_DBG = false
const IMPORT_SLICE_SIZE = 750  // no big difference between 500 and 1000
const DOM_parser = new DOMParser()
const DBG = true
let dt_source_objs = new Date()
let source_objs
let MAX_RES_BY_SRC
let	HEADLINE_PAGE_SIZE
let user_lang
let LANG_NAME
let COUNTRY_NAME
let mp_i18n
let ALT_LOAD_TXT
let IS_LOAD_IMG
let IS_UNDUP
let IS_IMPORT_FILE
let manifest
let source_keys = []
let cur_src_sel = []
let current_source_nb = 0
let rest_search_source = [] // Marin
let fetch_abort_controller
const mp_req = {
	findingList: new List('findings', {
		// indexAsync: true,
		item: 'finding-item',
		valueNames: ['f_h1', 'f_txt', 'f_source', 'f_dt', 'f_by',
			{ name: 'f_source_title', attr: 'title' },
			// { name: 'mp_data_source', attr: 'mp-data-src'},  // is not modified then
			{ name: 'f_by_title', attr: 'title' },
			{ name: 'f_ISO_dt', attr: 'datetime' },
			{ name: 'f_h1_title', attr: 'title' },
			{ name: 'f_dt_title', attr: 'title' },
			{ name: 'f_img_src', attr: 'src'},
			{ name: 'f_img_alt', attr: 'alt'},
			{ name: 'f_img_title', attr: 'title'},
			{ name: 'mp_data_img', attr: 'mp-data-img'},
			{ name: 'mp_data_alt', attr: 'mp-data-alt'},
			{ name: 'mp_data_title', attr: 'mp-data-title'},
			{ name: 'mp_data_key', attr: 'mp-data-key'},
			{ name: 'f_url', attr: 'href' },
			{ name: 'f_icon', attr: 'src' },
		],
		page: 10,
		pagination: {
			outerWindow: 2,
			innerWindow: 3,
			item: '<li><a class="page" href="#findings"></a></li>'
		},
		// fuzzySearch: { distance: 1501 }, // Unused. 1000 would search through 400 char.
	}),
	metaFindingList: new List('meta-findings', { // List of source related info for a query
		// indexAsync: true,
		item: 'meta-findings-item',
		valueNames: [
			{ name: 'mf_icon', attr: 'src' },
			'mf_name',
			{ name: 'mf_title', attr: 'title' },
			{ name: 'mf_ext_link', attr: 'href' },
			// { name: 'mf_remove_source', attr: 'data-mf_remove_source' },
			{ name: 'mf_res_nb', attr: 'data-mf_res_nb' },
			{ name: 'mf_report', attr: 'data-mf_report_source'},
			'mf_locale_res_nb',
		],
		page: META_FINDINGS_PAGE_SIZE,
		pagination: {
			outerWindow: 10,
			innerWindow: 10,
			item: '<li><a class="page" href="#meta-findings"></a></li>'
		}
	}),
	running_query_countdown: 0,
	final_result_displayed: 0,
	query_result_timeout: null,
	query_final_result_timeout: null,
	query_start_date: null,
	query_abort_search_timeout : null
}
function update_nb_src() {
	current_source_nb = cur_src_sel.length
	BOMµ.id('cur_tag_sel_nb').textContent = current_source_nb
	mµ.get_nb_needed_host_perm(cur_src_sel, source_objs).then((obj) => {
		const span = BOMµ.id('cur_tag_perm_nb')
		span.textContent = obj.not_perm
		span.title = obj.names.toString()
	})
	const btn = BOMµ.id('mp_submit')
	if (current_source_nb === 0) {
		btn.disabled = true
		btn.style.cursor = 'not-allowed'
		btn.title = mp_i18n.gettext('Select at least one source to search in.')
	} else if (btn.disabled) {
		btn.disabled = false
		btn.style.cursor = ''
	}
}
/**
 * Display the image stored in mp-data-img.
 * @param {click} e Click event
 */
function load_result_image (e) {
	const attrs = e.target.attributes
	const mp_data_src = attrs['mp-data-img'].textContent
	const mp_data_alt = attrs['mp-data-alt'] ? attrs['mp-data-alt'].textContent : ''
	const mp_data_title = attrs['mp-data-title'] ? attrs['mp-data-title'].textContent : ''
	for (const img of BOMµ.$$(`img[mp-data-img='${mp_data_src}']`)) {
		img.src = mp_data_src
		img.alt = mp_data_alt
		img.title = mp_data_title
	}
}
BOMµ.id('mp_query').value = new URL(window.location).searchParams.get('q')
BOMµ.id('mp_query').addEventListener('keypress', function(evt) {
	if (!evt) evt = window.event
	const keyCode = evt.keyCode || evt.which
	if (keyCode === 13) {	// search on pressing Enter key
		BOMµ.id('mp_submit').click()
		return false	// returning false will prevent the event from bubbling up.
	}
})
function toggle_sentence_search_tags (a, b) {
	const sel_mul_tech = tags.tech
	const len_before = sel_mul_tech.getValue(true)
	sel_mul_tech.removeActiveItemsByValue(a)
	const len_after = sel_mul_tech.getValue(true)
	if (len_before > len_after) {
		sel_mul_tech.setChoiceByValue(b)
		on_tag_sel()
	}
}
function tick_query_duration() {
	const dt = (new Date() - mp_req.query_start_date) / 1000
	BOMµ.id('mp_running_duration').textContent = dt > 10 ? Math.floor(dt) : dt
	mp_req.duration_ticker = setTimeout(tick_query_duration, 1000)
}
function alert_perm() {
	alert(mp_i18n.gettext(BOMµ.id('need_perm_txt').textContent))
	if (BOMµ.id('tags_handle').textContent === '+')
		BOMµ.id('tags_handle').click()
	BOMµ.id('cur_tag_perm_nb').classList.add('draw_attention')
}
async function load_source (src_key, src_def, search_terms) {
	let json_src_rep
	try {
		json_src_rep = await mp.search_source (1, src_key, src_def, search_terms, DBG, user_lang,
			DOM_parser, fetch_abort_controller, MAX_RES_BY_SRC, TEST_SRC)
	} catch (exc) {
		json_src_rep = {'errors':
			sµ.create_error(
				src_def, sµ.error_code.ERROR, exc, sµ.error_status.ERROR, 'test_src', {}, 1)
		}
	}
	if (TEST_SRC) {
		if (json_src_rep.errors.length) {
			st.add_errors(json_src_rep.errors, opener.document)
		} else {
			st.no_err(src_def, src_def.tags.key, 'No error', 'test_src', opener.document)
		}
		opener.document.getElementById('update_stats').click()
	} else {
		if (BOMµ.id('mp_query').value !== json_src_rep.search_terms) {  // once per search, not per src
			import_JSON_request(json_src_rep)
		}
		mp_req.metaFindingList.add(json_src_rep.meta_findings)
		await import_JSON_results(json_src_rep)
	}
	update_search_query_countdown(src_key)
	console.log(src_key, mp_req.running_query_countdown + 1, '/', current_source_nb)
}
BOMµ.id('mp_submit').addEventListener('click', async () => {
	if (current_source_nb === 0) return
	if (BOMµ.id('mp_query').value === '') return
	BOMµ.id('mp_submit').disabled = true
	BOMµ.id('mp_submit').style.cursor = 'not-allowed'
	BOMµ.id('search_stats').classList.add('display_none')
	BOMµ.id('donation_reminder').classList.add('display_none')
	BOMµ.id('favicon').href = '/img/favicon-metapress-v2.png'
	mp_req.metaFindingList.search()
	if (typeof(browser) === 'undefined') {  // so we are a simple web page
		mp_req.query_start_date = ndt()
		const token = `${ndt().toISOString()}_${uuidv4()}`.replace(/:/g, '-')
		const params = gen_permalink().searchParams
		BOMµ.id('waiting_gif').classList.remove('display_none')
		document.body.classList.add('waiting')
		const rep = await mp.try_fetch(
			`../scripts/cgi/backend.cgi?backend=1&token=${token}&${params}`)
		BOMµ.id('waiting_gif').classList.add('display_none')
		document.body.classList.remove('waiting')
		BOMµ.id('mp_submit').disabled = false
		BOMµ.id('mp_submit').style.cursor = 'normal'
		if (rep.ok)
			sub_import_JSON(await rep.json())
		return
	}
	BOMµ.id('mp_stop').classList.remove('display_none')
	BOMµ.id('mp_stop').classList.add('display_inline')
	// BOMµ.id('list_src').classList.add('display_none')  // annoying when testing sources
	BOMµ.id('close_list_src').click()
	const elt_sto = BOMµ.id('mp_stop')
	if (elt_sto.disabled) {
		elt_sto.disabled = false
		elt_sto.textContent = mp_i18n.gettext('Stop')
		elt_sto.style.opacity = 1
	}
	mp_req.search_terms = BOMµ.id('mp_query').value
	if (mp_req.running_query_countdown !== 0 || '' === mp_req.search_terms)
		return // to prevent 2nd search during a running one
	try {
		mp_req.running_query_countdown = 1 // to prevent 2nd search during permission request
		if (!await mµ.request_sources_perm(cur_src_sel, source_objs)) {
			alert_perm()
			mp_req.running_query_countdown = 0
			return
		}
		const val = await mµ.get_nb_needed_host_perm(cur_src_sel, source_objs)
		BOMµ.id('cur_tag_perm_nb').textContent = val.not_perm
	} catch (exc) {
		console.warn('Ignored exception : ', exc)
	}
	mp_req.metaFindingList.search()
	document.body.classList.add('waiting')
	mp_req.running_query_countdown = current_source_nb
	if (!TEST_SRC){
		mp_req.query_start_date = new Date()
		mp_req.duration_ticker = setTimeout(tick_query_duration, 1000)
		clear_results()
	}
	rest_search_source = [...cur_src_sel]
	fetch_abort_controller = new AbortController()
	fetch_abort_controller.signal.onabort = () => {
		if (TEST_SRC) return
		BOMµ.id('mp_running_stats').style.display = 'none'
		BOMµ.id('mp_cancelling').style.display = 'block'
		elt_sto.disabled = true
		elt_sto.textContent = mp_i18n.gettext('Stopping…')
		elt_sto.style.opacity = 0.75
	}
	let time = parseInt(await mµ.get_search_timeout(), 10)
	if (time > 0) {
		if (TEST_SRC) time = 3 * time
		clearTimeout(mp_req.query_abort_search_timeout)
		mp_req.query_abort_search_timeout = setTimeout(fetch_abort_controller.abort, 1000 * time)
	}
	mp.search_sources(mp_req.search_terms, cur_src_sel, source_objs, load_source)
})
function update_search_query_countdown(current_s) { // Marin
	const tooltip_text = BOMµ.id('mp_tooltip_text')
	const btn_src = BOMµ.id('mp_running_btn_src')
	const btn_src_text = BOMµ.id('mp_running_btn_src_text')
	rest_search_source = rest_search_source.filter(s => s !== current_s)
	clearTimeout(mp_req.query_result_timeout)
	mp_req.running_query_countdown -= 1
	if(mp_req.running_query_countdown === 0) {
		clearTimeout(mp_req.query_abort_search_timeout)
		btn_src.classList.add('display_none')
		btn_src.classList.remove('display_inline')
		btn_src.textContent = '+'
		tooltip_text.classList.add('display_none')
		btn_src_text.classList.add('display_none')
		tooltip_text.innerHTML = ''
		btn_src_text.innerHTML = ''
		display_ongoing_results()
	} else if (mp_req.running_query_countdown < 30 && !TEST_SRC) {
		let htm = `<h4>${mp_i18n.gettext('Waiting for…')}</h4>`
		btn_src.classList.remove('display_none')
		btn_src.classList.add('display_inline')
		tooltip_text.classList.add('display_none')
		for (const src_key of rest_search_source) {
			const src_def = source_objs[src_key]
			htm += '<p class="tooltip_text_family">'
				+`<img class="f_icon" src=${src_def.favicon || '.'} width=24>`
				+`&nbsp;<b>${src_def.tags.name}</b> : ${src_key}<p>`
		}
		if (!btn_src_text.classList.contains('display_none'))
			btn_src_text.innerHTML = htm
		tooltip_text.innerHTML = htm
	}
	if(mp_req.running_query_countdown % 15 === 0) {
		display_ongoing_results()
	}
}
function filter_one_src(mf_name_elt) {
	if (mf_name_elt === null) return
	try {
		mp_req.findingList.search(mf_name_elt.textContent, ['f_source', 'f_source_title'])
		radio_bold_clicked(mf_name_elt, 'mf_name')
		mf_date_slicing()
		filter_date_range()
	} catch (exc) {
		console.error(exc, {mf_name_elt})
	}
}
function attach_metaFindingList_evts () {
	// console.log('attach_metaFindingList_evts', mp_req.metaFindingList.visibleItems)
	let rm_elm, report_elm
	let itm_elm
	for (const vis_itm of mp_req.metaFindingList.visibleItems) {
		itm_elm = vis_itm.elm
		itm_elm.getElementsByClassName('mf_title')[0].onclick =	(evt) => filter_one_src(
			evt.target.parentNode.getElementsByClassName('mf_name')[0])
		rm_elm = itm_elm.getElementsByClassName('mf_remove_source')[0]
		rm_elm.onclick = evt => {
			document.body.classList.add('waiting')
			setTimeout(function () {
				const mf_name_elt = evt.target.parentNode.getElementsByClassName('mf_name')[0]
				filter_one_src(mf_name_elt) // filter only the elements to delete
				const cur_page_size = mp_req.findingList.page
				mp_req.findingList.show(0, mp_req.findingList.size()) // display all the elts to del
				for (const itm of mp_req.findingList.visibleItems)
					mp_req.findingList.remove('f_url', itm.values().f_url)
				mp_req.findingList.show(0, cur_page_size)
				mp_req.metaFindingList.remove('mf_name', mf_name_elt.textContent)
				BOMµ.id('mp_query_meta_total').textContent = get_mf_res_nb().toLocaleString(user_lang)
				BOMµ.id('mf_total').click()
				document.body.classList.remove('waiting')
			}, 0)
		}
		report_elm = itm_elm.getElementsByClassName('mf_report')[0]
		report_elm.onclick = evt => {
			const pop = evt.target.nextElementSibling
			pop.style.display = 'block'
			const name = evt.target.parentNode.firstElementChild.textContent.trim()
			itm_elm.disabled = true
			pop.querySelector('.mf_name').textContent = name
			pop.querySelector('.cancel_report').onclick = () => {
				pop.style.display = 'none'
				itm_elm.disabled = false
			}
			pop.querySelector('.send_report').onclick = async () => {
				pop.style.display = 'none'
				const comment = pop.querySelector('.input_report').value || 'none'
				const pb = pop.querySelector('.select_prob').value
				const url = new URL(`https://www.meta-press.es/fbk/${manifest.version}/${pb}/${name}`)
				await mµ.request_one_host_perm('https://www.meta-press.es') // keep www. cause redir
				console.info('The 2 next XHR are 404 on purpose, I\'ll get the feedback from logs')
				mp.try_fetch(url)
				let setting = ''
				if (pop.querySelector('.permit_send').checked)
					for (const qse of gen_permalink(1).searchParams) setting += `&${qse[0]}=${qse[1]}`
				mp.try_fetch(`${url}?date=${new Date().toISOString()}&txt=${comment}${setting}`)
			}
		}
	}
}
function display_ongoing_results() {
	// console.log('display_ongoing_results', mp_req.running_query_countdown, {mp_req})
	// console.trace()
	if (mp_req.final_result_displayed) return
	BOMµ.id('mp_query_countdown').textContent = mp_req.running_query_countdown
	BOMµ.id('mp_running_src_fetched').textContent = current_source_nb
	if(is(fetch_abort_controller) && !fetch_abort_controller.signal.aborted)
		if (!TEST_SRC) {
			BOMµ.id('mp_running_stats').classList.remove('display_none'),
			BOMµ.$('title').textContent = BOMµ.id('ongoing_tab_title').textContent
		}
	if (mp_req.findingList.size() > 0 && !TEST_SRC) {
		if(!IS_IMPORT_FILE) {
			mp_req.findingList.sort('f_epoc', {order: 'desc'})
			mp_req.metaFindingList.sort('mf_res_nb', {order: 'desc'})
		}
		BOMµ.id('meta-findings').classList.remove('display_none')
		apply_result_searchs()
	}
	if ((! mp_req.final_result_displayed) && (mp_req.running_query_countdown === 0))
		display_final_results()
}
let mf_search_cb_timeout
function mf_search_cb (evt) {
	clearTimeout(mf_search_cb_timeout)
	mf_search_cb_timeout = setTimeout(() => {
		mp_req.metaFindingList.search(triw(evt.target.value), ['mf_name'])
	}, evt.keyCode === 13 ? 0 : 500)
}
function get_mf_res_nb () {
	let cnt = 0
	for (const mf_itm of mp_req.metaFindingList.items) cnt += mf_itm.values().mf_res_nb
	return cnt
}
function result_attach_event () {
	for (const img of BOMµ.$$(`[src="${IMG_PLACEHOLDER}"]`)) {
		img.removeEventListener('click', load_result_image)
		img.addEventListener('click', load_result_image, {once: true})
	}
	for (const txt of document.getElementsByClassName('f_txt'))
		if (BOMµ.isOverflown(txt)) txt.style.resize = 'vertical'
}
function apply_result_searchs() {
	search_in_res(BOMµ.id('mp_filter').value) // resets filters
}
async function display_final_results() {
	// console.log('display_final_results', {TEST_SRC}, {mp_req})
	mp_req.final_result_displayed = 1
	IS_IMPORT_FILE = false
	if (TEST_SRC) {
		const pool_node = opener.document.getElementById('mp_display_result_test')
		const pool = parseInt(pool_node.dataset.pool) + 1
		pool_node.dataset.pool = pool
		self.close()
	} else {  // so: if (!TEST_SRC)
		clearTimeout(mp_req.repeat_timeout_ongoing)
		clearTimeout(mp_req.query_result_timeout)
		clearTimeout(mp_req.duration_ticker)
		BOMµ.id('mp_running_stats').classList.add('display_none')
		BOMµ.id('mp_cancelling').classList.add('display_none')
		BOMµ.id('search_stats').classList.remove('display_none')
		mp_req.findingList.sort('f_epoc', {order: 'desc'})
		mp_req.metaFindingList.sort('mf_res_nb', {order: 'desc'})
		apply_result_searchs()
		location.href = '#'  // ensure href is not already #before_search_stats, else it wont move
		location.href = '#before_search_stats'
		if (rnd(1) > 7)
			BOMµ.id('donation_reminder').classList.remove('display_none')
		if (0 === mp_req.findingList.size())
			BOMµ.id('no_results').classList.remove('display_none')
		else
			BOMµ.id('mp_page_sizes').classList.remove('display_none')
		if(!mp_req.findingList.handlers.updated.includes(result_attach_event)) {
			mp_req.findingList.on('updated', result_attach_event)
			mp_req.findingList.update()
		}
		BOMµ.id('mp_query_meta_total').textContent = get_mf_res_nb().toLocaleString(user_lang)
		BOMµ.id('mp_query_meta_local_total').textContent = mp_req.findingList.size()
		const mp_end_date = ndt()
		let req_duration = ((mp_end_date - mp_req.query_start_date) / 1000).toFixed(2)
		if (req_duration >= 10)
			req_duration = Math.round(req_duration)
		BOMµ.id('mp_duration').textContent = req_duration
		BOMµ.id('mp_duration').title = mp_end_date.toLocaleString(user_lang)
		BOMµ.id('mp_query_search_terms').textContent = mp_req.search_terms
		BOMµ.id('mp_src_nb').textContent =
			mp_req.metaFindingList.items.filter(mf_itm => mf_itm.values().mf_res_nb > 0).length
		BOMµ.id('mp_src_fetched').textContent = current_source_nb
		BOMµ.id('mp_submit').disabled = false
		BOMµ.id('mp_submit').style.cursor = 'pointer'
		BOMµ.id('mp_stop').classList.remove('display_inline')
		BOMµ.id('mp_stop').classList.add('display_none')
		document.getElementsByTagName('title')[0].textContent = BOMµ.id('tab_title').textContent
		document.body.classList.remove('waiting')
		const id_sch_s = QUERYSTRING.get('id_sch_s')
		if (req_duration > 8 && !id_sch_s)
			mµ.notify_user(BOMµ.id('tab_title').textContent,
				BOMµ.id('search_stats').textContent.trim().replace(' 🔗', ''))
		if (id_sch_s)
			add_elt_url(id_sch_s)
		if (await mµ.get_keep_host_perm() === '0')
			mµ.drop_host_perm()
		if (mp_req.metaFindingList.size() > 30) {  // search source in meta finding
			BOMµ.id('mf_meta_search').classList.remove('display_none')
			BOMµ.id('mf_meta_search').classList.add('display_flex')
			BOMµ.id('mp_meta_search').onkeyup = mf_search_cb
			BOMµ.id('mp_clear_search').onclick = () => {
				BOMµ.id('mp_meta_search').value = ''
				mp_req.metaFindingList.search()
			}
		}
		BOMµ.id('mp_search_permalink').href = µrl.rm_href_anchor(gen_permalink(1))
		if (QUERYSTRING.get('backend')) {
			await export_JSON(QUERYSTRING.get('token'))
		}
	}
}
function clear_results() {
	mp_req.final_result_displayed = 0
	IS_IMPORT_FILE = false
	BOMµ.id('no_results').classList.add('display_none')
	BOMµ.id('mp_page_sizes').classList.add('display_none')
	BOMµ.id('mp_query_meta_total').textContent = '…'
	BOMµ.id('mp_query_meta_local_total').textContent = '…'
	BOMµ.id('mp_running_stats').classList.add('display_none')
	BOMµ.id('mp_query_countdown').textContent = '…'
	BOMµ.id('mp_running_src_fetched').textContent = '…'
	BOMµ.id('mp_query_search_terms').textContent = '…'
	BOMµ.id('mp_src_nb').textContent = '…'
	BOMµ.id('mp_src_fetched').textContent = '…'
	BOMµ.id('mp_duration').textContent = '…'
	BOMµ.id('mf_meta_search').classList.remove('display_flex')
	BOMµ.id('mf_meta_search').classList.add('display_none')
	BOMµ.id('clear_date').click()
	BOMµ.id('mp_clear_filter').click()
	if ('block' === BOMµ.id('mp_sel_all').style.display)
		BOMµ.id('mp_sel_mod').click()
	BOMµ.id('meta-findings').classList.add('display_none')
	mp_req.findingList.clear()
	mp_req.metaFindingList.clear()
}
BOMµ.id('mp_autosearch_permalink').addEventListener('click', async () => {
	let sch_sea = gen_permalink(false)
	sch_sea = sch_sea.toString() // Chrome doesn't support URL in browser.storage elt
	await mµ.to_storage('new_sch_sea', sch_sea)
	await browser.tabs.create ({url: '/html/settings.html#sch_sea'})
})
BOMµ.id('mp_stop').addEventListener('click', () => {
	display_final_results()
	fetch_abort_controller.abort()
})
/**
 * Generate an URL who caontain all the sear settings.
 * @param {boolean} is_submit if true add autosubmit
 */
function gen_permalink(is_submit=false) {
	const permalink = new URL(window.location)
	permalink.searchParams.set('q', BOMµ.id('mp_query').value)
	for (const tag of tag_categ) {
		permalink.searchParams.delete(tag)
		for (const val of tags[tag].getValue(true))
			permalink.searchParams.append(tag, val)
	}
	if (del_src_sel.length > 0) {
		permalink.searchParams.delete('del_src_sel')
		for (const src_key of del_src_sel)
			permalink.searchParams.append('del_src_sel', src_key)
	}
	if (add_src_sel.length > 0) {
		permalink.searchParams.delete('add_src_sel')
		for (const src_key of add_src_sel)
			permalink.searchParams.append('add_src_sel', src_key)
	}
	if (is_submit)
		permalink.searchParams.set('submit', 1)
	else
		permalink.searchParams.delete('submit')
	return permalink
}
for (const pg_size of [5, 10, 20, 50])
	BOMµ.id(`mp_headlines_page_${pg_size}`).addEventListener('click', () => {
		headlineList.show(0, pg_size)
	})
BOMµ.id('mp_headlines_page_all').addEventListener('click', () => {
	const pg_size = headlineList.size()
	headlineList.show(0, pg_size)
})
for (const pg_size of [10, 20, 50, 100])
	BOMµ.id(`list_src_page_${pg_size}`).addEventListener('click', () =>
		display_panel.show(0, pg_size))
BOMµ.id('list_src_page_all').addEventListener('click', () => {
	display_panel.show(0, display_panel.size())})
for (const pg_size of [10, 20, 50])
	BOMµ.id(`mp_page_${pg_size}`).addEventListener('click', () => {
		mp_req.findingList.show(0, pg_size)
		apply_result_searchs()
	})
BOMµ.id('mp_page_all').addEventListener('click', () => {
	mp_req.findingList.show(0, mp_req.findingList.size())
	apply_result_searchs()
})
BOMµ.id('mp_import_RSS').addEventListener( 'click', () => import_XML('RSS'))
// BOMµ.id('mp_import_ATOM').addEventListener('click', () => import_XML(ATOM'))
BOMµ.id('mp_import_JSON').addEventListener('click', import_JSON)
BOMµ.id('mp_import_CSV').addEventListener( 'click', import_CSV)
function import_CSV() {
	BOMµ.read_user_file(function (evt) {
		mp_req.query_start_date = ndt()
		document.body.classList.add('waiting')
		setTimeout(function () {
			try {
				clear_results()
				IS_IMPORT_FILE = true  // must be after clear_results()
				mp_req.running_query_countdown = 1
				let input_csv = evt.target.result
				input_csv = csv.parse(input_csv)
				input_csv.splice(0, 1)
				const mf = {}
				let res_countdown = input_csv.length
				let csv_res = []
				const slice_import_CSV = function () {
					do {
						try {
							res_countdown -= 1
							csv_res = input_csv[res_countdown]
							const r_dt = new Date(csv_res[0])
							const f_source = csv_res[1]
							const f_source_key = csv_res[2]
							const img = {
								f_img_src: csv_res[8],
								f_img_alt: csv_res[9],
								f_img_title: csv_res[10],
								mp_data_img: csv_res[8],
								mp_data_alt: csv_res[9],
								mp_data_title: csv_res[10],
							}
							if (!IS_LOAD_IMG)
								img_not_load(img)
							mp_req.findingList.add({
								f_h1: csv_res[5],
								f_h1_title: csv_res[5],
								f_url: csv_res[4],
								f_icon: csv_res[3],
								f_img_src: img.f_img_src,
								f_img_alt: img.f_img_alt,
								f_img_title: img.f_img_title,
								mp_data_img: img.mp_data_img,
								mp_data_alt: img.mp_data_alt,
								mp_data_title: img.mp_data_title,
								mp_data_key: csv_res[2],
								f_txt: csv_res[6],
								f_by:  csv_res[7] || csv_res[1],
								f_by_title: csv_res[7] || csv_res[1],
								f_dt: r_dt.toISOString().slice(0, -14),
								f_dt_title: r_dt.toLocaleString(user_lang),
								f_epoc: r_dt.valueOf(),
								f_ISO_dt: r_dt.toISOString(),
								f_source: f_source,
								f_source_title: f_source
							})
							const src_def = source_objs[f_source_key]
							const url = !is(src_def) ? f_source_key : mp.format_search_url(
								src_def.search_url_web || src_def.search_url,
								mp_req.search_terms, MAX_RES_BY_SRC)
							if (!is(mf[f_source])) {  // initialization
								mf[f_source] = {
									mf_icon: csv_res[3],
									mf_name: f_source,
									mf_title: mf_title(f_source),
									mf_ext_link: url,
									// mf_remove_source: f_source,
									mf_res_nb: 0
								}
							}
							mf[f_source].mf_res_nb += 1
							mf[f_source].mf_locale_res_nb = mf[f_source].mf_res_nb.toLocaleString(user_lang)
						} catch (exec) {
							console.error('CSV item loading failure', exec, csv_res)
						}
					} while (res_countdown % IMPORT_SLICE_SIZE != 0)
					if (res_countdown > 0) {
						setTimeout(slice_import_CSV, 0)
					} else {
						for (const val of Object.values(mf)) {
							mp_req.metaFindingList.add(val)
						}
						mp_req.running_query_countdown = 0
						document.body.classList.remove('waiting')
					}
					display_ongoing_results()
				} // end slice_import_CSV
				setTimeout(slice_import_CSV, 0)
			} catch (exec) {
				console.error('CSV loading failure', exec)
			}
		}, 0)
	})
}
function import_JSON() {
	BOMµ.read_user_file(
		(evt) => sub_import_JSON (
			JSON.parse(evt.target.result)
		)
	)
}
function sub_import_JSON (input_json) {
	document.body.classList.add('waiting')
	mp_req.query_start_date = ndt()
	setTimeout(function () {
		try {
			clear_results()
			IS_IMPORT_FILE = true
			mp_req.running_query_countdown = 1  // avoid display_ongoing_results to call _final_
			import_JSON_request(input_json)
			import_JSON_results(input_json)
		} catch (exc) {
			console.error('JSON loading failure', exc)
		} finally {
			document.body.classList.remove('waiting')
		}
	}, 0)
}
function import_JSON_request(input_json) {
	if (input_json.filters)
		tags_from_JSON(input_json.filters)
	mp_req.search_terms = input_json.search_terms
	BOMµ.id('mp_query').value = input_json.search_terms
}
async function slice_import_JSON (input_json, res_countdown) {
	let dup_elt, dup_val
	let fdg = {}
	do {
		try {
			res_countdown -= 1
			fdg = input_json.findings[res_countdown]
			if (IS_LOAD_IMG) {
				fdg.f_img_src = fdg.mp_data_img
				fdg.f_img_alt = fdg.mp_data_alt
				fdg.f_img_title = fdg.mp_data_title
			} else if (fdg.mp_data_img)
				img_not_load(fdg)
		} catch (exc) {
			console.error('import_JSON img fail', exc, fdg, res_countdown, input_json)
		}
		try {
			if (IS_UNDUP && (dup_elt = get_dup(fdg.f_h1)).length) {
				dup_elt = dup_elt[0]
				dup_val = dup_elt.values()
				dup_val.f_source += dup_val.f_source.slice(-1) === '…' ? '' : '…'
				dup_val.f_source_title += `, ${fdg.f_source_title}`
				dup_elt.values(dup_val)
			} else {
				mp_req.findingList.add(fdg)
			}
		} catch (exc) {
			console.error('import_JSON res fail', exc, fdg, res_countdown, input_json)
		}
	} while (res_countdown % IMPORT_SLICE_SIZE != 0)
	if (res_countdown > 0) {
		if (IS_IMPORT_FILE) { // when not importing display_ongoing_results is called by
			display_ongoing_results()  // -> update_search_query_countdown
			await delay(0)
		}
		await slice_import_JSON(input_json, res_countdown)
	} else {
		if (IS_IMPORT_FILE) {
			mp_req.metaFindingList.add(input_json.meta_findings)
			mp_req.running_query_countdown = 0  // avoid display_ongoing_results to call _final_
			display_ongoing_results()  // -> update_search_query_countdown
			// display_final_results()
			document.body.classList.remove('waiting')
		}
	}
}
async function import_JSON_results(input_json) {
	const res_countdown = input_json.findings.length
	if (res_countdown > 0)
		await slice_import_JSON (input_json, res_countdown)
}
function get_dup(f_h1) {
	return mp_req.findingList.get('f_h1', f_h1)
}
function import_XML(flux_type) {
	if (BOMµ.id('tags_row').style.display === 'block')
		BOMµ.id('tags_handle').click()
	BOMµ.read_user_file(function (evt) {
		document.body.classList.add('waiting')
		mp_req.query_start_date = ndt()
		setTimeout(function () {
			try {
				clear_results()
				IS_IMPORT_FILE = true
				mp_req.running_query_countdown = 1
				const input_xml = mp.DOM_parse_text_rep(evt.target.result, 'application/xml',
					DOM_parser)
				let r_src, src_def, r_dt, r_by, f_source, f_icon, item_obj, f_h1, f_url, l_fmp
				const mf = {}
				let xml_src_def = sµ.XML_SRC[flux_type]  // parser info
				const xml_results = input_xml.querySelectorAll(xml_src_def.results)
				const found_RSS_type = mp.find_RSS_variant(xml_results, {tags:{name:'import_XML'}})
				// console.log('found_RSS_type', found_flux_type)
				xml_src_def = sµ.XML_SRC[found_RSS_type]
				tags_from_XML(input_xml.querySelectorAll(xml_src_def.filters), flux_type)
				mp_req.search_terms = input_xml.getElementsByTagName('title')[0].textContent.replace(
					'Meta-Press.es : ', '')
				BOMµ.id('mp_query').value = mp_req.search_terms
				let is_mp_RSS = false
				let generator = input_xml.getElementsByTagName('generator')[0]
				if (is(generator)) {
					generator = generator.textContent
					if (is_str(generator))
						is_mp_RSS = generator.startsWith('Meta-Press.es')
				}
				const results = input_xml.querySelectorAll(xml_src_def.results)
				let res_countdown = results.length
				let item = {}
				const slice_import_XML = function () {
					do {
						try {
							res_countdown -= 1
							item = results[res_countdown]
							l_fmp = {
								'f_img_src': '', 'f_img_alt': '', 'f_img_title': '', 'mp_data_img': '',
								'mp_data_alt': '', 'mp_data_title': '', 'f_txt': '', 'mp_data_key': ''
							}
							xml_src_def.tags = {key: ''}
							if (is_mp_RSS) {	// it's our format
								if ('RSS' === flux_type)
									l_fmp.mp_data_key = evaluateXPath(item, './dc:identifier').textContent
								else
									l_fmp.mp_data_key = item.querySelector('contributor > uri').textContent
								src_def = source_objs[l_fmp.mp_data_key]
								if (src_def)
									xml_src_def.tags.key = src_def.tags.key
							}
							f_url = scrap('r_url', item, xml_src_def, DOM_parser)
							f_h1 = scrap('r_h1', item, xml_src_def, DOM_parser)
							r_src = µrl.domain_part(f_url)
							if (!xml_src_def.tags.key)
								src_def = source_objs[r_src]
							l_fmp.f_txt = scrap('r_txt', item, xml_src_def, DOM_parser)
							if (is(xml_src_def.r_img_src) || is(xml_src_def.r_img_src_xpath)) {
								l_fmp.f_img_src = scrap('r_img_src', item, xml_src_def, DOM_parser)
								l_fmp.mp_data_img = l_fmp.f_img_src
							}
							if (is(xml_src_def.r_img_alt) || is(xml_src_def.r_img_alt_xpath)) {
								l_fmp.f_img_alt = scrap('r_img_alt', item, xml_src_def, DOM_parser)
								l_fmp.mp_data_alt = l_fmp.f_img_alt
							}
							if (is(xml_src_def.r_img_title) || is(xml_src_def.r_img_title_xpath)) {
								l_fmp.f_img_title = scrap('r_img_title', item, xml_src_def, DOM_parser)
								l_fmp.mp_data_title = l_fmp.f_img_title
							}
							r_dt = new Date(scrap('r_dt', item, xml_src_def, DOM_parser))
							r_by = scrap('r_by', item, xml_src_def, DOM_parser)
							f_icon = src_def && src_def.favicon_url || '/img/Feed-icon.svg'
							f_source = src_def && src_def.tags.name || µrl.domain_name(f_url) || ''
							if (!IS_LOAD_IMG)
								img_not_load(l_fmp)
							item_obj = {
								f_h1: f_h1,
								f_h1_title: f_h1,
								f_url: f_url,
								f_icon: f_icon,
								f_img_src: l_fmp.f_img_src,
								f_img_alt: l_fmp.f_img_alt,
								f_img_title: l_fmp.f_img_title,
								mp_data_img: l_fmp.mp_data_img,
								mp_data_alt: l_fmp.mp_data_alt,
								mp_data_title: l_fmp.mp_data_title,
								mp_data_key: l_fmp.mp_data_key,
								f_txt: l_fmp.f_txt,
								f_by: r_by || f_source,
								f_by_title: r_by,
								f_dt: r_dt.toISOString().slice(0, -14),
								f_dt_title: r_dt.toLocaleString(user_lang),
								f_epoc: r_dt.valueOf(),
								f_ISO_dt: r_dt.toISOString(),
								f_source: f_source,
								f_source_title: f_source
							}
							mp_req.findingList.add(item_obj)
							if (!is(mf[f_source])) {  // initialization
								mf[f_source] = {
									mf_icon: f_icon,
									mf_name: f_source,
									mf_title: mf_title(f_source),
									mf_ext_link: src_def && mp.format_search_url(
										src_def.search_url_web || src_def.search_url,
										mp_req.search_terms, MAX_RES_BY_SRC) || '',
									// mf_remove_source: f_source,
									mf_res_nb: 0
								}
							}
							mf[f_source].mf_res_nb += 1
							mf[f_source].mf_locale_res_nb = mf[f_source].mf_res_nb.toLocaleString(user_lang)
						} catch (exc) {
							console.error(flux_type, 'item loading failure', exc, item)
						}
					} while (res_countdown % IMPORT_SLICE_SIZE != 0)
					if (res_countdown > 0) {
						setTimeout(slice_import_XML, 0)
					} else {
						for (const val of Object.values(mf)) {
							mp_req.metaFindingList.add(val)
						}
						mp_req.running_query_countdown = 0
						document.body.classList.remove('waiting')
					}
					display_ongoing_results()
				}
				setTimeout(slice_import_XML, 0)
			} catch (exc) {
				console.error(flux_type, 'loading failure', exc)
			}
		}, 0)
	})
}
function img_not_load(l_fmp) {
	l_fmp.f_img_src = IMG_PLACEHOLDER
	l_fmp.f_img_alt = ALT_LOAD_TXT
}
function mf_title(src_name) {
	return `${mp_i18n.gettext('Filter results of')} '${src_name}' ${mp_i18n.gettext('only')}`
}
BOMµ.id('mp_sel_mod').addEventListener('click', () => {
	const cur_sel_all = BOMµ.id('mp_sel_all')
	const cur_unsel_all = BOMµ.id('mp_unsel_all')
	const cur_sel_inverse = BOMµ.id('mp_sel_inverse')
	cur_sel_all.style.display = cur_sel_all.style.display === 'inline' ? 'none' : 'inline'
	cur_unsel_all.style.display = cur_unsel_all.style.display === 'inline' ? 'none' : 'inline'
	cur_sel_inverse.style.display = cur_sel_inverse.style.display === 'inline' ?
		'none' : 'inline'
	const cur_sel_mod =dynamic_css.rules[CSS_CHECKBOX_RULE_INDEX].cssText.match(/(block|none)/)[0]
	const next_sel_mod = cur_sel_mod === 'none' ? 'block' : 'none'
	document.getElementsByClassName('mp_export_choices')[0].style.display = next_sel_mod
	dynamic_css.deleteRule(CSS_CHECKBOX_RULE_INDEX)
	dynamic_css.insertRule(
		`.f_selection {display:${next_sel_mod}!important}`, CSS_CHECKBOX_RULE_INDEX)
})
BOMµ.id('mp_sel_all').addEventListener('click', () => {
	for (const item of mp_req.findingList.visibleItems) {
		const check = item.elm.getElementsByClassName('f_selection')[0]
		check.checked = true
	}
})
BOMµ.id('mp_unsel_all').addEventListener('click', () => {
	for (const item of mp_req.findingList.visibleItems) {
		const check = item.elm.getElementsByClassName('f_selection')[0]
		check.checked = false
	}
})
BOMµ.id('mp_sel_inverse').addEventListener('click', () => {
	for (const item of mp_req.findingList.visibleItems) {
		const check = item.elm.getElementsByClassName('f_selection')[0]
		check.checked = check.checked === false
	}
})
/**
 * Export the curent source selection json file.
 */
function export_JSON(token) {
	const json_str = res_in_JSON(
		tags_values(),
		mp_req.findingList.matchingItems.map(itm => itm.values()),
		mp_req.metaFindingList.toJSON(),
		BOMµ.id('mp_query').value
	)
	if (is_str(token))
		BOMµ.id('backend_export_content').textContent = json_str
	else
		BOMµ.upload_file_to_user(export_filename(manifest.version, 'json'), json_str)
}
/**
 * Export the curent source selection csv file.
 */
function export_CSV() {
	document.body.classList.add('waiting')
	const res = res_in_CSV(mp_req.findingList.matchingItems.map(itm => itm.values()))
	document.body.classList.remove('waiting')
	BOMµ.upload_file_to_user(export_filename(manifest.version, 'csv'), res)
}
function export_RSS() {  // https://validator.w3.org/feed/#validate_by_input
	document.body.classList.add('waiting')
	console.log('tags', {tags})
	const flux = res_in_RSS(
		tags_values(),
		mp_req.findingList.matchingItems.map(itm => itm.values()),
		BOMµ.id('mp_query').value,  // search terms
		tags.lang.getValue(true) || user_lang,
		manifest.version
	)
	document.body.classList.remove('waiting')
	BOMµ.upload_file_to_user(export_filename(manifest.version, 'rss'), flux)
}
function export_ATOM() {  // https://validator.w3.org/feed/#validate_by_input
	document.body.classList.add('waiting')  // https://tools.ietf.org/html/rfc4287
	let flux = res_in_ATOM(
		tags_values(),
		mp_req.findingList.matchingItems.map(itm => itm.values()),
		BOMµ.id('mp_query').value,  // search terms
		manifest.version
	)
	document.body.classList.remove('waiting')
	BOMµ.upload_file_to_user(export_filename(manifest.version, 'atom'), flux)
}
// select export
BOMµ.id('mp_export_sel_CSV').addEventListener('click', () => {
	mp_req.findingList.filter(i =>
		i.elm && i.elm.getElementsByClassName('f_selection')[0].checked)
	export_CSV()
	mp_req.findingList.filter()
})
BOMµ.id('mp_export_sel_JSON').addEventListener('click', () => {
	mp_req.findingList.filter(i =>
		i.elm && i.elm.getElementsByClassName('f_selection')[0].checked)
	export_JSON()
	mp_req.findingList.filter()
})
BOMµ.id('mp_export_sel_ATOM').addEventListener('click', () => {
	mp_req.findingList.filter(i =>
		i.elm && i.elm.getElementsByClassName('f_selection')[0].checked)
	export_ATOM()
	mp_req.findingList.filter()
})
BOMµ.id('mp_export_sel_RSS').addEventListener('click', () => {
	mp_req.findingList.filter(i =>
		i.elm && i.elm.getElementsByClassName('f_selection')[0].checked)
	export_RSS()
	mp_req.findingList.filter()
})
// export all
BOMµ.id('mp_export_search_RSS').addEventListener( 'click', export_RSS)
BOMµ.id('mp_export_search_ATOM').addEventListener('click', export_ATOM)
BOMµ.id('mp_export_search_JSON').addEventListener('click', export_JSON)
BOMµ.id('mp_export_search_CSV').addEventListener( 'click', export_CSV)
const now = ndt()
const date_filters = [
	now.setDate(    now.getDate()-1),
	now.setDate(    now.getDate()-2),
	now.setDate(    now.getDate()-3),
	now.setDate(    now.getDate()-7),
	now.setDate(    now.getDate()-14),
	now.setMonth(   now.getMonth()-1),
	now.setMonth(   now.getMonth()-2),
	now.setMonth(   now.getMonth()-6),
	now.setFullYear(now.getFullYear()-1),
	now.setFullYear(now.getFullYear()-2),
	now.setFullYear(now.getFullYear()-5),
	now.setFullYear(now.getFullYear()-10)
]
const date_flt_fn = [
	function mf_last_day(r)  { return r.values().f_epoc > date_filters[0] },
	function mf_last_2d(r)	 { return r.values().f_epoc > date_filters[1] },
	function mf_last_3d(r)	 { return r.values().f_epoc > date_filters[2] },
	function mf_last_week(r) { return r.values().f_epoc > date_filters[3] },
	function mf_last_2w(r)	 { return r.values().f_epoc > date_filters[4] },
	function mf_last_month(r){ return r.values().f_epoc > date_filters[5] },
	function mf_last_2m(r)   { return r.values().f_epoc > date_filters[6] },
	function mf_last_6m(r)   { return r.values().f_epoc > date_filters[7] },
	function mf_last_1y(r)   { return r.values().f_epoc > date_filters[8] },
	function mf_last_2y(r)   { return r.values().f_epoc > date_filters[9] },
	function mf_last_5y(r)   { return r.values().f_epoc > date_filters[10] },
	function mf_last_dk(r)   { return r.values().f_epoc > date_filters[11] }
]
for (const flt of date_flt_fn)
	BOMµ.id(flt.name).addEventListener('click', evt => filter_last(evt.target, flt))
BOMµ.id('mf_all_res').addEventListener('click', evt => filter_last(evt.target, null))

function mf_date_slicing() {
	let prev_nb = 0
	const total_nb = set_filter_nb(null, 'mf_all_res', 0, 0)
	for (const flt of date_flt_fn)
		prev_nb = set_filter_nb(flt, flt.name, prev_nb, total_nb)
}
function set_filter_nb(fct, id, prev_nb, total_nb) {
	if(fct) mp_req.findingList.filter(fct)
	else mp_req.findingList.filter()
	const nb = mp_req.findingList.matchingItems.length
	if (nb > 0 && nb > prev_nb + (total_nb * 0.06)) {
		const val = Math.floor(nb * 40 / mp_req.findingList.size())
		BOMµ.id(id+'_nb').textContent = `${nb.toLocaleString(user_lang)} ${'.'.repeat(val)}`
		BOMµ.id(id).parentNode.classList.remove('display_none')
	} else {
		BOMµ.id(id).parentNode.classList.add('display_none')
	}
	return nb
}
/**
 * Filter the result in the starting date.
 * @param {*} target
 * @param {Function} fct the filter function
 */
function filter_last(target, fct) {
	// radio_bold_clicked(target, 'mf_dt_lbl')
	radio_bold_clicked(target, 'mf_dt_lbl')
	if(fct) {
		mp_req.findingList.filter(fct)
	} else {
		mp_req.findingList.filter()
	}
}
/**
 * Add the given class to the target and remove the other user of this class.
 * @param {Node} target target
 * @param {string} a_class class to add
 */
function radio_bold_clicked(target, a_class) {
	try {
		for (const elt of document.getElementsByClassName(a_class))
			elt.classList.remove('bold')
		target.classList.add('bold')
	} catch (exc) {
		console.error(exc, target, a_class)
	}
}
BOMµ.id('mp_filter').addEventListener('keyup', search_in_res_cb)
let search_in_res_cb_timeout
function search_in_res_cb (evt) {
	clearTimeout(search_in_res_cb_timeout)
	search_in_res_cb_timeout = setTimeout(() => {
		search_in_res(evt.target.value)
	}, evt.keyCode === 13 ? 0 : 500)
}
BOMµ.id('mp_clear_filter').addEventListener('click', () => {
	BOMµ.id('mp_filter').value = ''
	search_in_res ()
})
function search_in_res (val) {
	if (mp_req.findingList.searched) {
		BOMµ.id('mf_total').click()
		return  // seach_in_res is called by mf_total cb
	}
	/*
	if (mp_req.findingList.filtered)
		mp_req.findingList.filter()*/
	if (val)
		mp_req.findingList.search(val, ['f_h1', 'f_source', 'f_dt', 'f_by',	'f_txt'])
	// else
	//	mp_req.findingList.search()
	mf_date_slicing()
	filter_date_range()
}
/* * * Headlines * * */
const headlineList = new List('headlines', {
	indexAsync: true,
	item: 'headline-item',
	valueNames: [
		'h_title',
		'h_body',
		{ name: 'h_url',	attr: 'href' },
		{ name: 'h_icon',	attr: 'src' },
		{ name: 'h_tip',	attr: 'title' },
		{ name: 'h_dt',		attr: 'mp-data-dt' },
		{ name: 'h_img',	attr: 'src' },
		{ name: 'h_img_alt',attr: 'alt' },
		{ name: 'h_img_title',attr: 'title' },
	],
	page: HEADLINE_PAGE_SIZE,
	pagination: {
		outerWindow: 10,
		innerWindow: 10,
		item: '<li><a class="page" href="#headlines"></a></li>'
	}
})
/*
let headline_item = 1
let headline_timeout, headline_fadeout_timeout
function rotate_headlines_timeout() {
	headline_timeout = setTimeout(() => {
		if (headlineList.size() > HEADLINE_PAGE_SIZE) {
			headline_item = (headline_item + HEADLINE_PAGE_SIZE) % headlineList.size()
			headlineList.show(headline_item, HEADLINE_PAGE_SIZE)
			rotate_headlines()
		}
	}, 10000)
}
function rotate_headlines() {
	clearTimeout(headline_fadeout_timeout)
	rotate_headlines_timeout()
	fadeout_pagination_dot('headlines', 'rgba(0, 208, 192, 1)', 1)
}
function fadeout_pagination_dot(id, bg_color, dot_opacity) {
	headline_fadeout_timeout = setTimeout(() => {
		let dot = BOMµ.$(`#${id} ul.pagination > li.active a.page`)
		dot_opacity -= 0.1
		if (dot)
			dot.style.backgroundColor = bg_color.replace(/, 1\)/, `, ${dot_opacity})`)
		fadeout_pagination_dot(id, bg_color, dot_opacity)
	}, 1000)
}
BOMµ.id('headlines').addEventListener('mouseover', () => {
	clearTimeout(headline_timeout)
	clearTimeout(headline_fadeout_timeout)
})
BOMµ.id('headlines').addEventListener('mouseout', async () => {
	rotate_headlines()
})
*/
BOMµ.id('load_headlines').addEventListener('click', () => load_headlines())
BOMµ.id('reload_headlines').addEventListener('click', () => load_headlines())
BOMµ.id('remove_headlines').addEventListener('click', () => {
	headlineList.clear()
	BOMµ.id('headlines_wrapper').classList.add('display_none')
})
function load_internal_news() {
	const mp_hls = BOMµ.$$('.mp_hl')
	for (const hdl of mp_hls)
		headlineList.add({
			h_title: mp_i18n.gettext(hdl.textContent),
			h_url: hdl.href,
			h_icon: '/img/favicon-metapress-v2.png',
			h_tip: mp_i18n.gettext(DOMµ.strip_HTML_tags(hdl.title, DOM_parser)),
			h_img: '.',
			h_img_alt: ' ',
		})
	headlineList.show(0, mp_hls.length)
	for (let idx = mp_hls.length-1; idx--;)  // the last link should keep its target="_blank"
		// numbering starts at 1 for nth-of-type
		BOMµ.$(`#headlines > ul >li:nth-of-type(${idx+1}) a`).target = ''
	BOMµ.$('#headlines > ul >li:nth-of-type(1) a').addEventListener('click',
		() => load_headlines())
}
async function load_headlines() {
	const headline_host = mµ.get_current_news_hosts(cur_src_sel, source_objs)
	try {
		if (! await mµ.request_permissions(headline_host)) {
			return alert_perm()
		}
	} catch (exc) {
		// alert_perm()
		console.warn('Ignored exception', exc)
	}
	document.body.classList.add('waiting')
	BOMµ.id('headlines_wrapper').classList.remove('display_none')
	headlineList.clear()
	load_internal_news()
	BOMµ.id('mp_headlines_page_sizes').style.display = 'block'
	BOMµ.id('load_headlines').style.display = 'none'
	for (const ss of cur_src_sel) {
		const headlines = await mp.fetch_headlines (
			source_objs,
			[ss],
			await mµ.get_max_news_loading(),
			await mµ.get_max_news_by_src(),
			user_lang,
			DOM_parser
		)
		// console.log(headlines)
		headlineList.add(headlines)
		headlineList.sort('h_dt', {order: 'desc'})
		headlineList.show(0, HEADLINE_PAGE_SIZE)
	}
	document.body.classList.remove('waiting')
}
/* * * Tags * * */
const select_multiple_opt = {
	removeItemButton: true,
	resetScrollPosition: false,
	placeholder: true,
	placeholderValue: '*',
	duplicateItemsAllowed: false,
	searchResultLimit: 10,
	allowHTML: true
}
let add_src_sel = []
let del_src_sel = []
const nb_tags = {}
function populate_tags(source_selection) {
	for (const tag_name of tag_categ) {
		nb_tags[tag_name] = {}
	}
	for (const src_key of source_selection) {
		const src_def = source_objs[src_key]
		if (!is(src_def)) return
		for (const tag_name of tag_categ) {
			const tag_val = src_def.tags[tag_name]
			if (is_str(tag_val)) {
				nb_tags[tag_name][tag_val] = 1 + nb_tags[tag_name][tag_val] || 1
			} else if (is_obj(tag_val))
				for (const tag_val_itm of tag_val)
					nb_tags[tag_name][tag_val_itm] = 1 + nb_tags[tag_name][tag_val_itm] || 1
			else
				console.warn(`Unknown tag type ${tag_name} ${tag_val} for ${src_key}`)
		}
	}
	for (const tag_name of tag_categ) {
		const sel = []
		const nb_tag = nb_tags[tag_name]
		for (const tag of Object.keys(nb_tag).sort()) {
			if(tag_name === 'lang')
				sel.push({value: tag, label:
					`${LANG_NAME.of(tag)}<small> <i>[${tag}]</i> <i>(${nb_tag[tag]})</i></small>`})
			else if (tag_name === 'country')
				sel.push({value: tag, label:`${COUNTRY_NAME.of(tag)}<small> <i>[${tag}]</i> <i>(${
					nb_tag[tag]})</i></small>`})
			else
				sel.push({value: tag, label:
					`${mp_i18n.gettext(tag)}<small> <i>(${nb_tag[tag]})</i></small>`})
		}
		if(tags[tag_name] !== '')
			tags[tag_name].destroy()
		if (tag_name == 'tech')
			sel.push({value: 'cherry-pick_sources', label: 'cherry-pick'})
		tags[tag_name] = new Choices(BOMµ.id(`tags_${tag_name}`),
			Object.assign({choices: sel}, select_multiple_opt))
	}
}
function try_store_tags () {
	if (!QUERYSTRING.get('id_sch_s'))
		mµ.to_storage('filters', tags_values())
}
async function on_tag_sel(opt={}) {
	try_store_tags()
	const new_src_sel_keys = tag_sel_src_keys(source_objs, source_keys, tags_values())
	if (new_src_sel_keys.length === 0) {
		empty_sel(opt)
		return
	}
	if (new_src_sel_keys.length > del_src_sel.length || add_src_sel.length > 0) {
		cur_src_sel = new_src_sel_keys
		current_source_nb = cur_src_sel.length
		try_store_tags()
		if (await mµ.get_live_search_reload() && BOMµ.id('mp_query').value) {
			BOMµ.id('mp_submit').click()
		}
		if ((await mµ.get_news_loading()) &&
			headlineList.size() === 0 &&
			!QUERYSTRING.get('submit')
		) {
			load_headlines()
		} else if(await mµ.get_live_news_reload() && headlineList.size()) {
			const h_display = BOMµ.id('mp_headlines_page_sizes').style.display
			if(h_display !== 'none' && (opt && opt.target && opt.target.id !== 'tags_tech'))
				BOMµ.id('reload_headlines').click()
		}
	} else {
		empty_sel(opt)
	}
	update_nb_src()
	display_show_src()
}
function empty_sel(opt) {
	alert(mp_i18n.gettext(BOMµ.id('empty_sel_txt').textContent))
	if (opt && is(opt.parentNode)) {
		tags[opt.parentNode.id.replace('tags_', '')].setValue(opt.value)
	} else {
		set_default_tags()
	}
	on_tag_sel()
}
function tags_values() {
	const local_tags = {}
	for (const tag of tag_categ) {
		local_tags[tag] = tags[tag].getValue(true)
	}
	local_tags.del_src_sel = del_src_sel
	local_tags.add_src_sel = add_src_sel
	return local_tags
}
function tags_from_XML(tag_nodes, xml_type) {
	const tag_values = {}
	for (const tag of tag_categ) tag_values[tag] = []
	tag_values.add_src_sel = []
	tag_values.del_src_sel = []
	for (const node of tag_nodes)
		if ('ATOM' === xml_type) {
			const c_name = node.attributes.label.value
			try { tag_values[c_name.split('__')[1]].push(`${node.attributes.term.value}`) }
			catch (exc) { console.warn(`Bad ATOM filter name '${c_name}' (${exc}).`) }
		} else {
			const c_split = node.textContent.split('__')
			if (c_split.length > 2) {
				try { tag_values[c_split[1]].push(`${c_split[2]}`) }
				catch (exc) { console.warn(`Bad RSS filter name '${node.textContent}' (${exc}).`) }
			}
		}
	tags_from_JSON(tag_values)
	on_tag_sel()
}
function tags_from_JSON(tag_values) {
	let val, sel
	for (const tag_name of Object.keys(tag_values)) {
		sel = tags[tag_name]
		val = tag_values[tag_name]
		if (!sel) continue  // we want to avoid : s === ""
		sel.passedElement.element.removeEventListener('change', on_tag_sel)
		sel.removeActiveItems()
		sel.setChoiceByValue(val)
		sel.passedElement.element.addEventListener('change', on_tag_sel)
	}
	if (tag_values.add_src_sel)
		for (const tag of tag_values.add_src_sel)
			if (source_keys.includes(tag))
				add_src_sel = tag_values.add_src_sel
	if (tag_values.del_src_sel)
		for (const tag of tag_values.del_src_sel)
			if (source_keys.includes(tag))
				del_src_sel = tag_values.del_src_sel
	on_tag_sel()
}
function is_virgin_tags() {
	for (const cat of tag_categ)
		if (tags[cat].setChoiceByValue(cat).getValue(true).length > 0) return false
	if(QUERYSTRING.get('id_sch_s')) return false
	if(add_src_sel.length || del_src_sel.length) return false
	return true
}
function is_advanced_search(del_src_sel, add_src_sel) {
	const rz = JSON.stringify(get_default_tags(user_lang)) !== JSON.stringify(tags_values())
	return rz || del_src_sel.length || add_src_sel.lenth
}
BOMµ.id('reset_tag_sel').addEventListener('click', () => {
	set_default_tags(QUERYSTRING)
	on_tag_sel()
})
function set_default_tags() {
	if (QUERYSTRING.get('keep_empty_tags'))
		return console.info('Tags kept empty as per keep_empty_tags querystring variable')
	console.info('Meta-Press.es : loading default tags')
	tags_from_JSON(get_default_tags(user_lang))
}
/**
 * Set the tags search in browser data in the page.
 * @param {Object} stored_tags the tags in memory
 */
function init_tags(stored_tags) {
	tags_from_JSON(stored_tags)
	if (is_virgin_tags()) set_default_tags()
	if (is_advanced_search(del_src_sel, add_src_sel) &&
		getComputedStyle(BOMµ.id('tags_row')).display === 'none')
		BOMµ.id('tags_handle').click()
	BOMµ.id('mp_src_total').textContent = source_keys.length
	BOMµ.id('mp_src_countries').textContent = tags.country._presetChoices.length
	BOMµ.id('mp_src_langs').textContent = tags.lang._presetChoices.length
}
/* * * end tags * * */
/* * * dns_prefetch * * */
async function dns_prefetch(cur_src_sel) {
	const domain_to_prefetch = new Set()
	const all_search_urls = sµ.get_all_search_urls(cur_src_sel, source_objs)
	for (const url of all_search_urls)
		domain_to_prefetch.add(new URL(url).origin)
	for (const url of domain_to_prefetch)
		append_prefetch_link(url)
	for (const src_key of cur_src_sel)
		await DNS_resolve(new URL(src_key).host)
}
const already_add = []
function append_prefetch_link(url) {
	if (already_add.includes(url)) return
	const lnk = document.createElement('link')
	lnk.rel='dns-prefetch'
	lnk.href= url
	lnk.crossorigin = 'anonymous'
	document.head.appendChild(lnk)
	already_add.push(url)
}
/* * * end dns_prefetch * * */
function set_query_placeholder() {
	const val = [
		'La Quadrature du Net', 				'Wikipedia', 		'Repair Café', 	'Chelsea Manning',
		'5 centimeters per second', 		'Tor Browser', 	'Fairphone', 		'Frances Haugen',
		'Internet ou Minitel 2.0', 			'AsciiDoctor', 	'Yellow vests', 'Pliocene Exile',
		'Le Prince de Machiavel', 			'Nausicaä', 		'OpenStreetMap','Grimoire-Command.es',
		'Pensées pour moi-même', 				'F-Droid.org', 	'Leto Atreides','Le Petit Prince',
		'La princesse de Clèves', 			'Nuit Debout', 	'Reporterre', 	'Pablo Servigne',
		'Another World by Éric Chahi', 	'Delta.Chat', 	'OpenContacts',
	]
	BOMµ.id('mp_query').placeholder =
		mp_i18n.gettext('Search terms, ex.') +` : ${val[pick_between(0, val.length)]}`
}
BOMµ.id('tags_handle').addEventListener('click', () => {
	BOMµ.id('tags_row').classList.toggle('display_none')
	BOMµ.id('tags_2nd_row').classList.toggle('display_none')
	BOMµ.id('tags_handle').textContent = BOMµ.id('tags_handle').textContent === '+' ? '-' : '+'
	if (BOMµ.id('tags_handle').textContent === '+') BOMµ.id('list_src').style.display = 'none'
	else if (del_src_sel.length || add_src_sel.length) {
		BOMµ.id('list_src').style.display = 'block'
		BOMµ.id(del_src_sel.length ? 'v_del' : 'v_add').click()
	}
})
BOMµ.id('mp_running_btn_src').addEventListener('click', (evt) => {
	const btn_src_text = BOMµ.id('mp_running_btn_src_text')
	evt = evt.target
	evt.textContent = evt.textContent === '+' ? '-' : '+'
	btn_src_text.classList.toggle('display_none')
	for(const chd of btn_src_text.children){
		chd.style.display = btn_src_text.style.display
	}
})
function is_tag_in_querystring(QUERYSTRING) {
	if (QUERYSTRING.get('id_sch_s')) return true
	for (const tag of tag_categ)
		if (QUERYSTRING.get(tag)) return true
	if (QUERYSTRING.get('del_src_sel') || QUERYSTRING.get('add_src_sel')) return true
	return false
}
function update_add_and_del_lists(json_tags) {
	for (const value of json_tags.del_src_sel)
		src_list_del(value)
	for (const value of json_tags.add_src_sel)
		src_list_add(value)
}
let id_elt, last_elt
async function add_elt_url(id) {
	id_elt = id.split('__')[1]
	const list_elt = mp_req.findingList.items
	let not_in_future = false
	let idx = 0
	while(!not_in_future && list_elt.length) {
		const dt_elt = new Date(list_elt[idx]._values.f_epoc)
		not_in_future = new Date() > dt_elt
		if(not_in_future) {
			last_elt = dt_elt.toUTCString()
		} else { idx++ }
	}
	const store = await mµ.get_storage()
	await ss.init_table_sch_s(store)
	let sch_sea = store[`sch_sea__${id_elt}`]
	const local_url = gen_permalink(false)
	const new_url = new URL(local_url.origin + local_url.pathname + sch_sea)
	const sto_r = new_url.searchParams.get('last_res')
	const [list_p, params] = await mµ.set_text_params(new_url, mp_i18n)
	let body_txt = `${mp_i18n.gettext(' results for "')}` + list_p.q + '"\n' + params
	local_url.searchParams.delete('last_res')
	local_url.searchParams.sort()
	new_url.searchParams.delete('last_res')
	new_url.searchParams.delete('submit')
	new_url.searchParams.sort()
	if(local_url.search === new_url.search) { // if true we continue
		new_url.searchParams.set('last_res', last_elt)
		sch_sea = new_url.search
		let nb_new_res = count_new_res(sto_r) // sto_r can be null
		if(nb_new_res > 0) {
			BOMµ.id('favicon').href = '/img/favicon-metapress-active.png'
			if (nb_new_res >= 10) { nb_new_res = '+' + nb_new_res }
			body_txt = nb_new_res + mp_i18n.gettext(' new ') + body_txt
			const notif_txt = mp_i18n.gettext('New result(s) for scheduled search')
			mµ.notify_user(`${notif_txt} ${id_elt}`, body_txt)
		}
		await mµ.to_storage(`sch_sea__${id_elt}`, sch_sea)
	}
}
function count_new_res(sto_r) {
	if (!sto_r) return 0
	const sto_r_dt = new Date(sto_r)
	const results = mp_req.findingList.matchingItems
	let last = null
	let count = 0
	for(const rz of results) {
		// rz.elm.classList.remove('first_new_result', 'new_result', 'last_new_result')
		if (!rz.elm) break
		const dt_r = rz.elm.querySelector('time').dateTime
		if(sto_r_dt < new Date(dt_r)) {
			last = rz.elm
			last.classList.add('new_result')
			if (!count) last.classList.add('first_new_result')
			count += 1
		}
	}
	if(last) last.classList.add('last_new_result')
	return count
}
// date filter
const start = BOMµ.id('date_filter_sta')
const stop = BOMµ.id('date_filter_sto')
stop.disabled = true
BOMµ.id('clear_date').addEventListener('click', () => {
	start.value = ''
	stop.value = ''
	stop.min = ''
	stop.disabled = true
	filter_date_range()
})
start.value = ''
stop.value = ''
start.onchange = evt => {
	if (new Date(evt.target.value)) {  // eslint-disable-line no-constant-condition
		BOMµ.id('date_filter_sto').min = evt.target.value
		// filter_date_range()
		// reset if Start empty
		stop.disabled = evt.target.value === ''
		if (stop.disabled)
			stop.value = ''
		if (evt.target.value !== '')
			filter_date_range(evt)
	}
}
stop.onchange = evt => {
	if (new Date(evt.target.value))  // eslint-disable-line no-constant-condition
		filter_date_range()
}
/**
 * Applies a date range filter with #date_filter_sta #date_filter_sto.
 * @param {Event} evt The fired event
 */
function filter_date_range() {
	let dt_sta = new Date(BOMµ.id('date_filter_sta').value)
	const dt_sto = new Date(BOMµ.id('date_filter_sto').value)
	if (isNaN(dt_sta) || isNaN(dt_sto)) return
	dt_sto.setHours('23')
	dt_sto.setMinutes('59')
	dt_sto.setSeconds('59')
	if (dt_sta > dt_sto) dt_sta = dt_sto
	//console.log('filter_date_range', dt_sta, dt_sto)
	mp_req.findingList.filter(r => r.values().f_epoc >= dt_sta && r.values().f_epoc <= dt_sto)
	// mf_date_slicing()  // <- resets filtering
}
// end date filter
/**
 * Refresh the Meta-Press sources: source_objs, current_source_nb
 * and also refresh the ChoiceJS selection.
 * @param {string} src_name The name of the soure to get
 * @param {boolean} build_src If we need to rebuild the sources before import
 * @param {boolean} init If we are in the intended initial call
 * @param {boolean} reload If we need to reload the sources
 */
async function update_source_objs(src_name='', built_src, init=true, reload=false) {
	dt_source_objs = new Date()
	source_objs = await sµ.get_prov_src(src_name, built_src)
	if (await mµ.get_child_mode()) {
		const kids_src_objs = {}
		for (const [key, val] of Object.entries(source_objs))
			if (val.tags.tech.includes('for kids'))
				kids_src_objs[key] = val
		source_objs = kids_src_objs
	}
	source_keys = Object.keys(source_objs)
	cur_src_sel = deep_copy(source_keys)
	current_source_nb = source_keys.length
	if (TEST_SRC)
		return
	if (typeof (mp_i18n) !== 'undefined' && typeof(browser) !== 'undefined') {
		let old = await mµ.get_filters()
		if(reload) {
			// let t_id = await browser.tabs.getCurrent().then(e => e.id)
			const t_id = (await browser.tabs.getCurrent()).id
			old = (await browser.storage.local.get(`filters_${t_id}`))[`filters_${t_id}`]
			browser.storage.local.remove(`filters_${t_id}`)
		} else if (!init && is_tag_in_querystring(QUERYSTRING)) {
			old = QS_to_JSON(QUERYSTRING)
		}
		populate_tags(cur_src_sel)
		init_tags(old)
		BOMµ.id('cur_tag_perm_nb').onclick = async () => {
			BOMµ.id('cur_tag_perm_nb').classList.remove('draw_attention')
			await mµ.request_sources_perm(cur_src_sel, source_objs)
			update_nb_src()
		}
		display_show_init()
		update_nb_src()
	} else {
		populate_tags(cur_src_sel)
		let tgs = {}
		if(!init && is_tag_in_querystring(QUERYSTRING))
			tgs = QS_to_JSON(QUERYSTRING)
		init_tags(tgs)
		display_show_init()
	}
	// document.dispatchEvent(new Event('tags_src_load'))
}
/**
 * Import a JSON containing the custom source
 * https://stackoverflow.com/questions/16215771/how-to-open-select-file-dialog-via-js#answer-40971885
 * @param {Event} evt event fired
 */
/* async function import_custom_src() {
	let input = document.createElement('input')
	input.type = 'file'
	input.accept = '.json'
	input.click()
	input.onchange = async () => {
		let file = input.files.item(0)
		let txt = await file.text()
		try {
			let new_src = JSON.parse(txt)
			let old = await mµ.get_custom_src()
			let merge_src = Object.assign(old, new_src)
			mµ.set_custom_src()
			mµ.update_need_reload_src()
			update_source_objs('', Object.assign(source_objs, merge_src), false, false)
		} catch (error) {
			window.alert('File not readable (bad format or corrupted file)')
		}
	}
} */
/**
 * Export the user custom source(sf) in JSON file.
 */
/* function export_custom_src() {
		BOMµ.upload_file_to_user(`${_.ndt_human_readable()}_custom_sources.json`,
			await mµ.get_custom_src())
}*/
// BOMµ.id('import_c_src').addEventListener('click', import_custom_src)
// BOMµ.id('export_c_src').addEventListener('click', export_custom_src)
/**
 * Initialises the tab div and the interaction with its button.
 * @param {string} id_manager The id of the div which contains the tab
 * @param {Function} treatment The function called when tab changes, the tab id is passed in
 * parameter
 */
function init_tabs(id_manager) {
	const manager = document.getElementById(id_manager)
	for (const elt of manager.children) {
		elt.addEventListener('click', evt => {
			if (evt.target.classList.contains('select')) return
			for (const elt2 of manager.children)
				elt2.classList.remove('select')
			evt.target.classList.add('select')
			display_show_src()
			const cur_search = BOMµ.id('mp_v_src_search').value
			BOMµ.id('mp_clear_v_src_search').click()
			if ('v_all' === evt.target.id) {
				BOMµ.id('mp_v_src_search').value = cur_search
				BOMµ.id('mp_v_src_search').click()
			}
		})
	}
}
let display_panel
BOMµ.id('mp_edit_v_src_search').addEventListener('click', () => {
	const page_size = display_panel.page
	display_panel.page = source_keys.length
	display_panel.update()
	const command_bar = BOMµ.id('mp_v_sel')
	command_bar.style.display = 'block' !== command_bar.style.display ? 'block' : 'none'
	display_panel.page = page_size
	display_panel.update()
})
init_tabs('tab_v_src')  // source list tab selector
BOMµ.id('view_select_src').addEventListener('click', () => {
	const section = BOMµ.id('list_src')
	if (section.style.display != 'block') {
		section.style.display = 'block'
		display_show_src()
	} else section.style.display = 'none'
})
BOMµ.id('close_list_src').addEventListener('click', () => {
	BOMµ.id('list_src').style.display = 'none'
})
/**
 * Fills the List.js display src panel with the source.
 * @async
 * @function
 */
function display_show_init() {  // fill
	display_panel.clear()
	const to_add = []
	for (const [src_key, src_def] of Object.entries(source_objs)) {
		let symbol = ''
		if (src_def.overwrite)
			symbol = '&#10071;'
		if (src_def.custom)
			symbol += '&#128190;'
		const favicon_url = src_def.favicon_url
		const name = src_def.tags.name
		const country = src_def.tags.country
		const lang = src_def.tags.lang
		let src_type = src_def.tags.src_type
		src_type = is_str(src_type) ? mp_i18n.gettext(src_type) : src_type.map(
			elt => mp_i18n.gettext(elt)).join(', ')
		let res_type = src_def.tags.res_type
		res_type = is_str(res_type) ? mp_i18n.gettext(res_type) : res_type.map(
			elt => mp_i18n.gettext(elt)).join(', ')
		to_add.push({
			f_icon: favicon_url,
			f_icon_title: name,
			black: symbol,
			src_list_nom: name,
			src_list_lang: lang,
			src_list_lang_name: LANG_NAME.of(lang),
			src_list_country: country,
			src_list_type: src_type,
			src_list_result_type: res_type,
			src_list_url : src_key,
			src_list_edit:`${µrl.domain_part(document.baseURI)}/html/new_source.html?name=${src_key}`,
			src_list_del: src_key,
			src_list_add: src_key,
			src_list_only: src_key
		})
	}
	display_panel.add(to_add, () => {
		display_panel.sort('src_list_nom', { order: 'asc' })
		display_show_src()
	})
}
function src_list_del (src_key) {
	if (source_keys.includes(src_key))
		arr.array_add_once(del_src_sel, src_key)
	arr.array_remove_once(cur_src_sel, src_key)
	arr.array_remove_once(add_src_sel, src_key)
}
function src_list_add(src_key) {
	arr.array_remove_once(del_src_sel, src_key)
	if (source_keys.includes(src_key)) {
		arr.array_add_once(cur_src_sel, src_key)
		arr.array_add_once(add_src_sel, src_key)
	}
}
function update_display_panel() {
	for (const itm of display_panel.visibleItems) {
		const li = itm.elm
		li.querySelector('.src_list_del').onclick = evt => {
			const key = evt.target.dataset.src_url
			const current_tab = BOMµ.$('#tab_v_src .select').id
			if (current_tab === 'v_del')
				arr.array_remove_once(del_src_sel, key)
			else
				src_list_del(key)
			on_tag_sel()
		}
		li.querySelector('.src_list_add').onclick = evt => {
			const key = evt.target.dataset.src_url
			src_list_add(key)
			on_tag_sel()
		}
		li.querySelector('.src_list_only').onclick = evt => {
			const key = evt.target.dataset.src_url
			arr.array_remove_once(del_src_sel, key)
			cur_src_sel = []
			add_src_sel = []
			arr.array_add_once(cur_src_sel, key)
			arr.array_add_once(add_src_sel, key)
			tags.tech.setChoiceByValue('cherry-pick_sources')
			on_tag_sel()
			if (BOMµ.id('mp_query').value)
				BOMµ.id('mp_submit').click()
		}
		const add = li.querySelector('.src_list_add')  // display the corresponding button
		const del = li.querySelector('.src_list_del')
		const key = del.dataset.src_url
		const current_tab = BOMµ.$('#tab_v_src .select').id
		del.style.display = current_tab === 'v_del' ? 'block' : 'none'
		add.style.display = 'block'
		if (cur_src_sel.includes(key)) {
			li.classList.add('selected')
			add.style.display = 'none'
			del.style.display = 'block'
		} else li.classList.remove('selected')
		if (['v_select', 'v_all'].includes(current_tab)) {
			BOMµ.id('mp_v_src_del_page').style.display = 'none'
			BOMµ.id('mp_v_src_del_all').style.display = 'none'
		} else {
			BOMµ.id('mp_v_src_del_page').style.display = 'inline-block'
			BOMµ.id('mp_v_src_del_all').style.display = 'inline-block'
		}

	}
}
/**
 * Refresh the filter used when the sources to display change.
 */
function display_show_src() {
	if (!is(display_panel)) return
	const cur_itm_nb = display_panel.i
	const cur_pg_size = display_panel.page
	display_panel.filter()
	const cur_tab = BOMµ.$('#tab_v_src .select').id
	if (cur_tab !== 'v_all') {
		let cur_content
		switch (cur_tab) {
		case 'v_select': cur_content = cur_src_sel ;break
		case 'v_add': cur_content = add_src_sel ;break
		case 'v_del': cur_content = del_src_sel ;break
		}
		display_panel.filter(item => cur_content.includes(item.values().src_list_url))
	}
	if (cur_itm_nb) {
		display_panel.show(cur_itm_nb, cur_pg_size)
	}
}
BOMµ.id('mp_v_src_del_all').addEventListener('click', () => {
	const current_tab = BOMµ.$('#tab_v_src .select').id
	if (current_tab === 'v_del')
		del_src_sel = []
	on_tag_sel()
})
BOMµ.id('mp_v_src_del_page').addEventListener('click', () => {
	const current_tab = BOMµ.$('#tab_v_src .select').id
	if (current_tab === 'v_del')
		for (const itm of display_panel.visibleItems)
			arr.array_remove_once(del_src_sel,
				itm.elm.querySelector('.src_list_del').dataset.src_url)
	else
		for (const itm of display_panel.visibleItems)
			src_list_del(itm.elm.querySelector('.src_list_del').dataset.src_url)
	on_tag_sel()
})
BOMµ.id('mp_v_src_sel_all').addEventListener('click', () => {
	for (const src_key of source_keys)
		src_list_add(src_key)
	on_tag_sel()
})
BOMµ.id('mp_v_src_sel_page').addEventListener('click', () => {
	for (const itm of display_panel.visibleItems)  // .src_list_del always exists
		src_list_add(itm.elm.querySelector('.src_list_del').dataset.src_url)
	on_tag_sel()
})
let search_in_src_callback_timeout  // search in list
BOMµ.id('mp_v_src_search').addEventListener('keyup', search_v_src)
BOMµ.id('mp_v_src_search').addEventListener('click', search_v_src)
/**
 * Search in the display src panel.
 * @param {Event} evt the event fired
 */
function search_v_src(evt) {
	clearTimeout(search_in_src_callback_timeout)
	search_in_src_callback_timeout = setTimeout(() => {
		// bug in listjs 2.3.1 with dots in keywords so we replace them with spaces
		// display_panel.search(triw(evt.target.value).replace('.', ' '), [
		// From MP v1.8.15 we're using lovasoa/ListJS 2.3.5
		display_panel.search(triw(evt.target.value), [
			'src_list_nom',
			'src_list_lang',
			'src_list_country',
			'src_list_type',
			'src_list_result_type',
			'src_list_url'
		])
	}, evt.keyCode === 13 ? 0 : 500)
}
BOMµ.id('mp_clear_v_src_search').addEventListener('click', () => {  // Clear the source list search
	BOMµ.id('mp_v_src_search').value = ''
	display_panel.search()
})
BOMµ.id('mp_clear_query').addEventListener('click', () => {
	BOMµ.id('mp_query').value = ''
	BOMµ.id('mp_query').focus()
})
/**
 * Update all the source favicons with {@link get_favicon} and the headline_rss, then sends
 * a new sources.json file to the user.
 * @memberof new_update_sources
 * @async
 */
// export async function update_favicon_rss(source_objs) {
export async function update_source_definitions () {
	const raw_src = await sµ.get_raw_src()
	let idx = 0
	for (const src_key of Object.keys(raw_src)) {
		console.log(idx++)
		const src_def = raw_src[src_key]
		if (typeof (src_def.tags.positive_test_search_term) !== 'undefined')
			delete src_def.tags.positive_test_search_term
		raw_src[src_key] = src_def
	}
	const json_str = sµ.src_to_json(raw_src)
	BOMµ.upload_file_to_user(`${ndt_human_readable()}_favicon_updated_sources.json`, json_str)
}
async function init() {
	MAX_RES_BY_SRC = await mµ.get_max_res_by_src()
	if (TEST_SRC) {
		const src_names = QUERYSTRING.getAll('name')
		await update_source_objs(src_names, await mµ.get_built_src(0), false)  // force reload src
	} else {
		HEADLINE_PAGE_SIZE = await mµ.get_news_page_size()
		user_lang = QUERYSTRING.get('lang') || await mµ.get_wanted_locale()
		mp_i18n = await gtt.gettext_html_auto(user_lang) // must be before populate_tags
		BOMµ.set_HTML_lang(user_lang)
		LANG_NAME = new Intl.DisplayNames([user_lang], {type: 'language'})
		COUNTRY_NAME = new Intl.DisplayNames([user_lang], {type: 'region'})
		ALT_LOAD_TXT = mp_i18n.gettext('Click to load image')
		IS_LOAD_IMG = await mµ.get_load_photos()
		IS_UNDUP = await mµ.get_undup_results()
		manifest = await mµ.get_manifest()
		display_panel = new List('list_src_t', {
			indexAsync: true,
			item: 'src_item_tpl',
			valueNames: [
				{ name: 'f_icon', attr: 'src' },
				{ name: 'f_icon_title', attr: 'title' },
				'src_list_nom',
				'src_list_country',
				'src_list_lang',
				'src_list_lang_name',
				'src_list_type',
				'src_list_result_type',
				'black',
				{ name: 'src_list_url', attr: 'href' },
				{ name: 'src_list_edit', attr: 'href' },
				{ name: 'src_list_del', attr: 'data-src_url' },
				{ name: 'src_list_add', attr: 'data-src_url' },
				{ name: 'src_list_only', attr: 'data-src_url' }
			],
			page: 20,
			pagination: {
				outerWindow: 3,
				innerWindow: 3,
				item: '<li><a class="page" href="#list_src_t"></a></li>'
			},
		})
		const news_loading = document.getElementById('news_loading')
		news_loading.checked = await mµ.get_news_loading()
		news_loading.addEventListener('change', () => {
			mµ.to_storage('news_loading', news_loading.checked)
		})
		const ld_src_dt_start = new Date()
		// Calls populate_tags
		await update_source_objs('', await mµ.get_built_src(dt_source_objs), false)
		console.info('Meta-Press.es : loading source', new Date() - ld_src_dt_start, 'ms')
		// console.info({source_objs})
		if (QUERYSTRING.get('xgettext'))
			await gtt.xgettext_html()  // is after populate_tags (must be called from english)
		document.onvisibilitychange = async() =>	{  // Update sources if tab becomes visible again
			if (document.visibilityState !== 'visible') return
			if (!await mµ.is_need_reload_src(dt_source_objs)) return
			update_source_objs('', mµ.get_built_src(dt_source_objs), false, true)
			// let tab_id = await browser.tabs.getCurrent().then(e => e.id)
			const tab_id = (await browser.tabs.getCurrent()).id
			mµ.to_storage(`filters_${tab_id}`, tags_values())
		}
		display_panel.on('updated', update_display_panel)
		if(await mµ.get_sentence_search())
			BOMµ.id('mp_query').addEventListener('input', () => {
				const query = BOMµ.id('mp_query').value
				const mw_query = new RegExp('[^ ]\\s\\s*[^ ]')
				if(mw_query.test(query))
					toggle_sentence_search_tags('one word', 'many words')
				else
					toggle_sentence_search_tags ('many words', 'one word')
			})
		if(QUERYSTRING.get('id_sch_s') && !QUERYSTRING.get('submit')) {
			BOMµ.id('add_auto_s_text').textContent = mp_i18n.gettext('save modification')
			BOMµ.id('add_auto_s_new').style.display = 'none'
			BOMµ.id('add_auto_s_save').style.display = 'inline-block'
		}
		set_query_placeholder()
		BOMµ.id('mp_version').textContent = 'v' + manifest.version
		BOMµ.dropdown_alive()
		// xµ.check_current_storage_health()  // was only needed for sync storage
		// sµ.update_favicon_rss(source_objs)
	}
	mp_req.metaFindingList.on('updated', attach_metaFindingList_evts)
	BOMµ.id('mf_total').addEventListener('click', () => {
		mp_req.findingList.search()
		apply_result_searchs()
		radio_bold_clicked(BOMµ.$('#mf_total > .mf_name'), 'mf_name')
	})
	if (QUERYSTRING.get('submit'))
		BOMµ.id('mp_submit').click()
	else {
		dns_prefetch(cur_src_sel)
	}
	document.body.classList.add('javascript_loaded')
}
init()
