// name		: test_sources.js
// SPDX-FileCopyrightText: 2021-2024 Simon Descarpentres <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
/* globals List */
/* globals document Event setTimeout URL window console */

// import * as _		from '../js_utils.js'
import { deep_copy } from '../lib/js/object.js'
import { is } from '../lib/js/types.js'
import { triw } from '../lib/js/text.js'
import * as arr from '../lib/js/array.js'
import { id, $, $$ } from '../lib/browser/BOM_utils.js'
import * as mµ	from '../mp_utils.js'
import * as st	from '../lib/source_testing.js'

const DEFAULT_SEARCH_TERMS_TAB_NB = 4
// const POPUP_MAXIMUM = id('mp_display_result_test').dataset.pool
const INTER_POPUP_DELAY = 1000
const DEFAULT_SEARCH_TERMS = {
	// 'one word': ['Esperanto', 'Biden', 'alternatiba', 'Quadrature du Net'],
	'one word': ['europe'],
	// 'many words': ['Quadrature du net', 'Watergate', 'Gilets Jaunes', 'Jaunes Gilets'],
	'many words': ['europe'],
	'approx': ['europe']
}
const RES_FRAMES = [ 'test_integrity', 'test_src']
let date_before_test_src, date_after_test_src
let source_objs

/**
 * Returns the default search terms depending on tech tags : "many words", "one word", "approx"…
 * @param {Array} the tech tags of a source
 * @returns {Array} the terms
 */
function get_default_terms(src_tech_tags) {
	if(src_tech_tags.includes('many words')) {
		return DEFAULT_SEARCH_TERMS['many words']
	} else if (src_tech_tags.includes('one word')) {
		return DEFAULT_SEARCH_TERMS['one word']
	} else {
		return DEFAULT_SEARCH_TERMS.approx
	}
}
function reset_all_src_for_query() {
	const all_src_for_query = {}
	for(const val of Object.values(DEFAULT_SEARCH_TERMS))
		for(const elt of val)
			if(!Object.keys(all_src_for_query).includes(elt.toLowerCase()))
				all_src_for_query[elt.toLowerCase()] = {not_rdy: [], rdy: []}
	return all_src_for_query
}
/*
function make_report_card(elt) {
	let elt2 = document.getElementById('test-item').cloneNode(true)
	elt2.querySelector('.f_h1').textContent = elt.f_h1
	elt2.querySelector('.src_notes').textContent = elt.src_notes
	elt2.querySelector('.src_warn_mask').textContent = elt.src_warn_mask
	elt2.querySelector('.warn_mask_label').id = elt.warn_mask_label
	elt2.querySelector('.src_id').id = elt.src_id
	elt2.querySelector('.f_icon').src = elt.f_icon
	elt2.querySelector('.f_name').title = elt.f_name
	elt2.querySelector('.src_key').dataset.src_key = elt.src_key
	elt2.querySelector('.name_src').dataset.name_src = elt.name_src
	return elt2
}*/
/**
 * Test (integrity, unity and JSON diff) for the given sources.
 * @param {Object} source_objs The sources
 */
function before_test_sources(source_objs, integrity_only) {
	id('mp_display_report_test').style.display = 'inline-block'
	const elt = id('mp_display_result_test')
	date_before_test_src = new Date().getTime()
	// mp_test.testList.clear()
	const all_src_for_query = reset_all_src_for_query()
	const src_objs_entries = Object.entries(source_objs)
	NB_TESTS = src_objs_entries.length
	for(const [tested_src_k, tested_src_v] of src_objs_entries) {
		const src_tested_name = tested_src_v.tags.name
		const src_tested_favicon = tested_src_v.favicon_url
		get_all_search_terms(tested_src_k, tested_src_v, all_src_for_query)
		mp_test.testList.add({
			f_h1: src_tested_name,
			src_notes: tested_src_v.tags.notes || '',
			src_warn_mask: tested_src_v.tags.warn_mask || '',
			warn_mask_label: tested_src_v.tags.warn_mask & 'Mask: ' || '',
			src_id: `report_${tested_src_k}`,
			f_icon: src_tested_favicon,
			f_name: tested_src_k,
			src_key: tested_src_k,
			name_src: src_tested_name
		})
		// workaround List.js to verify it was this that was slowing things down
		/*document.getElementById('mp_report').appendChild(make_report_card({
			f_h1: src_tested_name,
			src_notes: tested_src_v.tags.notes || '',
			src_warn_mask: tested_src_v.tags.warn_mask || '',
			warn_mask_label: tested_src_v.tags.warn_mask & 'Mask: ' || '',
			src_id: `report_${tested_src_k}`,
			f_icon: src_tested_favicon,
			f_name: tested_src_k,
			src_key: tested_src_k,
			name_src: src_tested_name
		}))*/
		let src_test_btn = id(`btn_${tested_src_k}`)
		if(!src_test_btn) {
			src_test_btn = document.createElement('button')
			src_test_btn.id = `btn_${tested_src_k}`
			src_test_btn.title = src_tested_name
			src_test_btn.addEventListener('click', (evt) => {
				for (const btn of document.querySelectorAll('.btn_test'))
					btn.classList.remove('btn_test_actif')
				evt.target.classList.add('btn_test_actif')
				id('mp_filter_report').value = src_tested_name
				id('mp_filter_report').dispatchEvent(new Event('input'))
			})
			elt.append(src_test_btn)
		}
		src_test_btn.classList.add('btn_test', 'default')
		src_test_btn.textContent = ' '
		const report_div = id(`report_${tested_src_k}`)
		report_div.getElementsByClassName('btn_retest')[0].addEventListener('click',  () => {
			for(const elt of $$(`[id='report_${tested_src_k}'] [data-query]`))
				open_query_tab([tested_src_k], elt.dataset.query)
		})
		for (const elt of RES_FRAMES) {
			report_div.getElementsByClassName(`${elt}_btn`)[0].addEventListener('click', () => {
				report_div.getElementsByClassName(`${elt}`)[0].classList.toggle('display_none')
				report_div.getElementsByClassName(`${elt}_btn`)[0].getElementsByClassName(
					'fld_ico')[0].classList.toggle('fld_ico_hide')
			})
		}
	}
	test_sources(all_src_for_query, source_objs, integrity_only)
}
function test_stats() {
	id('test_volume').textContent = $$('.btn_test').length
	id('mp_test_results').style.display = 'inline'
	test_stats_ticker()
}
export function test_stats_ticker() {  // not used anywhere else
	update_test_stats()
	if($$('button.default').length > 0)
		setTimeout(test_stats_ticker, INTER_POPUP_DELAY)
}
function update_test_stats() {
	// id('mp_clear_filter_report').click()  // slows things down with per-source reporting
	for (const frm of RES_FRAMES) {
		const warning = $$(`.${frm} .warn`).length
		const error = $$(`.${frm} .error`).length
		if(frm === 'test_src'){
			const success = $$(`.${frm} a.success`).length
			const success_rate = Math.floor(success / $$('.btn_test').length * 100)
			const net_err = $$(`.${frm} a.network_error`).length
			id('stat_V').textContent = `${success_rate}% ${success}`
			id('stat_N').textContent = `${net_err}`
			if (success + net_err + warning + error	=== NB_TESTS)
				id('mp_filter_report').addEventListener('input', () => filter_src_test(false))
			id('stat_W').textContent = `${warning}`
			id('stat_X').textContent = `${error}`
		} else {
			id(frm).textContent = `${warning} W ; ${error} X`
		}
	}
	date_after_test_src = new Date().getTime()
	id('test_duration').textContent = (date_after_test_src - date_before_test_src) / 1000
}
/**
 *
 */ /*
async function retest_sources() {
	id('mp_test_results').style.display = 'none'
	mp_test.testList.filter()
	let all_src_for_query = reset_all_src_for_query()
	let err_src = $$('.test_src[class*=error_], .test_src.default_, .test_src.warn_')
	// also matches .test_src.no_result_error_
	for(let err_src_tested of err_src) {
		let src_key_to_retest = err_src_tested.dataset.src_key
		get_all_search_terms(src_key_to_retest, source_objs[src_key_to_retest], all_src_for_query)
		st.edit_style_class(err_src_tested, 'default')
		err_src_tested.textContent = ''
		let btn_mat = id(`btn_${src_key_to_retest}`)
		btn_mat.textContent = 'O'
		st.edit_style_class(btn_mat, 'default')
		for (const frm of RES_FRAMES)
			st.edit_style_class(µ.$(`[id='report_${src_key_to_retest}'] .${frm}_btn`), 'default')
	}
	st.clear_reserved_names()
	test_sources(all_src_for_query, source_objs)
	test_stats()
}*/
function test_sources(all_src_for_query, source_objs, integrity_only=false){
	id('test_queries').textContent = Object.keys(all_src_for_query).length
	for(const val of Object.values(all_src_for_query)) {
		const clone_all = deep_copy(val.not_rdy)
		let cur_src
		for(const tested_src of clone_all) {
			cur_src = source_objs[tested_src]
			st.test_src_integrity(source_objs, tested_src, (ready_to_test) => {
				const src_tags_tech = cur_src.tags.tech
				const terms = cur_src.positive_test_search_term || get_default_terms(src_tags_tech)
				for (const elt of ready_to_test[1])
					st.add_err(cur_src, 0, elt.text, elt.level, 'test_integrity')
				if (integrity_only)
					return
				for(let query of terms){
					query = query.toLowerCase()
					arr.array_remove_once(all_src_for_query[query].not_rdy, tested_src)
					if (ready_to_test[0])
						all_src_for_query[query].rdy.push(tested_src)
					if(all_src_for_query[query].not_rdy.length === 0) {
						const src_for_query = all_src_for_query[query].rdy
						const src_for_query_nb = src_for_query.length
						const slice_size = src_for_query_nb / DEFAULT_SEARCH_TERMS_TAB_NB
						let slice_end = slice_size
						if (src_for_query_nb > 400) {  // for more parallelisation
						// if (src_for_query_nb > 400 && false) {  // to run on dual-core only
							for (let slc = 0; slc < src_for_query_nb - (slice_size / 2); slc += slice_size){
								open_query_tab(src_for_query.slice(slc, slice_end), query)
								slice_end += slice_size
							}
						} else {
							open_query_tab(all_src_for_query[query].rdy, query)
						}
					}
				}
			})
		}
	}
}
function open_query_tab(tested_srcs, tested_src_query) {
	const local_url = new URL(window.location)
	const elt = id('mp_display_result_test')
	const pool = parseInt(elt.dataset.pool)
	if(pool > 0) {
		elt.dataset.pool = pool - 1
		const new_url = new URL(local_url.origin + '/html/index.html')
		new_url.searchParams.set('q', tested_src_query)
		for(const src_key of tested_srcs){
			const elt = $(`[id='report_${src_key}'] .test_src`)
			st.add_result_test_to_query(
				source_objs[src_key], elt, '', tested_src_query, 'O', 'default')
			// new_url.searchParams.set('tech', 'cherry-pick_sources')
			// new_url.searchParams.append('add_src_sel', src_key)
			new_url.searchParams.append('name', source_objs[src_key].tags.name)
		}
		new_url.searchParams.set('submit', 1)
		new_url.searchParams.delete('launch_test')
		new_url.searchParams.set('test_sources', 1)
		window.open(new_url.toString())
	} else {
		setTimeout(function () {open_query_tab(tested_srcs, tested_src_query)}, INTER_POPUP_DELAY)
	}
}
const mp_test = {
	testList: new List('mp_display_test', {
		item: 'test-item',
		valueNames: ['f_h1','src_notes', 'src_warn_mask', 'warn_mask_label',
			{ name: 'src_id', attr: 'id' },
			{ name: 'f_icon', attr: 'src' },
			{ name: 'f_name', attr: 'title'},
			{ name: 'src_key', attr: 'data-src_key'},
			{ name: 'name_src', attr:'data-name_src'},
			{ name: 'toggle_fct', attr:'script'}
		]
	})
}
// id('mp_search_filter_report').addEventListener('click', () => filter_src_test(true))
id('mp_clear_filter_report').addEventListener('click', () => {
	id('mp_filter_report').value = ''
	mp_test.testList.filter()
})
/*
 * Search through ListJS of test result cards
 */
function filter_src_test(strict_equal) {
	let val = id('mp_filter_report').value
	val = triw(val).toLowerCase()
	if(strict_equal && val !== '')
		mp_test.testList.filter(i => i.values().f_h1.toLowerCase() === val)
	else {
		mp_test.testList.filter(i => i.values().f_h1.toLowerCase().includes(val))
	}
}
function get_all_search_terms (src_key, src, all_src_for_query) {
	const src_positive_sch_terms = src.positive_test_search_term
	const src_tags_tech = src.tags.tech
	let src_tested_query
	if (is(src_positive_sch_terms)) {
		if (Array.isArray(src_positive_sch_terms)) {
			src_tested_query = src_positive_sch_terms
			for (const val of src_tested_query)
				if (!all_src_for_query[val.toLowerCase()])
					all_src_for_query[val.toLowerCase()] = {not_rdy: [], rdy: []}
		} else console.warn(`${src_key} definition of positive_test_search_terms should be a list`)
	} else src_tested_query = get_default_terms(src_tags_tech)
	for (const val of Object.values(src_tested_query))
		all_src_for_query[val.toLowerCase()].not_rdy.push(src_key)
}

let NB_TESTS = 50
async function init() {
	await mµ.set_theme()
	source_objs = await mµ.get_built_src(new Date())
	const src_keys = Object.keys(source_objs)
	id('mp_start_test').addEventListener('click', async(evt) => {
		evt.target.style.display = 'none'
		if(await mµ.request_sources_perm(src_keys, source_objs)) {
			st.clear_reserved_names()
			before_test_sources(source_objs)
			test_stats()
		}
	})
	id('mp_start_test_nb').addEventListener('click', async(evt) => {
		let src_test_nb = 0
		const need_test = Math.min(src_test_nb + NB_TESTS, src_keys.length)
		const src_test = {}
		let cur_src_key
		for (let idx = src_test_nb; idx < need_test; idx++) {
			cur_src_key = src_keys[idx]
			src_test[cur_src_key] = source_objs[cur_src_key]
		}
		evt.target.style.display = 'none'
		if(await mµ.request_sources_perm(src_keys, source_objs)) {
			before_test_sources(src_test)
			test_stats()
		}
		evt.target.style.display = 'inline-block'
		src_test_nb += NB_TESTS
		if (src_test_nb >= src_keys.length) {
			st.clear_reserved_names()
			src_test_nb = 0
		}
	})
	id('update_stats').addEventListener('click', () => {
		update_test_stats()
	})
	id('mp_test_integrity').addEventListener('click', () => {
		document.body.classList.add('waiting')
		setTimeout(() => {
			before_test_sources(source_objs, true)
			update_test_stats()
			document.body.classList.remove('waiting')
		}, 0)
	})
	id('mp_toggle_ok_display').addEventListener('click', () => {
		for (const elt of $$('.btn_test.success'))
			elt.classList.toggle('display_none')
	})
	id('mp_turn_on_search').addEventListener('click', () => {
		id('mp_filter_report').addEventListener('input', () => filter_src_test(false))
	})
	id('mp_retry_net_err').addEventListener('click', () => {
		const src_test = {}
		let cur_src_key
		for (const elt of $$('.btn_test.network_error')) {
			cur_src_key = elt.id.replace(/^btn_/, '')
			src_test[cur_src_key] = source_objs[cur_src_key]
		}
		before_test_sources(src_test)
		test_stats()
	})
	document.body.classList.add('javascript_loaded')
}
init()
