// SPDX-Filename: ./welcome.js
// SPDX-FileCopyrightText: 2021-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
import * as mµ from '../mp_utils.js'
import * as gtt from '../gettext_html_auto.js/gettext_html_auto.js'

async function init() {
	await mµ.set_theme()
	const QUERYSTRING = new URL(window.location).searchParams
	if (QUERYSTRING.get('xgettext'))
		await gtt.xgettext_html()
	const userLang = await mµ.get_wanted_locale()
	/*const mp_i18n = */await gtt.gettext_html_auto(userLang)
	document.body.classList.add('javascript_loaded')
}
init()
