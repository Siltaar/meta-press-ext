// SPDX-FileName: ./search_worker.js
// SPDX-FileCopyrightText: 2025-2025 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

import * as arr from '../lib/js/array.js'
import { DNS_resolve } from '../lib/DNS_resolver.js'
import * as mp  from '../core/source_fetching.js'
import * as jsdom from 'https://esm.sh/jsdom-deno@19.0.2'


let findings = []
let meta_findings = []
let len_src_sel = 0
const DOM_parser = typeof (DOMParser) !== 'undefined' && new DOMParser() || init_JSDOMParser()


function init_JSDOMParser(dbg) {
	const virtualConsole = new jsdom.VirtualConsole()
	virtualConsole.on('error', () => { /* No-op to skip console errors. */})
	let JSDOM_args = {virtualConsole}
	const dt = () => new Date().toISOString()
	if (dbg > 2) {
		JSDOM_args = {}
	}
	// Generating the DOM parser
	// Node DOMParser https://github.com/fb55/htmlparser2
	const JSDOMParser = {
		parseFromString: function (a, c_type) {
			// console.debug('parseFromString', c_type)
			JSDOM_args = Object.assign({}, JSDOM_args, {
				// url: "https://example.com/",
				// referrer: "https://example.org/",
				contentType: c_type,
				storageQuota: 100000000,
			})
			const DOM = new jsdom.JSDOM (a, JSDOM_args)
			if (DOM && DOM.window && DOM.window.document)
				return DOM.window.document
			else {
				console.debug('Failed to parse', String(a.slice(0,60), c_type))
				return undefined
			}
		}
	}
	return JSDOMParser
}

async function load_source(worker_id, src_key, src_def, search_terms, dbg, user_lang) {
	const rz = await mp.search_source(worker_id,
		src_key, src_def, search_terms, (dbg > 1), user_lang, DOM_parser)
	rz.worker_id = worker_id
	postMessage(rz)  // send partial results
}

async function search_sources(worker_id, search_terms, src_keys, src_objs, dbg,	user_lang) {

	const force_log = console.log
	const force_trace = console.trace
  console.log   = () => {}  // no op
  console.info  = () => {}
  console.warn  = () => {}
  console.error = () => {}
  console.debug = () => {}
  console.trace = () => {}
  const dt = () => new Date().toISOString()
  if (dbg) {
    console.info  = (...txt) => force_log('[I]', dt(), ...txt)
    if (dbg > 1) {
	    console.warn  = (...txt) => force_log('[W]', dt(), ...txt)
	    console.error  = (...txt) => force_log('[E]', dt(), ...txt)
    	console.debug  = (...txt) => force_log('[D]', dt(), ...txt)
    }
    if (dbg > 2) {
      console.log = force_log
      console.trace = force_trace
    }
  }

  let next_req_delay
  arr.shuffle_array(src_keys)
  let domain_ip = null
  let last_fetch_date = null
  let now_dt = null
  const SERIAL_QUERIES_DBG = false
  const last_seen_IPs = {}
	const resolved_search_hosts = {}
  let src_host = ''
  let first_ip
	len_src_sel = src_keys.length
  // for (let src_key of current_source_selection.slice(0, 499)) {
  for (const src_key of src_keys) {
    SERIAL_QUERIES_DBG && console.debug(worker_id, 'will load', src_key)
    // console.debug('will', search_cb.name, src_key)
    src_host = new URL(src_key).host
    domain_ip = resolved_search_hosts[src_host]
    if (!domain_ip) {
      const addrs = await DNS_resolve(src_host)
      if (addrs.length) {
        first_ip = addrs[0]
        resolved_search_hosts[src_host] = first_ip
        domain_ip = first_ip
      }
    }
    // console.log(src_key, domain_ip)
    if (domain_ip) {
      now_dt = new Date().getTime()
      last_fetch_date = last_seen_IPs[domain_ip]
      const should_wait = last_fetch_date && last_fetch_date > (now_dt - mp.INTER_FETCH_DELAY)
      const next_fetch_date = last_fetch_date + mp.INTER_FETCH_DELAY
      next_req_delay = should_wait ? next_fetch_date - now_dt : 0
      should_wait && console.debug(worker_id, src_key, 'will wait', next_req_delay, 'ms')
      last_seen_IPs[domain_ip] = next_fetch_date || now_dt
    } else {
      // next_req_delay = mp.INTER_FETCH_DELAY / 100
      next_req_delay = 0
    }
    const src_def = src_objs[src_key]
    if (SERIAL_QUERIES_DBG) {
      await load_source(worker_id, src_key, src_def, search_terms, dbg, user_lang)
    } else {
      setTimeout(() => load_source(worker_id, src_key, src_def, search_terms, dbg, user_lang),
				next_req_delay)
    }
  }
  console.debug(worker_id, 'finished launching search_cbs')
}

onmessage = async (e) => {
	await search_sources(...(e.data))
}
