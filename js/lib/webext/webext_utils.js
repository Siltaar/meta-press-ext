// SPDX-FileName: ./webext_utils.js
// SPDX-FileCopyrightText: 2022 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
/* globals browser fetch console window alert Notification */

import { fetch_manifest } from '../../mp_snippets.js'

const is_browser = typeof browser === 'undefined' ? null : browser

/**
 * Execute an function in background and return a promise or false if not tound.
 * @param {string} function_name the name
 * @param {Array} params The parameter in order
 * @returns {Promise}
 */
export function exec_in_background(function_name, params=[]) {
	if (!is_browser) return null
	return browser.runtime.sendMessage(browser.runtime.id,
		{'function': function_name, 'params': params})
}
/**
 *
 */
export async function get_manifest() {
	if (!is_browser) {
		return fetch_manifest()
	}
	return browser.runtime.getManifest()
}
/**
 * Remove all CORS permissions
 * @async
 */
export async function browser_drop_host_perm () {
	if (!is_browser) return
	const perm = await browser.permissions.getAll()
	// console.log(`Host permissions to drop : ${perm.origins.join(', ')}`)
	// console.log(`API permissions to drop : ${perm.permissions.join(', ')}`)
	browser.permissions.remove({origins: perm.origins})
}
/**
 * Fetch browser current permissions
 * @async
 * @returns {browser.permissions.getAll()}
 */
export async function get_all_permissions() {
	if (!is_browser) return {origins:[]}
	return await browser.permissions.getAll()
}
/**
 * Request special webext browser permissions to user
 */
export function request_browser_origin_perm (perm) {
	if (!is_browser) return
	return browser.permissions.request(perm)
}
/* For Local Storage
export function get_local_stored(key, default_value) {
	let local_data = localStorage.getItem(key)
	return local_data == null ? default_value : local_data
}*/
// const MAX_STORAGE_ITEM_SIZE = 8192	// in bytes, storage.StorageArea.getBytesInUse() to check
const MAX_STORAGE_ITEM_COUNT = 512
const MAX_STORAGE_TOTAL_SIZE = 102400  // in bytes
// https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/storage/sync
export async function check_sync_storage_health () {
	if (!is_browser) return
	const cur_storage = await browser.storage.sync.get()
	const cur_storage_item_count = Object.keys(cur_storage).length
	const cur_storage_total_size = JSON.stringify(cur_storage).length
	console.log('browser.storage.sync item count: ' +
		`${Math.round(cur_storage_item_count / MAX_STORAGE_ITEM_COUNT * 100)} % ` +
		`(${cur_storage_item_count} / ${MAX_STORAGE_ITEM_COUNT})`)
	console.log('browser.storage.sync total size: ' +
		`${Math.round(cur_storage_total_size / MAX_STORAGE_TOTAL_SIZE * 100)} % ` +
		`(${cur_storage_total_size} / ${MAX_STORAGE_TOTAL_SIZE})`)
	if (MAX_STORAGE_ITEM_COUNT - cur_storage_item_count < MAX_STORAGE_ITEM_COUNT / 2)
		console.warn('More than half the authorized browser.storage.sync item count is used')
	if (MAX_STORAGE_ITEM_COUNT - cur_storage_item_count < MAX_STORAGE_ITEM_COUNT / 10)
		alert('You are using too much browser storage items, less than 10% left')
	if (MAX_STORAGE_TOTAL_SIZE - cur_storage_total_size < MAX_STORAGE_TOTAL_SIZE / 2)
		console.warn('More than half the authorized browser.storage.sync total size is used')
	if (MAX_STORAGE_TOTAL_SIZE - cur_storage_total_size < MAX_STORAGE_TOTAL_SIZE / 10)
		alert('You are using too much browser storage, less than 10% left')
}
/*
 *
 */
const MAX_LOCAL_STORAGE_SIZE = 1024 * 1024 * 5
export async function check_local_storage_capacity() {
	if (!is_browser) return
	const storage_local_length = JSON.stringify(await browser.storage.local.get()).length
	const local_storage_capacity = (storage_local_length / (1024*1024)).toFixed(2)
	console.log('storage.local', local_storage_capacity, 'Mo over 5 Mo')
	if (MAX_LOCAL_STORAGE_SIZE - storage_local_length < MAX_LOCAL_STORAGE_SIZE / 2)
		console.warn('More than half the authorized browser.storage.local total size is used')
	if (MAX_LOCAL_STORAGE_SIZE - storage_local_length < MAX_LOCAL_STORAGE_SIZE / 10)
		alert('Meta-Press.es is using too much browser storage, less than 10% left. Are you using'+
			'a lot of locally defined custom sources ?')
}
/*
 *
 */
export function get_storage() {
	if (!is_browser) return {}
	return browser.storage.local.get()
}
/**
 * Gets or sets the browser storage value of the given key, using given default value if needed
 * @param {string} key The storage key
 * @param {Object} default_value the wanted default value for this key
 * @returns {Object} The stored value if available, the default value otherwise
 */
export async function get_stored(key, default_value) {
	if (!is_browser) return default_value
	const val = (await browser.storage.local.get(key))[key]
	// console.log('get_stored', 'key', key, 'val', val)
	if(typeof(val) === 'undefined' || val === null || val === '') {
		await try_store(key, default_value)
		return default_value
	}
	return val
}
/**
 * Set the value of the given key in the browser storage, or the given default value
 * @param {string} key The storage key
 * @param {Object} val The value for this key
 */
function to_browser_storage(key, val) {
	if (!is_browser) return
	const storage_obj = {}
	storage_obj[key] = val
	return browser.storage.local.set(storage_obj)
	// browser.storage.sync.set(storage_obj)
	// localStorage.setItem(key, val)  // for local storage
}
/*
 *
 */
export function try_store(key, val) {
	/*if (JSON.stringify(val).length < MAX_STORAGE_ITEM_SIZE)
		to_browser_storage(key, val)
	else
		console.trace('Tried to store a too big object')*/
	return to_browser_storage(key, val)
}
/*
 *
 */
export function del_storage(key) {
	return browser.storage.local.remove(key)
}
/**
 * Generate a user notification asking for the permission if needed
 * The Notification permission may only be requested from inside a short running
 * user-generated event handler.
 * @param {string} title title of the notification
 * @param {string} body the body of the notification
 */
export function generate_notification(title, body) {
	if (!('Notification' in window))
		console.warn('Browser don\'t support desktop notification')
	else if (Notification.permission === 'granted')
		new Notification(title, body)
	else if (Notification.permission !== 'denied')
		Notification.requestPermission().then(function (permission) {
			if (permission === 'granted')
				new Notification(title, body)
		})
}
