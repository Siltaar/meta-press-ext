// SPDX-FileName: ./DNS_resolver.js
// SPDX-FileCopyrightText: 2025 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

export async function DNS_resolve(hostname) {
  if (typeof Deno !== 'undefined') {
    try {
    	const addrs = await Deno.resolveDns(hostname, 'A')
    	return addrs  // array
    } catch (exc) {
      console.error('Caught Deno.resolveDns(hostname, \'A\')', hostname, exc)
    }
  } else if (typeof browser !== 'undefined') {
    try {
      const record = await browser.dns.resolve(hostname)
      return record.addresses || [record.address]  // Firefox always returns a list
    } catch (exc) {
      console.error('Caught browser.dns.resolve(hostname)', hostname, exc)
    }
  }
  return []
}
