// SPDX-FileName: ./BOM_utils.js
// SPDX-FileCopyrightText: 2017-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

// BOM stands for Browser Object Model
// function defined here have dependancies to in-browser JavaScript API

/* globals console document window alert navigator Intl FileReader */

import { urlify } from '../js/URL.js'

export const	$ = (str) => document.querySelector(str)
export const $$ = (str) => document.querySelectorAll(str)
export const id = (str) => document.getElementById(str)
/**
 * Redirects console printing to an alert displayed on demand
 * Used in mobile debugging
 */
export function alert_console() {
	let log = ''
	function make_mp_print (name, nat_obj) {	// eslint-disable-line no-inner-declarations
		return function () {
			log += `${name} : `
			nat_obj.apply(console, arguments)
			for (let i=0; i < arguments.length; i++)
				log += ' '+ String(arguments[i])
			log += '\n'
		}
	}
	console.info = make_mp_print('Info', console.info)
	console.log = make_mp_print('Log', console.log)
	console.warn = make_mp_print('Warning', console.warn)
	console.error = make_mp_print('Error', console.error)
	console.trace = make_mp_print('Trace', console.trace)
	const btn = id('mp_dev')
	btn.style.display = 'block'
	btn.addEventListener('click', () => alert(log))
}
/**
 * Decode the HTML entities from the given string to their UTF8 counterparts
 * More info : https://stackoverflow.com/questions/3700326/decode-amp-back-to-in-javascript
 * @example htmlDecode("&lt;img src='myimage.jpg'&gt;") returns "<img src='myimage.jpg'>"
 * @param {string} str The string to decode
 * @returns {string} The decoded string
 */
export function HTML_decode_entities (str) {
	if (typeof (document) === 'undefined') {
		console.debug('Why did we lost document ? When using JSOM')
		return str
	}
	const elt = document.createElement('textarea')
	// console.log('input of HTML_decode_entities', s)
	elt.innerHTML = str
	// console.log('elt_value', elt.value)
	return elt.value || ''
}
/**
 * Check if the browser is in dark mode.
 * https://stackoverflow.com/questions/50840168/how-to-detect-if-the-os-is-in-dark-mode-in-browsers
 * @returns {boolean} dark mode
 */
export function isDarkMode() {
	return window.matchMedia('(prefers-color-scheme: dark)').matches
}
/**
 * Upload the data in a filename and submit to the user.
 * @param {string} file_name The filename
 * @param {string} str The data to upload
 */
export function upload_file_to_user(file_name, str) {
	const elt_a = document.createElement('a')
	elt_a.href=`data:application/octet-stream;charset=UTF-8,${encodeURIComponent(str)}`
	elt_a.download=file_name
	elt_a.click()
}
/**
 * Return all the months name of the given locale in the given format
 * From https://stackoverflow.com/a/70869819
 * @param {object} with properties : locale (fr, en, ja…) ; format (short, long, numeric…)
 * @returns {Array} an array containing the 12 months names
 */
export function get_months(locale = navigator.language, format = 'long') {
	const locale_format = new Intl.DateTimeFormat(locale, {month: format}).format
	return [...Array(12).keys()].map((m) => locale_format(new Date(2021, m)))
}
/**
 * Lower case, normalize and remove diacritics from the given string
 * https://stackoverflow.com/a/51874002
 */
function norm_str (str) {
	return str.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f.]/g, '')
}
/**
 *
 */
function string_index_in_array(str, arr) {
	return arr.findIndex(a => norm_str(a).startsWith(norm_str(str)))
	// return arr.findIndex(a => norm_str(a).search(norm_str(str)) > 0)
}
/** Gets the locale that the browser is asking for */
export const get_browser_locale = () => navigator.language || navigator.userLanguage
/**
 * Return the month number of the given month name from the given language
 * @param {string} a locale (fr, en, ja…)
 * @param {string} a month name in the given language (février, january, 5月)
 * @returns {number} the corresponding number (2, 1, 5) or the given month_name if not matched
 */
export function lang_month_nb(lang, month_name) {
	let mon = string_index_in_array(month_name, get_months(lang)) + 1 // months start at 0 in JS
	if (mon > 0) return mon  // nl maart is abbr-ed in mrt. so :
	mon = string_index_in_array(month_name, get_months(lang, 'short')) + 1
	if (mon > 0) return mon
	return month_name
}
/**
 * Count the number of browser supported locales as per ISO 639-2 2-letters codes
 * 2022-05-12 : 218 in Firefox 102.0a1
 */
export function locales() {
	return supported_ISO_639_2_language_locales()
}
export function supported_ISO_639_2_language_locales() {
	const res = new Set()
	const alpha = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q',
		'r','s','t','u','v','w','x','y','z']
	const beta_ = ['','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p',
		'q','r','s','t','u','v','w','x','y','z']
	let rz
	for (const l1 of alpha)
		for (const l2 of alpha)
			for (const l3 of beta_) {
				rz = Intl.DateTimeFormat.supportedLocalesOf([l1 + l2 + l3])
				rz.length && res.add(rz[0])
			}
	return Array.from(res).sort()
}
function _supported_xx_xx_country_locales() {	// eslint-disable-line no-unused-vars
	const res = new Set()
	const alpha = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q',
		'r','s','t','u','v','w','x','y','z']
	let rz
	for (const l1 of alpha)
		for (const l2 of alpha)
			for (const l3 of alpha)
				for (const l4 of alpha) {
					rz = Intl.DateTimeFormat.supportedLocalesOf([`${l1+l2}-${l3+l4}`])
					rz.length && res.add(rz[0])
				}
	return Array.from(res).sort()
}
export function known_countries_and_code(lang) {
	const res = {}
	const alpha = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q',
		'r','s','t','u','v','w','x','y','z']
	const beta_ = ['','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p',
		'q','r','s','t','u','v','w','x','y','z']
	let key
	const name = new Intl.DisplayNames([lang], {type: 'region'})
	for (const l1 of alpha)
		for (const l2 of alpha)
			for (const l3 of beta_) {
				key = l1+l2+l3
				try {
					res[key] = name.of([key])
				} catch (_err) {
					continue
				}
			}
	// return Array.from(a).sort()
	return res
}
/**
 * Find the favicon with the given HTML fragment and domain.
 * @param {Node} html_fragment The working area
 * @param {string} domain_part The domain of the website
 * @returns The favicon url
 */
export function get_favicon_URL(HTML_fragment, domain_part) { // favicon may be implicit
	const fav_node = HTML_fragment.querySelector('link[rel~="icon"]')
	// return urlify(fav_node && fav_node.attributes.href.value || '/favicon.ico', domain_part)
	return urlify(fav_node && fav_node.getAttribute('href') || '/favicon.ico', domain_part)
}
/*
 *
 */
export function get_HTML_lang(HTML_fragment) {  // lang could be find in
	// or attributed <html … lang= -> split(/[-_]/)
	// <meta http-equiv="Content-language" content="fr">
	// <meta property="og:locale" content="fr_FR">
	let lang = HTML_fragment.querySelector('html').lang
	if (!lang) {
		const node = HTML_fragment.querySelector('meta[http-equiv="Content-Language"]') ||
			HTML_fragment.querySelector('meta[property="og:locale"]')
		lang = node.content || ''
	}
	const lang_split = lang.split(/[_-]/)
	return [lang_split[0], lang_split[1]]
}
/**
 * Find the RSS feed of a given HTML fragment and domain.
 * @param {Node} html_fragment The working area
 * @param {string} domain_part The domain of the website
 * @returns The favicon url
 */
export function get_RSS_URL(html_fragment, domain_part) { // favicon may be implicit
	let rss = html_fragment.querySelector('link[rel~="alternate"][type="application/rss+xml"]')
	if (rss) return urlify(rss.getAttribute('href'), domain_part)
	rss = html_fragment.querySelector('a[data-smarttag-name="rss"]')
	if (rss) return urlify(rss.getAttribute('href'), domain_part)
	rss = html_fragment.querySelector('a[aria-label="RSS"]')
	if (rss) return urlify(rss.getAttribute('href'), domain_part)
	rss = html_fragment.querySelector('a[href*="feed"]')
	if (rss) return urlify(rss.getAttribute('href'), domain_part)
	rss = html_fragment.querySelector('a[href*="rss"]')
	if (rss) return urlify(rss.getAttribute('href'), domain_part)
	for (const elt_a of html_fragment.querySelectorAll('a')) {
		if (elt_a.textContent.includes('feed') ||
			elt_a.textContent.includes('rss') ||
			elt_a.textContent.includes('RSS')
		)
			return urlify(rss.getAttribute('href'), domain_part)
	}
	if (!rss)
		console.log('RSS not found', domain_part, html_fragment)
	return rss && urlify(rss.getAttribute('href'), domain_part) || ''
}
/**
 * Creates a style link with href or a style node.
 * https://stackoverflow.com/a/524798
 * @param {string} href The stylesheet to reach if undefined create style
 * @returns {StyleSheet} The created or loaded stylesheet
 */
export function createStyleSheet(href) {
	let elt
	if(typeof href !== 'undefined') {
		elt = document.createElement('link')
		elt.type = 'text/css'
		elt.rel = 'stylesheet'
		elt.href = href
	} else elt = document.createElement('style')
	// elt['generated'] = true
	document.getElementsByTagName('head')[0].appendChild(elt)
	const sheet = document.styleSheets[document.styleSheets.length - 1]
	// if(typeof sheet.addRule === 'undefined') // we're not targeting browsers without addRule()
	//	sheet.addRule = addRule
	if(typeof sheet.removeRule === 'undefined')
		sheet.removeRule = sheet.deleteRule
	return sheet
}
/**
 * Demand a file from the user and pass it to a hoop treatment function
 * @param {Function} treatment_hook Given function to act on the file content
 */
export function read_user_file(treatment_hook) {
	const input_elt = document.createElement('input')
	input_elt.type = 'file'
	input_elt.click()
	input_elt.addEventListener('change', () => {
		const file = input_elt.files[0]
		if (file) {
			const reader = new FileReader()
			reader.readAsText(file, 'UTF-8')
			reader.onload = treatment_hook
			reader.onerror = () => console.error('error importing user file')
		}
	}, false)
}
/**
 * Activate dropdown buttons constructs (.drop_btn / .drop_down_div)
 */
export function dropdown_alive() {
	const menus = document.querySelectorAll('.drop_btn')
	for (const menu of menus) {
		menu.addEventListener('click', evt => {
			const elt = evt.target
			elt.textContent = elt.classList.contains('drop_active') ? ' + ' : ' - '
			elt.classList.toggle('drop_active')
			elt.parentNode.querySelector('.drop_down_div').classList.toggle('drop_display')
		})
	}
}
/**
 * Set the language in the HTML document.
 * @param {string} lg The language
 */
export function set_HTML_lang(lg) { document.body.parentElement.lang = lg }
//export const intl_lang_name = (lg) => new Intl.DisplayNames([lg], {type: 'language'}).of(lg)
/**
 * Check if the given HTML element is in overflow status
 * @param {Node} elt The HTML element to check
 * @returns {boolean} overflow ?
 */
export function isOverflown(elt) {
	return elt.scrollHeight > elt.clientHeight || elt.scrollWidth > elt.clientWidth
}
