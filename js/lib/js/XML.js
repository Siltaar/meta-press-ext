// SPDX-FileName: XML.js
// SPDX-FileCopyrightText: 2017-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
/* globals console */

/**
 * Encode the given XML string with UTF8 entities
 * More info : https://stackoverflow.com/questions/18749591/encode-html-entities-in-javascript
 * @param {string} s String to encode
 * @example XML_encode_UTF8 (" « ") returns " &#171; "
 * @returns {string} The encoded string
 */
export function XML_encode_UTF8 (str) {
	return str.replace(/[&\u00A0-\u9999]/g, (itm) => `&#${itm.charCodeAt(0)};`)
}
/**
 * Encode the string for xml (<, >, &, ', ").
 * @param {string} unsafe The string to encode
 * @returns {string} The encoded string
 */
const encode_XML_re = /[<>&'"]/g
export function encode_XML(unsafe) {  // used in export_XML
	if (!unsafe) return unsafe
	unsafe = remove_XML_invalid_chars(unsafe, false) // removeDiscouragedChars -> removes all
	return unsafe.replace(encode_XML_re, (chr) => {
		switch (chr) {
		case '<': return '&lt;'
		case '>': return '&gt;'
		case '&': return '&amp;'
		case "'": return '&apos;'
		case '"': return '&quot;'
		}
	})
}
/**
 * Removes invalid XML 1.0 characters from a string plus the unicode replacement char U+FFFD
 * From https://gist.github.com/john-doherty/b9195065884cdbfd2017a4756e6409cc
 * @param {string} str - a string containing potentially invalid XML char (non-UTF8, STX, EOX)
 * @param {boolean} removeDiscouragedChars - should it remove discouraged but valid XML char.
 * @return {string} a sanitized string stripped of invalid XML characters
 */
/* eslint-disable no-control-regex */
const remove_XML_invalid_chars_re = new RegExp(
	'[^\x09\x0A\x0D\x20-\xFF\x85\xA0-\uD7FF\uE000-\uFDCF\uFDE0-\uFFFD]', 'gm')
/* eslint-enable no-control-regex */
function remove_XML_invalid_chars(str, removeDiscouragedChars=true) {
	str = str.replace(remove_XML_invalid_chars_re, '')
	if (removeDiscouragedChars) {
		// remove everything discouraged by XML 1.0 specifications
		console.debug('Using removeDiscouragedChars')
		const regex = new RegExp(
			'([\\x7F-\\x84]|[\\x86-\\x9F]|[\\uFDD0-\\uFDEF]|(?:\\uD83F[\\uDFFE\\uDFFF])|' +
			' (?:\\uD87F[\\uDFFE\\uDFFF])|(?:\\uD8BF[\\uDFFE\\uDFFF])|(?:\\uD8FF[\\uDFFE\\uDFFF])|'+
			'(?:\\uD93F[\\uDFFE\\uDFFF])|(?:\\uD97F[\\uDFFE\\uDFFF])|(?:\\uD9BF[\\uDFFE\\uDFFF])|'+
			'(?:\\uD9FF[\\uDFFE\\uDFFF])|(?:\\uDA3F[\\uDFFE\\uDFFF])|(?:\\uDA7F[\\uDFFE\\uDFFF])|'+
			'(?:\\uDABF[\\uDFFE\\uDFFF])|(?:\\uDAFF[\\uDFFE\\uDFFF])|(?:\\uDB3F[\\uDFFE\\uDFFF])|'+
			'(?:\\uDB7F[\\uDFFE\\uDFFF])|(?:\\uDBBF[\\uDFFE\\uDFFF])|(?:\\uDBFF[\\uDFFE\\uDFFF])|'+
			'(?:[\\0-\\t\\x0B\\f\\x0E-\\u2027\\u202A-\\uD7FF\\uE000-\\uFFFF]|[\\uD800-\\uDBFF]'+
			'[\\uDC00-\\uDFFF]|[\\uD800-\\uDBFF](?![\\uDC00-\\uDFFF])|(?:[^\\uD800-\\uDBFF]|^)'+
			'[\\uDC00-\\uDFFF]))', 'g')
		str = str.replace(regex, '')
	}
	return str
}
