// SPDX-FileName: types.js
// SPDX-FileCopyrightText: 2017-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

export const is = (val) => typeof val !== 'undefined'
export const is_str = (val) => typeof val === 'string'
export const is_obj = (val) => typeof val === 'object'
export const is_nb  = (val) => typeof val === 'number'
export const is_arr = (val) => Array.isArray(val)
