// SPDX-FileName: array.js
// SPDX-FileCopyrightText: 2017-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

export const is_in	= (lst, val) => lst.indexOf(val) !== -1
/**
 * Returns true if the given array contains the given value
 * @param {Array} array work space
 * @param {*} value The value to remove
 * @returns {Boolean} True if the array contains the value
 */
export function array_contains(arr, val) {
	return arr.indexOf(val) > -1
}
/**
 * Remove the 1st occurrence of the value in the given array.
 * @param {Array} array work space
 * @param {*} value The value to remove
 * @returns {Array} without value
 */
export function array_remove_once(arr, val) {
	let idx = arr.indexOf(val)
	if (idx > -1) arr.splice(idx, 1)
}
/**
 * Add a value in an Array only if it's not already in.
 * @param {Array} Array work space
 * @param {*} value The value to add
 * @returns {Array} The entry Array that might have been modified
 */
export function array_add_once(arr, val) {
	if(arr.indexOf(val) === -1) arr.push(val)
	return arr	// should be removed
}
/**
 * Removes duplicata in a array.
 * @param {Array} array The working array
 * @returns A new clean array
 */
export function remove_duplicate_in_array(arr) { return [...new Set(arr)] }
/*
 *
 */
export function shuffle_array(arr) {
	for (let idx = arr.length - 1; idx > 0; idx--) {
		const jdx = Math.floor(Math.random() * (idx + 1));
		[arr[idx], arr[jdx]] = [arr[jdx], arr[idx]]
	}
}
