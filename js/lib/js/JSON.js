// SPDX-FileName: JSON.js
// SPDX-FileCopyrightText: 2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
/* globals console */

/*
 *
 */
export function backward_indexOf(what_char, where) {
	for (let idx = where.length; idx--;) {
		if (where[idx] == what_char) {
			return idx
		}
	}
	return -1
}
/*
 *
 */
export function escape_charAt(what, where) {
	return [what.slice(0, where), '\\', what.slice(where)].join('')
}

/**
 * Reguplar JSON.parse that escapes embeded double quotes upon exceptions
 */
const JSON_parse_exc_0 = /SyntaxError: JSON.parse: (?:un)?expected (.+?) at line (\d+?) column (\d+?) of the JSON data/  // eslint-disable-line max-len
export function parse_json (str, dbg=false) {
	let res = ''
	for (let retries = 1000; retries--;) {
		try {
			res = JSON.parse(str)
		} catch (exc) {
			dbg && console.info('retries left', retries, exc.toString())
			const match_arr = exc.toString().match(JSON_parse_exc_0)
			if (match_arr && match_arr.length > 3) {
				const pos = Number(match_arr[3]) - 1
				const slice_str = str.slice(0, pos)
				dbg && console.debug('Slice at', pos, `[${str[pos]}]`, str.slice(pos-3, pos+3), '|',
					str.slice(Math.max(0, pos - 60), pos + 60))
				let idx
				switch (match_arr[1]) {
				case "',' or '}' after property value in object":
				case 'double-quoted property name':
					idx = backward_indexOf('"', slice_str)
					// if (pos != idx) console.log('diff', pos - idx, {pos}, {idx})
					str = escape_charAt(str, idx)
					idx++
					dbg && console.log(str.slice(idx-3, idx+3), '|',
						str.slice(Math.max(0, idx - 60), idx + 60))
					break
				default: throw exc
				}
			} else {
				throw exc
			}
		}
	}
	return res
}
