// SPDX-FileName: UUID.js
// SPDX-FileCopyrightText: 2017-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
/* globals crypto */

/**
 * Generate a UUIDv4 (Unique Universal Identifier version 4) from lost StackOverflow example
 * @returns {string} UUIDv4
 */
export function uuidv4() {
	return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, val =>
		(val ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> val / 4).toString(16)
	)
}
