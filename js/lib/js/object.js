// SPDX-FileName: object.js
// SPDX-FileCopyrightText: 2017-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only


/** Returns a deep copy of an object, via JSON.parse(JSON.stringify(obj))
 * @param {Object} obj The object to duplicate
 * @returns {Object} a clone of the given object
 */
export function deep_copy(obj){ return JSON.parse(JSON.stringify(obj)) }
