// SPDX-FileName: text.js
// SPDX-FileCopyrightText: 2017-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

/**
 * Cut the string to the at size if string is too long.
 * @param {string} str The string
 * @param {number} at The needed size
 * @returns {string} the shorten str
 */
export function shorten(str, at) {
	return str && str.length > at ? `${str.slice(0, at)}…` : str || ''
}
/**
 * Remove trailing space before and after the string and the double space (\s\s) in the string.
 * @param {string} str Working string
 * @returns {string}
 */
export function triw(str) { return str && str.replace(/^\s*|\s*$|(\s\s)+/g, '') || '' }
/**
 * Remove all spaces.
 * @param {string} str Working string
 * @returns {string}
 */
export function dry_triw(str) { return str && str.replace(/\s*/g, '') }
export function drop_low_unprintable_utf8(str) {
	str = str.replace(/[\u0000-\u0009]/g, '')					 // eslint-disable-line no-control-regex
	str.replace(/[\u000B-\u000C]/g, '')								 // eslint-disable-line no-control-regex
	return str && str.replace(/[\u000E-\u001F]/g, '')  // eslint-disable-line no-control-regex
}
/**
 * Quote the special character in the string.
 * From : http://kevin.vanzonneveld.net, http://magnetiq.com
 * @param {string} str Working string
 * @example preg_quote("How many? $40") returns 'How many\? \$40'
 * @example preg_quote("\\.+*?[^]$(){}=!<>|:"); returns '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'
 * @returns {string} The string prepared
 */
export function preg_quote(str) {
	// str = String(str).replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1")
	str = String(str).replace(/([\\.+*?[^\]$(){}=!<>|:])/g, '\\$1')
	return str.replace(/\s/g, '\\s')	// ensure we match converted &nbsp;
}
export function bolden (str, search) {
	if (typeof str !== 'string') return ''
	const search_term_list = search.split(' ')
	for (const term of search_term_list)
		str = str.replace(new RegExp(`(${preg_quote(term)})`, 'gi'), '<b>$1</b>')
	return str
}
/**
 * Quote the special character in the string for a file.
 * @param {string} str Working string
 * @returns {string} The string prepared
 */
export function preg_quote_file(str) { return str.replace(/([\\"',])/g, '\\$1') }
export function drop_escaped_quote_file(str) { return str.replace(/\\(["',\n])/g, '$1') }
/**
 * Remove the \' (and \") in a string.
 * @param {string} str The string to clean
 * @returns {string}
 */
export function drop_escaped_quotes(str) { return str.replace(/\\'/g, "'").replace(/\\"/g,'"')}
/**
 * Return a string formated in title case : 1st letter uppercase and others lowercase.
 * @function
 * @param {string} str string to tranform
 * @returns {string} string title formated
 */
export function title_case(str) {
	return str.replace(/\w\S*/g, a => `${a.charAt(0).toUpperCase()}${a.substr(1).toLowerCase()}`)
}
/**
 * Concat all lines of a single String into a one liner String
 * @param {string} str The string to work on
 * @returns {string} a new string with no '\n' nor '\r' in it
 */
export function str_uniq_line (str) {
	return str.replace(/[\n\r]+/g, '')
}
