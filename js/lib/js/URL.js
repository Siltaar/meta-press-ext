// SPDX-FileName: URL.js
// SPDX-FileCopyrightText: 2017-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
/* globals console */

import { is_in } from './array.js'
import { title_case } from './text.js'

/**
 * Extract the domain of the given url.
 * @param {string} url The url
 * @returns {string} The domain extracted
 */
export function domain_part(url) {
	const [htt, , dom] = url.split('/')
	return `${htt}//${dom}`
}
/**
 * Extract the domain name of the given url.
 * @param {string} url The url
 * @returns {string} The domain extracted
 */
export function domain_name(url) {
	const [ , , dom] = url.split('/')
	let s_dom = dom.split('.')
	if (s_dom[0] === 'www') s_dom = s_dom.slice(1, s_dom.length)
	return title_case(s_dom.join('.'))
}
// const valid_HTTP_protocols = ['https:', 'http:', 'data:', 'file:']

/**
 * String to define a regular expression test the validity of a URL
 * https://stackoverflow.com/questions/3809401/what-is-a-good-regular-expression-to-match-a-url
 * https://www.freecodecamp.org/news/check-if-a-javascript-string-is-a-url/
 * https://stackoverflow.com/questions/13373504/what-is-a-valid-url-query-string
 * https://www.rfc-editor.org/rfc/rfc3986
 * http://tools.ietf.org/html/std66
 */
const rfc3986_alphanum_subdelim = '-a-z0-9._~!$&"\'()*+,;=:@'
const encoded_hexa = '%[0-9a-f]{2}'
const rfc3986_allowed_in_path = `[${rfc3986_alphanum_subdelim}]|${encoded_hexa}`
const rfc3986_allowed_in_querystring = `[${rfc3986_alphanum_subdelim}/?]|${encoded_hexa}`
const valid_HTTP_URL_str = '^\\s*'+
	'(https?://)?'+ // validate protocol
	'('+
		'('+
			'('+
				'(www\\.[\\w-.]*?[\\w-])'+ // validate sub-domain and domain name with www.
				'|((?!www\\.)[\\w.-]*?[\\w-])'+ // validate sub- and domain name, negative lookahead
			')'+
			'\\.[a-z]{2,}'+ // validate extension
		')'+
	//    '|((\\d{1,3}\\.){3}\\d{1,3})'+ // or validate IPv4 address
	//    '|()'+  // or validate IPv6 address hint: https://gist.github.com/syzdek/6086792
	')'+
	'(:\\d{1,5})?'+ // validate port 0 - 65536
	`(/(${rfc3986_allowed_in_path})*)*`+ // validate path
	`(\\?(${rfc3986_allowed_in_querystring})*)?`+ // validate query string
	`(\\#(${rfc3986_allowed_in_querystring})*)?`+ // validate fragment locator / anchor
	'\\s*$'
/**
 * Regular expression to test the validity of a URL
 */
export const valid_HTTP_URL_RE = new RegExp(valid_HTTP_URL_str, 'i')
/**
 * Tests if a given string is a valid URL
 * @param {string} str The string to test
 * @returns {boolean} URL validity
 */
/*export function is_valid_HTTP_URL(str) {  // inspired from https://stackoverflow.com/a/43467    144
// let url
		try { url = new URL(str) } catch (_) { return false }
		return true  // valid_HTTP_protocols.includes(url.protocol)
	}*/  // validates : http://w
export function is_valid_HTTP_URL(str) {
	return valid_HTTP_URL_RE.test(str)  // validates : http://www.kon
}
/**
 * Tranform the link in a url link for domain part.
 * @param {string} link The link to transform
 * @param {string} domain_part The doamin part of the destination
 * @returns {string} The working url
 */
export function urlify(link, domain_part) { // return URL from any href (even relative links)
	let url
	if (!link ||
		link.startsWith('http') ||
		link.startsWith('data:') ||
		link.startsWith('file:')
	)
		// and we are very happy to test implicitely many cases
		// http -> the link is ok ? might need to check for is_valid_HTTP_URL
		// data: -> e.g. img from JamaicaObserver, we need to have them pass through
		// should we also passthrouh file: links ?
		return link
	// else if (link.startsWith('file:') || link.startsWith('data:'))
	else if (link.startsWith('//'))
		url = (domain_part.split('/'))[0] + link
	else {
		const sep = link[0] !== '/' ? '/' : ''
		url = `${domain_part}${sep}${link}`
	}
	//
	// *.wikinews.org links are already encoded
	// ubpost.mn needs links (paths) to be encoded
	url = is_in(link, '%') ? url : encodeURI(url)
	if (!is_valid_HTTP_URL(url)) {
		console.error('urlify detected an invalid URL')
		console.debug('urlify input link', {link})
		console.debug('urlify url', {url})
	}
	return is_valid_HTTP_URL(url) ? url : null
}
/*
 *
 */
export function slugify(str) { return str.replace(/ /g, '_') }
/*
 *
 */
export function rm_trailing_slash(str) { return str.replace(/\/$/, '') }
/**
 * Get the selected value in the given HTML select object
 * @param {Object} sel The HTML select object
 * @returns The current selected value
 */
export function get_HTML_select_value(sel) { return sel.options[sel.selectedIndex].value }
/**
 * Remove an anchor from a given URL.
 * @example  rm_href_anchor('olivier.fr#tree') return 'olivier.fr'
 * @param {URL} permalink The link
 * @returns The link without the anchor
 */
export function rm_href_anchor(permalink) { return permalink.href.split('#')[0] }
