// SPDX-FileName: date.js
// SPDX-FileCopyrightText: 2017-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
/* globals console setTimeout */

import { is_in } from './array.js'

export function ndt() { return new Date() }
/**
 * Generate string of the current date in a generic human readable format
 * @returns {string} Human readable current date representation
 */
export function ndt_human_readable() {
	return new Date().toISOString().replace('T', '_').replace(':', 'h').split(':')[0]
}
/**
 * Await for a certain delay, in milliseconds
 * From : https://www.delftstack.com/howto/javascript/javascript-wait-for-x-seconds/#use-promi  ses-and-async%2fawait-to-wait-for-x-seconds-in-javascripthttps://www.revue.lu/search/
 * @function
 * @param {Number} n the delay in ms
 * @returns {Promise}
 */
export async function delay(n) {
	return new Promise((resolve) => setTimeout(resolve, n))
}
/**
 * Returns the given date transformed regarding to the navigator timezone.
 * If nav_to_tz is true the timezone offset is added, else it is removed
 * else returns the date from the given timezone
 * @param {string} dt_str The date
 * @param {string} tz The timezone
 * @param {boolean} nav_to_tz The direction of the conversion
 * @returns {Date} the date converted
 */
export function timezoned_date (dt_str, tz='UTC', nav_to_tz=false, dbg=false) {
	const dt = dt_str ? new Date(dt_str) : new Date()
	dbg && console.log('timezoned_date', {dt_str}, '\n', {dt}, '\n', {tz})
	if (isNaN(dt)) return NaN
	if ('UTC' === tz || 'GMT' === tz) return dt
	const dt_orig = new Date(dt.getTime() - dt.getTimezoneOffset()*60*1000)  // to get it in ms
	const dt_UTC  = new Date(dt.getTime() + dt.getTimezoneOffset()*60*1000)
	dbg && console.log('dt', dt, '\n', {dt_orig}, '\n', {dt_UTC}, '\n', {tz})
	let tz_offset
	const nb_tz = Number(tz)
	if (tz !== '' && !isNaN(nb_tz)) {  // NaN is a number, '' is not NaN…
		const tz_sign = nb_tz > 0 ? '' : '-'
		const tz_hours = String(Math.abs(nb_tz / 60)).padStart(2, '0')
		const tz_minutes = String(Math.abs(nb_tz % 60)).padStart(2, '0')
		tz_offset = `${tz_sign}${tz_hours}:${tz_minutes}`
	} else {  // usually the tz is a string like : Europe/Paris
		const dt_repr = dt_UTC.toLocaleTimeString('fr', {timeZoneName: 'short', timeZone: tz})
		// dt_UTC.toLocaleTimeString -> should be replaced by Intl.DateTimeFormat
		// BUT tz changes at each call
		// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/DateTimeFormat/format
		dbg && console.log('dt_repr', dt_repr)
		tz_offset = dt_repr.split('UTC')[1]
		if ('' === tz_offset)  // so it's UTC finally
			return dt
		if (!is_in(tz_offset, ':'))
			tz_offset = `${tz_offset}:00`
		if (tz_offset.length < 6)
			tz_offset = `${tz_offset[0]}0${tz_offset.slice(1)}`
		if ('+' !== tz_offset[0])
			tz_offset = `-${tz_offset.slice(1)}`
	}
	dbg && console.log('tz_offset', tz_offset, 'typeof tz', typeof(tz))
	let date_tz
	if (nav_to_tz) {
		date_tz = dt_UTC
		const tz_sign = tz_offset[0]
		tz_offset = `${'+' === tz_sign ? '-' : '+'}${tz_offset.slice(1)}`
	} else {
		date_tz = dt_orig
	}
	dbg && console.log('date_tz.toISOString', date_tz.toISOString(), 'tz_offset', tz_offset)
	// example -> Date.parse("2019-01-01T00:00:00.000+00:00");
	const new_tzd_date = date_tz.toISOString().replace(/Z/, tz_offset)
	dbg && console.log('new_tzd_date', new_tzd_date)
	return new Date(new_tzd_date)
}
