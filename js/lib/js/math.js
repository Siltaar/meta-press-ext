// SPDX-FileName: math.js
// SPDX-FileCopyrightText: 2017-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

/**
 * Convert the given string to Number, removing thousand separators (such as english ',' or
 * french '.')
 * @param {string} str The string to be converted to Number()
 * @returns {Number}
 */
export function do_int (str) { return Number(str ? str.replace(/[,.\s]/g, '') : 0) }
/**
 * Return a 'random' number of n digits
 * @function
 * @param {number} n The number of wanted digits in the returned integer
 * @returns {number} A 'random' integer of given n digits
 */
export function rnd(n) { return Math.ceil (Math.random () * Math.pow (10, n)) }
/**
 * Pick a 'random' number between a and b.
 * @function
 * @param {number} min Inferior limit
 * @param {number} max Superior limit
 * @returns {number} The random number
 */
export function pick_between(min, max) { return Math.floor(Math.random() * max) + min }
