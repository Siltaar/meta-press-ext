// SPDX-FileName: ./lib/js/XPath_evaluator.js
// SPDX-FileCopyrightText: 2017-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
/* XPathResult */

/**
 * Return a namespace URL from a given prefix. Contains all the namespaces we found yet.
 * Extended version of : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Introduction_to_using_XPath_in_JavaScript#Implementing_a_User_Defined_Namespace_Resolver
 * @param {string} prefix The prefix
 * @returns {string} the corresponding URL of the intended prefix norm
 */
function NS_resolver(prefix) {
	const NS = {
		'atom': 'http://www.w3.org/2005/Atom',
		'cc': 'http://web.resource.org/cc/',
		'content':'http://purl.org/rss/1.0/modules/content/',
		'dc': 'http://purl.org/dc/elements/1.1/',
		'enc': 'http://purl.oclc.org/net/rss_2.0/enc/',
		'image': 'http://www.google.com/schemas/sitemap-image/1.1',
		'itunes': 'http://www.itunes.com/dtds/podcast-1.0.dtd',
		'mathml': 'http://www.w3.org/1998/Math/MathML',
		'media': 'http://search.yahoo.com/mrss/',
		'prism': 'http://prismstandard.org/namespaces/basic/2.0/',
		'rdf' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
		'slash': 'http://purl.org/rss/1.0/modules/slash/',
		'sy': 'http://purl.org/rss/1.0/modules/syndication/',
		'wfw': 'http://wellformedweb.org/CommentAPI/',
		'xhtml' : 'http://www.w3.org/1999/xhtml',
	}
	return NS[prefix] || null
}
// Evaluate an XPath expression aExpression against a given DOM node
// or Document object (aNode), returning the results as an array
// thanks wanderingstan at morethanwarm dot mail dot com for the
// initial work.
/**
 * The XPath evaluator used by {@link evaluateXPath}.
 * @constant
 * @type {XPathEvaluator}
 */
// const xpe = new XPathEvaluator()
/**
 * Search the XPath expression path in a node (querySelectorAll for XPath).
 * @param {Node} node The node working area
 * @param {string} XPath_expression The xpath expression
 * @returns {Array} All the found matchs
 */
export function evaluateXPath(node, XPath_expression) {
	// let nsResolver = document.createNSResolver(  // 2023-07-07: not working with RSS files
	//  node.ownerDocument == null ? node.documentElement : node.ownerDocument.documentElement)
	// let result = xpe.evaluate(XPath_expression, node, NS_resolver, XPathResult.ANY_TYPE, null)
	if (typeof (node.ownerDocument.evaluate) === 'undefined') return null
	const result = node.ownerDocument.evaluate (  // 2024-07-04: avoid new XPathEvaluator()
		XPath_expression,
		node,
		NS_resolver,
		0,  // means -> XPathResult.ANY_TYPE,
		null,
	)
	let found = [], res
	// console.info ('XPath result', result)
	/*if (typeof(result) === 'string')
			return result*/
	while (res = result.iterateNext())
		found.push(res)
	if (found.length === 1)
		found = found[0]
	return found
}
