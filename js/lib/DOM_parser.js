// SPDX-FileName: ./DOM_parser.js
// SPDX-FileCopyrightText: 2022 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
/* globals console fetch */

/**
 * The DOM parser used to generate a DOM error example
 * More info : https://stackoverflow.com/questions/11563554/how-do-i-detect-xml-parsing-errors-when-using-javascripts-domparser-in-a-cross
 * @type {DOMParser}
 * @constant
 */
// const DOM_parser = new DOMParser()  // Hoisted at upper level to allow choosing the provider
/**
 * Check if the dom has parser error.
 * @param {string} dom The dom fargment to check
 * @returns {boolean} parser error ?
 */
export function is_DOM_parsererror(DOM) {
	/* if (DOM_err_NS === 'http://www.w3.org/1999/xhtml') {
		// In PhantomJS the parserirror element doesn't seem to have a special namespace,
		// so we are just guessing here :(
		return dom.getElementsByTagName('parsererror').length > 0
	}*/
	const err = get_DOM_parsererror(DOM)
	return Boolean(err)
}
// https://developer.mozilla.org/fr/docs/Web/API/TextDecoder/TextDecoder
//
export function get_DOM_parsererror(DOM, dbg=false) {
// export function get_DOM_parsererror(DOM, dbg=true) {
	const err = DOM.querySelector('parsererror')
	err && dbg && console.error('FIXME', err)
	return err
}
/**
 *
 */
export async function get_HTML_fragment(URL, DOM_parser) {
	console.log('should be replaced by try_fetch && DOM_parse_text_rep')
	try {
		let page
		try {
			page = await fetch(URL)
		} catch (exc) {
			console.warn(exc)
			page = {ok:false}
		}
		if (!page.ok) return null
		const page_text = await page.text()
		let ctype = page.headers.get('content-type')
		ctype = ctype.split(';')[0] // remove charset
		const rep = DOM_parser.parseFromString(page_text, ctype)
		if (is_DOM_parsererror(rep))
			throw new Error('DOM parser '+get_DOM_parsererror(rep).textContent)
		// console.log('rep', rep)
		return rep
	} catch (err) {
		console.warn(err)
		return null
	}
}
/**
 * Remove HTML tags from a string, used to sanitize inputs
 * @example strip_HTML_tags('<html><body>Hello</body></html>') return 'Hello'
 * @param {string} str The string expected to content HTML code
 * @returns the string without HTML tags
 */
export function strip_HTML_tags (str, DOM_parser) {
	if (!str) return ''
	if (!(typeof (str) === 'string')) str = String(str)
	let html
	let text_content = ''
	try {
		html = DOM_parser.parseFromString(str, 'text/html')
		const body = html.body
		text_content = body.textContent
	} catch (exc) {
		console.warn(exc)
	}
	// console.log(html)
	// if (is_DOM_parsererror(html))
	// 	throw 'DOM parser error'
	//  return null
	return text_content
}
